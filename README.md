Margem fácil - Alfa
================

Sistema de gestão de consignados.

Cada branch representa um convênio que utiliza o sistema, convênios inclusos:

*  Amarante
*  Queimadas
*  Sape
*  Areia
*  Vitoria do Mearim
*  São Benedito do Rio Preto (sbrp)
*  São josé de Ribamar (sjr)
*  Buriticupu


Pré-requisitos
--------------

* ruby:  2.1.2p95
* ruby on rails: 4.1.6


Configurando o banco de dados
--------------------

Só é necessário executar isso uma vez.

1. Instalar o postgresql:

		$ apt-get install postgresql


2. Criar um usuário margemfacil com permissão pra criação de bancos (a senha deve ser "23052013"):

		$ sudo -u postgres createuser -PE -dRS margemfacil

3. Instalar libpq-dev
                $ sudo apt-get install libpq-dev

4. Criar o banco normalmente:

		$ rake db:create


Rodando a aplicação localmente
------------------------------


1. Rodar o servidor rails como usual

		$ rake db:migrate
		$ rails s

2. Abrir o navegador em "localhost:3000"