task :erase do
  #Rails.env = "staging"
  Rake::Task["db:drop"].reenable
  Rake::Task['db:drop'].invoke
  Rake::Task["db:create"].reenable
  Rake::Task['db:create'].invoke
  Rake::Task["db:migrate"].reenable
  Rake::Task['db:migrate'].invoke
end

task 'db:dev' => :erase do
  Rake::Task["db:seed:development"].reenable
  Rake::Task['db:seed:development'].invoke
end

task 'db:stg' => :erase do
  Rake::Task["db:seed:staging"].reenable
  Rake::Task['db:seed:staging'].invoke
end