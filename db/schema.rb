# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160622172927) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assistance_contracts", force: true do |t|
    t.decimal  "discount_value"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "public_employee_id"
    t.integer  "consignee_id"
    t.string   "contract_number"
    t.integer  "operator_id"
    t.integer  "paid_installments",  default: 0
  end

  add_index "assistance_contracts", ["consignee_id"], name: "index_assistance_contracts_on_consignee_id", using: :btree
  add_index "assistance_contracts", ["operator_id"], name: "index_assistance_contracts_on_operator_id", using: :btree
  add_index "assistance_contracts", ["public_employee_id"], name: "index_assistance_contracts_on_public_employee_id", using: :btree

  create_table "assistance_observations", force: true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "operator_id"
    t.integer  "assistance_contract_id"
  end

  add_index "assistance_observations", ["assistance_contract_id"], name: "index_assistance_observations_on_assistance_contract_id", using: :btree
  add_index "assistance_observations", ["operator_id"], name: "index_assistance_observations_on_operator_id", using: :btree

  create_table "attendance_locals", force: true do |t|
    t.string   "name"
    t.string   "cnpj"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "zip_code"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.integer  "consignee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authorizations", force: true do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authorizations", ["role_id"], name: "index_authorizations_on_role_id", using: :btree
  add_index "authorizations", ["user_id"], name: "index_authorizations_on_user_id", using: :btree

  create_table "card_observations", force: true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "card_id"
    t.integer  "operator_id"
  end

  add_index "card_observations", ["card_id"], name: "index_card_observations_on_card_id", using: :btree
  add_index "card_observations", ["operator_id"], name: "index_card_observations_on_operator_id", using: :btree

  create_table "cards", force: true do |t|
    t.string   "proposal_number"
    t.decimal  "discount_value"
    t.integer  "public_employee_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.integer  "consignee_id"
    t.integer  "operator_id"
  end

  add_index "cards", ["consignee_id"], name: "index_cards_on_consignee_id", using: :btree
  add_index "cards", ["operator_id"], name: "index_cards_on_operator_id", using: :btree
  add_index "cards", ["public_employee_id"], name: "index_cards_on_public_employee_id", using: :btree

  create_table "commissioneds", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "configurations", force: true do |t|
    t.string   "name"
    t.text     "observation"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "category"
  end

  create_table "consignees", force: true do |t|
    t.string   "name"
    t.string   "cnpj"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "zip_code"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "contact"
    t.string   "post"
    t.string   "phone_number"
    t.string   "second_contact"
    t.string   "second_post"
    t.string   "second_phone_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "errors", force: true do |t|
    t.string   "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "file_exception_notifications", force: true do |t|
    t.string   "file_type"
    t.boolean  "fail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "financial_release_files", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "financial_releases", force: true do |t|
    t.integer  "card_id"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.integer  "financial_release_file_id"
    t.integer  "operator_id"
  end

  add_index "financial_releases", ["card_id"], name: "index_financial_releases_on_card_id", using: :btree
  add_index "financial_releases", ["financial_release_file_id"], name: "index_financial_releases_on_financial_release_file_id", using: :btree
  add_index "financial_releases", ["operator_id"], name: "index_financial_releases_on_operator_id", using: :btree

  create_table "holidays", force: true do |t|
    t.integer  "holiday_type"
    t.text     "observation"
    t.date     "holiday_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insertion_lines", force: true do |t|
    t.string   "year"
    t.string   "month"
    t.string   "organ_code"
    t.string   "registration"
    t.string   "event_code"
    t.string   "discount_value"
    t.string   "blank_field"
    t.string   "cpf"
    t.string   "contract_number"
    t.string   "uso_emp"
    t.integer  "installment_to_pay"
    t.integer  "deadline"
    t.string   "segment"
    t.string   "final_fill"
    t.string   "line_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "insertion_id"
    t.integer  "loan_id"
    t.integer  "assistance_contract_id"
    t.integer  "variation_id"
    t.integer  "financial_release_id"
    t.string   "insertion_type"
    t.boolean  "partial_discount"
    t.string   "averbation_code"
    t.integer  "consignee_id"
  end

  add_index "insertion_lines", ["assistance_contract_id"], name: "index_insertion_lines_on_assistance_contract_id", using: :btree
  add_index "insertion_lines", ["consignee_id"], name: "index_insertion_lines_on_consignee_id", using: :btree
  add_index "insertion_lines", ["financial_release_id"], name: "index_insertion_lines_on_financial_release_id", using: :btree
  add_index "insertion_lines", ["insertion_id"], name: "index_insertion_lines_on_insertion_id", using: :btree
  add_index "insertion_lines", ["loan_id"], name: "index_insertion_lines_on_loan_id", using: :btree
  add_index "insertion_lines", ["variation_id"], name: "index_insertion_lines_on_variation_id", using: :btree

  create_table "insertions", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "month"
    t.integer  "year"
    t.string   "file_name"
    t.string   "file_path"
  end

  create_table "io_accesses", force: true do |t|
    t.boolean  "successful", default: false
    t.boolean  "finished",   default: false
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "literacies", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "loan_histories", force: true do |t|
    t.integer  "public_employee_id"
    t.decimal  "contract_value"
    t.decimal  "liquid_value"
    t.integer  "deadline"
    t.string   "status"
    t.string   "contract_type"
    t.datetime "created"
    t.datetime "updated"
    t.decimal  "balance_due"
    t.integer  "consignee_id"
    t.integer  "installments_paid"
    t.decimal  "installment_value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "loan_id"
    t.integer  "father_id"
    t.string   "contract_number"
    t.decimal  "cet"
    t.decimal  "interest_tax"
    t.decimal  "others"
    t.integer  "operator_id"
  end

  add_index "loan_histories", ["loan_id"], name: "index_loan_histories_on_loan_id", using: :btree

  create_table "loan_observations", force: true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "loan_id"
    t.integer  "operator_id"
  end

  add_index "loan_observations", ["loan_id"], name: "index_loan_observations_on_loan_id", using: :btree
  add_index "loan_observations", ["operator_id"], name: "index_loan_observations_on_operator_id", using: :btree

  create_table "loans", force: true do |t|
    t.integer  "father_id"
    t.integer  "public_employee_id"
    t.decimal  "contract_value"
    t.decimal  "liquid_value"
    t.integer  "deadline"
    t.string   "status"
    t.string   "contract_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "balance_due"
    t.integer  "consignee_id"
    t.integer  "installments_paid",   default: 0
    t.decimal  "installment_value"
    t.string   "contract_number"
    t.decimal  "cet"
    t.decimal  "interest_tax"
    t.decimal  "others"
    t.integer  "operator_id"
    t.string   "origin_operation"
    t.decimal  "balance_due_release"
  end

  add_index "loans", ["consignee_id"], name: "index_loans_on_consignee_id", using: :btree
  add_index "loans", ["operator_id"], name: "index_loans_on_operator_id", using: :btree
  add_index "loans", ["public_employee_id"], name: "index_loans_on_public_employee_id", using: :btree

  create_table "margin_criticisms", force: true do |t|
    t.string   "file_name"
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "margin_histories", force: true do |t|
    t.integer  "public_employee_id"
    t.decimal  "margin_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "margin_histories", ["public_employee_id"], name: "index_margin_histories_on_public_employee_id", using: :btree

  create_table "marital_statuses", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "negotiations", force: true do |t|
    t.integer  "loan_id"
    t.integer  "child_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "negotiation_type"
  end

  add_index "negotiations", ["child_id"], name: "index_negotiations_on_child_id", using: :btree
  add_index "negotiations", ["loan_id"], name: "index_negotiations_on_loan_id", using: :btree

  create_table "operators", force: true do |t|
    t.string   "name"
    t.string   "cpf"
    t.string   "company_phone_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "consignee_id"
    t.integer  "attendance_local_id"
    t.string   "email"
  end

  add_index "operators", ["attendance_local_id"], name: "index_operators_on_attendance_local_id", using: :btree
  add_index "operators", ["consignee_id"], name: "index_operators_on_consignee_id", using: :btree

  create_table "organs", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cnpj"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "zip_code"
    t.string   "city"
    t.string   "state"
    t.string   "country"
  end

  create_table "portability_requests", force: true do |t|
    t.string   "status"
    t.integer  "loan_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "seller_id"
    t.integer  "buyer_id"
    t.string   "portability_code"
    t.text     "observation_seller"
    t.text     "observation_buyer"
  end

  add_index "portability_requests", ["loan_id"], name: "index_portability_requests_on_loan_id", using: :btree

  create_table "product_configurations", force: true do |t|
    t.integer  "product_id"
    t.integer  "configuration_id"
    t.boolean  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_configurations", ["configuration_id"], name: "index_product_configurations_on_configuration_id", using: :btree
  add_index "product_configurations", ["product_id"], name: "index_product_configurations_on_product_id", using: :btree

  create_table "product_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "event_code"
    t.integer  "deadline"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_type_id"
    t.integer  "consignee_id"
  end

  add_index "products", ["consignee_id"], name: "index_products_on_consignee_id", using: :btree
  add_index "products", ["product_type_id"], name: "index_products_on_product_type_id", using: :btree

  create_table "properties", force: true do |t|
    t.string   "name"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "observation"
    t.string   "category"
  end

  create_table "public_employees", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cpf"
    t.integer  "organ_id"
    t.string   "registration"
    t.date     "admission_date"
    t.string   "situation"
    t.date     "end_contract_date"
    t.string   "suspension_age"
    t.decimal  "margin_value",       precision: 7, scale: 2
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "zip_code"
    t.string   "phone"
    t.date     "birth_date"
    t.string   "marital_status"
    t.string   "voter_registration"
    t.string   "zone"
    t.string   "gender"
    t.string   "rg"
    t.string   "dispatcher_organ"
    t.string   "dispatcher_uf"
    t.string   "role"
    t.integer  "literacy_id"
    t.integer  "situation_id"
    t.integer  "commissioned_id"
    t.integer  "suspension_age_id"
    t.integer  "marital_status_id"
    t.string   "section"
    t.string   "approval_password"
    t.string   "email"
  end

  add_index "public_employees", ["commissioned_id"], name: "index_public_employees_on_commissioned_id", using: :btree
  add_index "public_employees", ["literacy_id"], name: "index_public_employees_on_literacy_id", using: :btree
  add_index "public_employees", ["marital_status_id"], name: "index_public_employees_on_marital_status_id", using: :btree
  add_index "public_employees", ["organ_id"], name: "index_public_employees_on_organ_id", using: :btree
  add_index "public_employees", ["situation_id"], name: "index_public_employees_on_situation_id", using: :btree
  add_index "public_employees", ["suspension_age_id"], name: "index_public_employees_on_suspension_age_id", using: :btree

  create_table "return_codes", force: true do |t|
    t.string   "code"
    t.string   "description"
    t.text     "obs"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "return_file_critics", force: true do |t|
    t.string   "file_name"
    t.string   "observation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "review_financial_releases", force: true do |t|
    t.string   "file_name"
    t.string   "error"
    t.string   "card_number"
    t.string   "name"
    t.string   "cpf"
    t.string   "registration"
    t.string   "organ"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "value"
    t.integer  "financial_release_file_id"
    t.integer  "line"
  end

  add_index "review_financial_releases", ["financial_release_file_id"], name: "index_review_financial_releases_on_financial_release_file_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "situations", force: true do |t|
    t.string   "name"
    t.string   "file_reference"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "suspension_ages", force: true do |t|
    t.string   "name"
    t.integer  "file_reference"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "login",                     limit: 50, null: false
    t.integer  "login_attempts",                       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.boolean  "blocked"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.integer  "public_employee_id"
    t.integer  "operator_id"
    t.string   "create_user_token"
    t.datetime "create_user_token_sent_at"
    t.datetime "last_password_change"
    t.integer  "roles_mask"
  end

  add_index "users", ["operator_id"], name: "index_users_on_operator_id", using: :btree
  add_index "users", ["public_employee_id"], name: "index_users_on_public_employee_id", using: :btree

  create_table "variations", force: true do |t|
    t.decimal  "value"
    t.string   "category"
    t.integer  "assistance_contract_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.integer  "operator_id"
    t.integer  "paid_installments"
  end

  add_index "variations", ["assistance_contract_id"], name: "index_variations_on_assistance_contract_id", using: :btree
  add_index "variations", ["operator_id"], name: "index_variations_on_operator_id", using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

end
