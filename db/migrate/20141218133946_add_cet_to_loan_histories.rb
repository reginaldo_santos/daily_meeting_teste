class AddCetToLoanHistories < ActiveRecord::Migration
  def change
    add_column :loan_histories, :cet, :decimal
  end
end
