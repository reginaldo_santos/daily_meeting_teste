class CreateCardObservations < ActiveRecord::Migration
  def change
    create_table :card_observations do |t|
      t.text :text
      t.timestamps
    end
  end
end
