class ChangeTypeStatusLoan < ActiveRecord::Migration
  def change
    change_column :loans, :status, :string
  end
end
