class CreateNegotiations < ActiveRecord::Migration
  def change
    create_table :negotiations do |t|
      t.integer :loan_id
      t.integer :child_id

      t.timestamps
    end
    add_index :negotiations, :loan_id
    add_index :negotiations, :child_id
  end
end
