class AddCommissionedIdToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :commissioned_id, :integer
    add_index :public_employees, :commissioned_id
  end
end
