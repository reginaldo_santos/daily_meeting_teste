class CreateSituations < ActiveRecord::Migration
  def change
    create_table :situations do |t|
      t.string :name
      t.string :file_reference

      t.timestamps
    end
  end
end
