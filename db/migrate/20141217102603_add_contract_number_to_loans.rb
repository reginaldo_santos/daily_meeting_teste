class AddContractNumberToLoans < ActiveRecord::Migration
  def change
    add_column :loans, :contract_number, :string
  end
end
