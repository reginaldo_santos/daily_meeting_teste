class AddAssistanceContractToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :assistance_contract_id, :integer
    add_index :insertion_lines, :assistance_contract_id
  end
end
