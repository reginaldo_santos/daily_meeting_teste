class CreateConsignees < ActiveRecord::Migration
  def change
    create_table :consignees do |t|
      t.string :name
      t.string :event_code
      t.string :cnpj
      t.string :street
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country
      t.string :contact
      t.string :post
      t.string :phone_number
      t.string :second_contact
      t.string :second_post
      t.string :second_phone_number
      t.string :product_type
      t.integer :deadline

      t.timestamps
    end
  end
end
