class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :father_id, :references => :loans
      t.integer :public_employee_id
      t.integer :user_id
      t.decimal :contract_value
      t.decimal :liquid_value
      t.integer :deadline
      t.integer :status
      t.integer :contract_type

      t.timestamps
    end
    add_index :loans, :public_employee_id
    add_index :loans, :user_id
  end
end
