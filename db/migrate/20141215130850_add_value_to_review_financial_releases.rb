class AddValueToReviewFinancialReleases < ActiveRecord::Migration
  def change
    add_column :review_financial_releases, :value, :decimal
  end
end
