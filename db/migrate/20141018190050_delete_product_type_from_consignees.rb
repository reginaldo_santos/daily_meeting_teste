class DeleteProductTypeFromConsignees < ActiveRecord::Migration
  def change
    remove_column :consignees, :product_type
  end
end
