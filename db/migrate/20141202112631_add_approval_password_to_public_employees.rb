class AddApprovalPasswordToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :approval_password, :string
  end
end
