class AddUserToAssistanceContracts < ActiveRecord::Migration
  def change
    add_column :assistance_contracts, :user_id, :integer
    add_index :assistance_contracts, :user_id
  end
end
