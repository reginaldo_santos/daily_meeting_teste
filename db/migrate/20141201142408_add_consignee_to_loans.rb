class AddConsigneeToLoans < ActiveRecord::Migration
  def change
    add_column :loans, :consignee_id, :integer
    add_index :loans, :consignee_id
  end
end
