class CreateProductConfigurations < ActiveRecord::Migration
  def change
    create_table :product_configurations do |t|
      t.integer :product_id
      t.integer :configuration_id
      t.boolean :value

      t.timestamps
    end
    add_index :product_configurations, :product_id
    add_index :product_configurations, :configuration_id
  end
end
