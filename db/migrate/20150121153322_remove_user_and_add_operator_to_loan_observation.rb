class RemoveUserAndAddOperatorToLoanObservation < ActiveRecord::Migration
  def change
    remove_column :loan_observations, :user_id
    add_column :loan_observations, :operator_id, :integer
    add_index :loan_observations, :operator_id
  end
end
