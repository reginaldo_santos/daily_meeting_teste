class AddLoanToLoanHistories < ActiveRecord::Migration
  def change
    add_column :loan_histories, :loan_id, :integer
    add_index :loan_histories, :loan_id
  end
end
