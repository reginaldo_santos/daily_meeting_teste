class AddFilePathToInsertions < ActiveRecord::Migration
  def change
    add_column :insertions, :file_path, :string
  end
end
