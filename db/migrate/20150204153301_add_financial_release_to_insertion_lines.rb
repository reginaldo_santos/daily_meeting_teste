class AddFinancialReleaseToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :financial_release_id, :integer
    add_index :insertion_lines, :financial_release_id
  end
end
