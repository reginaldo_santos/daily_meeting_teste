class AddFinancialReleaseFileToFinancialRelease < ActiveRecord::Migration
  def change
    add_column :financial_releases, :financial_release_file_id, :integer
    add_index :financial_releases, :financial_release_file_id
  end
end
