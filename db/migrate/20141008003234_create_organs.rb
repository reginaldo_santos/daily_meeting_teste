class CreateOrgans < ActiveRecord::Migration
  def change
    create_table :organs do |t|
      t.string :code
      t.string :name
      t.string :abbreviation

      t.timestamps
    end
  end
end
