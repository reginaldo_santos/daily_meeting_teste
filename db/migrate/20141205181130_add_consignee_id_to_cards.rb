class AddConsigneeIdToCards < ActiveRecord::Migration
  def change
    add_column :cards, :consignee_id, :integer
    add_index :cards, :consignee_id
  end
end
