class ChangeTypeContractTypeLoan < ActiveRecord::Migration
  def change
    change_column :loans, :contract_type, :string
  end
end
