class AddInsertionTypeToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :insertion_type, :string
  end
end
