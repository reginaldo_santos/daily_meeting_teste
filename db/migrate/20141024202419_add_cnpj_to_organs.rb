class AddCnpjToOrgans < ActiveRecord::Migration
  def change
    add_column :organs, :cnpj, :string
  end
end
