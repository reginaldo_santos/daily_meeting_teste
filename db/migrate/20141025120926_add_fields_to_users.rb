class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cpf, :string
    add_column :users, :rg, :string
    add_column :users, :company_phone_number, :string
    add_column :users, :phone_number, :string
    add_column :users, :gender, :string
    add_column :users, :birthdate, :date
    add_column :users, :admission_date, :date
  end
end
