class CreateVariations < ActiveRecord::Migration
  def change
    create_table :variations do |t|
      t.decimal :value
      t.string :category
      t.integer :assistance_contract_id
      t.integer :user_id

      t.timestamps
    end
    add_index :variations, :assistance_contract_id
    add_index :variations, :user_id
  end
end
