class AddLoanIdToLoanObservations < ActiveRecord::Migration
  def change
    add_column :loan_observations, :loan_id, :integer
    add_index :loan_observations, :loan_id
  end
end
