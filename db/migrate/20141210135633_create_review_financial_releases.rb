class CreateReviewFinancialReleases < ActiveRecord::Migration
  def change
    create_table :review_financial_releases do |t|
      t.string :file_name
      t.string :error
      t.string :card_number
      t.string :name
      t.string :cpf
      t.string :registration
      t.string :organ
      t.timestamps
    end
  end
end
