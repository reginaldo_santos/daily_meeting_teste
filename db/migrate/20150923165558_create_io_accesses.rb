class CreateIoAccesses < ActiveRecord::Migration
  def change
    create_table :io_accesses do |t|
      t.boolean :successful, :default => false
      t.boolean :finished, :default => false
      t.string :category

      t.timestamps
    end
  end
end
