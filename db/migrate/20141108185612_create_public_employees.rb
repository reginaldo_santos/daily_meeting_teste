class CreatePublicEmployees < ActiveRecord::Migration
  def change
    create_table :public_employees do |t|
      t.string :name

      t.timestamps
    end
  end
end
