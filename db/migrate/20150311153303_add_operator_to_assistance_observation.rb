class AddOperatorToAssistanceObservation < ActiveRecord::Migration
  def change
    add_column :assistance_observations, :operator_id, :integer
    add_index :assistance_observations, :operator_id
  end
end
