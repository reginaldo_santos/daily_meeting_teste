class RemoveUserAndAddOperatorToCards < ActiveRecord::Migration
  def change
    remove_column :cards, :user_id
    add_column :cards, :operator_id, :integer
    add_index :cards, :operator_id
  end
end
