class RemoveFieldsFromConsignees < ActiveRecord::Migration
  def change
    remove_column :consignees, :deadline
    remove_column :consignees, :event_code
    remove_column :consignees, :product_type_id
  end
end
