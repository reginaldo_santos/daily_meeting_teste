class RemoveUserAndAddOperatorToCardObservations < ActiveRecord::Migration
  def change
    remove_column :card_observations, :user_id
    add_column :card_observations, :operator_id, :integer
    add_index :card_observations, :operator_id
  end
end
