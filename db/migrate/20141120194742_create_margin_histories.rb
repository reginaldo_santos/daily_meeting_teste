class CreateMarginHistories < ActiveRecord::Migration
  def change
    create_table :margin_histories do |t|
      t.date :date
      t.integer :public_employee_id
      t.decimal :margin_value

      t.timestamps
    end
    add_index :margin_histories, :public_employee_id
  end
end
