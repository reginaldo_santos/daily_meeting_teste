class AddSellerToPortabilityRequests < ActiveRecord::Migration
  def change
    add_column :portability_requests, :seller_id, :integer
  end
end
