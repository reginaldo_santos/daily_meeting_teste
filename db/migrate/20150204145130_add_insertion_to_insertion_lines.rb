class AddInsertionToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :insertion_id, :integer
    add_index :insertion_lines, :insertion_id
  end
end
