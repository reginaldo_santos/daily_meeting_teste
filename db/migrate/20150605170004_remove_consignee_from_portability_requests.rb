class RemoveConsigneeFromPortabilityRequests < ActiveRecord::Migration
  def change
    remove_column :portability_requests, :consignee_id
  end
end
