class AddLiteracyIdToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :literacy_id, :integer
    add_index :public_employees, :literacy_id
  end
end
