class CreateAttendanceLocals < ActiveRecord::Migration
  def change
    create_table :attendance_locals do |t|
      t.string :name
      t.string :cnpj
      t.string :street
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country
      t.integer :consignee_id

      t.timestamps
    end
  end
end
