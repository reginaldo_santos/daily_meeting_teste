class AddObservationSellerToPortabilityRequests < ActiveRecord::Migration
  def change
    add_column :portability_requests, :observation_seller, :text
  end
end
