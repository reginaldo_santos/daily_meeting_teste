class AddPaidInstallmentsToAssistanceContracts < ActiveRecord::Migration
  def change
    add_column :assistance_contracts, :paid_installments, :integer, :default => 0
  end
end
