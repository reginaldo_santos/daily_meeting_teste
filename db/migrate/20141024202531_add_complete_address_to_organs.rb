class AddCompleteAddressToOrgans < ActiveRecord::Migration
  def change
    add_column :organs, :street, :string
    add_column :organs, :number, :string
    add_column :organs, :complement, :string
    add_column :organs, :neighborhood, :string
    add_column :organs, :zip_code, :string
    add_column :organs, :city, :string
    add_column :organs, :state, :string
    add_column :organs, :country, :string
  end
end
