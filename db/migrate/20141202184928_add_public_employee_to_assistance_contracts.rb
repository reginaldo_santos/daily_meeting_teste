class AddPublicEmployeeToAssistanceContracts < ActiveRecord::Migration
  def change
    add_column :assistance_contracts, :public_employee_id, :integer
    add_index :assistance_contracts, :public_employee_id
  end
end
