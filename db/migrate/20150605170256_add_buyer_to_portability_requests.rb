class AddBuyerToPortabilityRequests < ActiveRecord::Migration
  def change
    add_column :portability_requests, :buyer_id, :integer
  end
end
