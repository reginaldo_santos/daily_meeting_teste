class RemoveUserAndAddOperatorToLoans < ActiveRecord::Migration
  def change
    remove_column :loans, :user_id
    add_column :loans, :operator_id, :integer
    add_index :loans, :operator_id
  end
end
