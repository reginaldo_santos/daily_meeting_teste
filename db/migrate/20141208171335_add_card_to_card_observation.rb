class AddCardToCardObservation < ActiveRecord::Migration
  def change
    add_column :card_observations, :card_id, :integer
    add_index :card_observations, :card_id
  end
end
