class CreateAssistanceObservations < ActiveRecord::Migration
  def change
    create_table :assistance_observations do |t|
      t.text :text

      t.timestamps
    end
  end
end
