class RemoveAbbreviationFromOrgans < ActiveRecord::Migration
  def change
    remove_column :organs, :abbreviation
  end
end
