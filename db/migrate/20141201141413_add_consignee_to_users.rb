class AddConsigneeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :consignee_id, :integer
    add_index :users, :consignee_id
  end
end
