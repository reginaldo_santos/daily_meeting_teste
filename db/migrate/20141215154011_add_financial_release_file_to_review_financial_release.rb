class AddFinancialReleaseFileToReviewFinancialRelease < ActiveRecord::Migration
  def change
    add_column :review_financial_releases, :financial_release_file_id, :integer
    add_index :review_financial_releases, :financial_release_file_id
  end
end
