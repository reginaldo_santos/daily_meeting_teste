class AddConsigneeAndAttendanceLocalToOperators < ActiveRecord::Migration
  def change
    add_column :operators, :consignee_id, :integer
    add_column :operators, :attendance_local_id, :integer
    add_index :operators, :consignee_id
    add_index :operators, :attendance_local_id
  end
end
