class AddStatusToNegotiations < ActiveRecord::Migration
  def change
    add_column :negotiations, :status, :string
  end
end
