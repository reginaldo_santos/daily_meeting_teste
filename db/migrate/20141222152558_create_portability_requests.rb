class CreatePortabilityRequests < ActiveRecord::Migration
  def change
    create_table :portability_requests do |t|
      t.string :status
      t.integer :loan_id
      t.integer :consignee_id

      t.timestamps
    end
    add_index :portability_requests, :loan_id
    add_index :portability_requests, :consignee_id
  end
end
