class AddInterestTaxToLoan < ActiveRecord::Migration
  def change
    add_column :loans, :interest_tax, :decimal
  end
end
