class AddPaidInstallmentsToVariations < ActiveRecord::Migration
  def change
    add_column :variations, :paid_installments, :integer
  end
end
