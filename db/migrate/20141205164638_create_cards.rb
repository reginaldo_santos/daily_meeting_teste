class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :proposal_number
      t.decimal :discount_value
      t.integer :public_employee_id
      t.integer :user_id

      t.timestamps
    end
    add_index :cards, :public_employee_id
    add_index :cards, :user_id
  end
end
