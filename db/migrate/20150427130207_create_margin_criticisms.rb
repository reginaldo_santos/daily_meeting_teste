class CreateMarginCriticisms < ActiveRecord::Migration
  def change
    create_table :margin_criticisms do |t|
      t.string :file_name
      t.string :text

      t.timestamps
    end
  end
end
