class AddStatusToFinancialReleases < ActiveRecord::Migration
  def change
    add_column :financial_releases, :status, :string
  end
end
