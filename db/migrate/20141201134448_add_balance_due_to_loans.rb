class AddBalanceDueToLoans < ActiveRecord::Migration
  def change
    add_column :loans, :balance_due, :decimal
  end
end
