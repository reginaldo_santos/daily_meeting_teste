class RemoveUserAndAddOperatorToAssistanceContract < ActiveRecord::Migration
  def change
    remove_column :assistance_contracts, :user_id
    add_column :assistance_contracts, :operator_id, :integer
    add_index :assistance_contracts, :operator_id
  end
end
