class CreateLoanHistories < ActiveRecord::Migration
  def change
    create_table :loan_histories do |t|
      t.integer :public_employee_id
      t.integer :user_id
      t.decimal :contract_value
      t.decimal :liquid_value
      t.integer :deadline
      t.string :status
      t.string :contract_type
      t.datetime :created
      t.datetime :updated
      t.decimal :balance_due
      t.integer :consignee_id
      t.integer :installments_paid
      t.decimal :installment_value
      t.timestamps
    end
  end
end
