class AddOthersToLoanHistories < ActiveRecord::Migration
  def change
    add_column :loan_histories, :others, :decimal
  end
end
