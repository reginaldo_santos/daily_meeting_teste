class AddPortabilityCodeToPortabilityRequests < ActiveRecord::Migration
  def change
    add_column :portability_requests, :portability_code, :string
  end
end
