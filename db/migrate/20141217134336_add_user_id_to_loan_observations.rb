class AddUserIdToLoanObservations < ActiveRecord::Migration
  def change
    add_column :loan_observations, :user_id, :integer
    add_index :loan_observations, :user_id
  end
end
