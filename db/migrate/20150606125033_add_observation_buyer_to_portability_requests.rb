class AddObservationBuyerToPortabilityRequests < ActiveRecord::Migration
  def change
    add_column :portability_requests, :observation_buyer, :text
  end
end
