class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login, null: false, :limit => 50
      t.string :email, null: false, :limit => 100
      t.string :name, null: false, :limit => 100
      t.integer :login_attempts, null: false

      t.timestamps
    end
  end
end
