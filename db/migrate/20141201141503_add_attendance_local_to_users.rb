class AddAttendanceLocalToUsers < ActiveRecord::Migration
  def change
    add_column :users, :attendance_local_id, :integer
    add_index :users, :attendance_local_id
  end
end
