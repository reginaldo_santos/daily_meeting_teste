class RemoveDateFromMarginHistories < ActiveRecord::Migration
  def change
    remove_column :margin_histories, :date
  end
end
