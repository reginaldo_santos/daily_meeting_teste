class AddBalanceDueReleaseToLoans < ActiveRecord::Migration
  def change
    add_column :loans, :balance_due_release, :decimal
  end
end
