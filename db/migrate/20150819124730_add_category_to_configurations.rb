class AddCategoryToConfigurations < ActiveRecord::Migration
  def change
    add_column :configurations, :category, :string
  end
end
