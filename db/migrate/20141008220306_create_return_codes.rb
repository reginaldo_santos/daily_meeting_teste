class CreateReturnCodes < ActiveRecord::Migration
  def change
    create_table :return_codes do |t|
      t.string :code
      t.string :description
      t.text :obs

      t.timestamps
    end
  end
end
