class AddInstallmentValueToLoans < ActiveRecord::Migration
  def change
    add_column :loans, :installment_value, :decimal
  end
end
