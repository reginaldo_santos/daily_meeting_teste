class AddContractNumberToLoanHistories < ActiveRecord::Migration
  def change
    add_column :loan_histories, :contract_number, :string
  end
end
