class AddOriginOperationToLoans < ActiveRecord::Migration
  def change
    add_column :loans, :origin_operation, :string
  end
end
