class RemoveUserAndAddOperatorToVariations < ActiveRecord::Migration
  def change
    remove_column :variations, :user_id
    add_column :variations, :operator_id, :integer
    add_index :variations, :operator_id
  end
end
