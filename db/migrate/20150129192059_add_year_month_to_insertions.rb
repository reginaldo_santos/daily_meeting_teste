class AddYearMonthToInsertions < ActiveRecord::Migration
  def change
    add_column :insertions, :month, :integer
    add_column :insertions, :year, :integer
  end
end
