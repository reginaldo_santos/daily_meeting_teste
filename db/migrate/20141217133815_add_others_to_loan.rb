class AddOthersToLoan < ActiveRecord::Migration
  def change
    add_column :loans, :others, :decimal
  end
end
