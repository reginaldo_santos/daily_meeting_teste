class AddLineToReviewFinancialReleases < ActiveRecord::Migration
  def change
  	add_column :review_financial_releases, :line, :integer
  end
end
