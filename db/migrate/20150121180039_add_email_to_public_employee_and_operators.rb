class AddEmailToPublicEmployeeAndOperators < ActiveRecord::Migration
  def change
    add_column :public_employees, :email, :string
    add_column :operators, :email, :string
  end
end
