class CreateAuthorizations < ActiveRecord::Migration
  def change
    create_table :authorizations do |t|
      t.integer :role_id
      t.integer :user_id

      t.timestamps
    end
    add_index :authorizations, :role_id
    add_index :authorizations, :user_id
  end
end
