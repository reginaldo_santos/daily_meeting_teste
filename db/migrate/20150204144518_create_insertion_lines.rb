class CreateInsertionLines < ActiveRecord::Migration
  def change
    create_table :insertion_lines do |t|
      t.string :year
      t.string :month
      t.string :organ_code
      t.string :registration
      t.string :event_code
      t.string :discount_value
      t.string :blank_field
      t.string :cpf
      t.string :contract_number
      t.string :uso_emp
      t.integer :installment_to_pay
      t.integer :deadline
      t.string :segment
      t.string :final_fill
      t.string :margem_facil_code
      t.timestamps
    end
  end
end
