class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :event_code
      t.integer :deadline

      t.timestamps
    end
  end
end
