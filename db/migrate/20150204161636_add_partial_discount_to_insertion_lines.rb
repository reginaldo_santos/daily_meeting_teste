class AddPartialDiscountToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :partial_discount, :boolean
  end
end
