class CreateFinancialReleaseFiles < ActiveRecord::Migration
  def change
    create_table :financial_release_files do |t|
      t.string :name
      t.timestamps
    end
  end
end
