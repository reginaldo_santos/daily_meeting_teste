class CreateAssistanceContracts < ActiveRecord::Migration
  def change
    create_table :assistance_contracts do |t|

      t.decimal :discount_value
      t.string :status
      t.timestamps
    end
  end
end
