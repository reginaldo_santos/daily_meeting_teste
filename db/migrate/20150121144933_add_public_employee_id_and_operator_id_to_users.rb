class AddPublicEmployeeIdAndOperatorIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :public_employee_id, :integer
    add_column :users, :operator_id, :integer
    add_index :users, :public_employee_id
    add_index :users, :operator_id
  end
end
