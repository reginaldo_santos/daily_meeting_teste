class RemoveUserAndAddOperatorToFinancialReleases < ActiveRecord::Migration
  def change
    remove_column :financial_releases, :user_id
    add_column :financial_releases, :operator_id, :integer
    add_index :financial_releases, :operator_id
  end
end
