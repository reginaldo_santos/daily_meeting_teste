class AddContractNumberToAssistanceContracts < ActiveRecord::Migration
  def change
    add_column :assistance_contracts, :contract_number, :string
  end
end
