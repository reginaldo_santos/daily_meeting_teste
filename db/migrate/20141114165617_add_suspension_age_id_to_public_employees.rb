class AddSuspensionAgeIdToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :suspension_age_id, :integer
    add_index :public_employees, :suspension_age_id
  end
end
