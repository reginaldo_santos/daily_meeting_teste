class RemoveUserAndAddOperatorToLoanHistory < ActiveRecord::Migration
  def change
    remove_column :loan_histories, :user_id
    add_column :loan_histories, :operator_id, :integer
  end
end
