class AddStatusToVariation < ActiveRecord::Migration
  def change
    add_column :variations, :status, :string
  end
end
