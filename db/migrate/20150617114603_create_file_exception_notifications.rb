class CreateFileExceptionNotifications < ActiveRecord::Migration
  def change
    create_table :file_exception_notifications do |t|
      t.string :file_type
      t.boolean :fail

      t.timestamps
    end
  end
end
