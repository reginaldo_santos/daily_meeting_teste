class CreateFinancialReleases < ActiveRecord::Migration
  def change
    create_table :financial_releases do |t|
      t.integer :card_id
      t.decimal :value

      t.timestamps
    end
    add_index :financial_releases, :card_id
  end
end
