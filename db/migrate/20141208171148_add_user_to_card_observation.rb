class AddUserToCardObservation < ActiveRecord::Migration
  def change
    add_column :card_observations, :user_id, :integer
    add_index :card_observations, :user_id
  end
end
