class AddFatherIdToLoanHistories < ActiveRecord::Migration
  def change
    add_column :loan_histories, :father_id, :integer
  end
end
