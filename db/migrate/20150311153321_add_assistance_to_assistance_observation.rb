class AddAssistanceToAssistanceObservation < ActiveRecord::Migration
  def change
    add_column :assistance_observations, :assistance_contract_id, :integer
    add_index :assistance_observations, :assistance_contract_id
  end
end
