class AddInterestTaxToLoanHistories < ActiveRecord::Migration
  def change
    add_column :loan_histories, :interest_tax, :decimal
  end
end
