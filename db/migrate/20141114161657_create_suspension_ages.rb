class CreateSuspensionAges < ActiveRecord::Migration
  def change
    create_table :suspension_ages do |t|
      t.string :name
      t.integer :file_reference

      t.timestamps
    end
  end
end
