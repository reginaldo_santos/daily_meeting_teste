class AddProductTypeIdToConsignees < ActiveRecord::Migration
  def change
    add_column :consignees, :product_type_id, :integer
    add_index :consignees, :product_type_id
  end
end
