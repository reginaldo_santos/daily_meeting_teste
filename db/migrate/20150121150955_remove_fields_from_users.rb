class RemoveFieldsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :name
    remove_column :users, :cpf
    remove_column :users, :company_phone_number
    remove_column :users, :consignee_id
    remove_column :users, :attendance_local_id
  end
end
