class AddSectionToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :section, :string
  end
end
