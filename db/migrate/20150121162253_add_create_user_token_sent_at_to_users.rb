class AddCreateUserTokenSentAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :create_user_token_sent_at, :datetime
  end
end
