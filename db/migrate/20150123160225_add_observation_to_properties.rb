class AddObservationToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :observation, :text
  end
end
