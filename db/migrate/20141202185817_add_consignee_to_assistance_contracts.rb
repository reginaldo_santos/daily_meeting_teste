class AddConsigneeToAssistanceContracts < ActiveRecord::Migration
  def change
    add_column :assistance_contracts, :consignee_id, :integer
    add_index :assistance_contracts, :consignee_id
  end
end
