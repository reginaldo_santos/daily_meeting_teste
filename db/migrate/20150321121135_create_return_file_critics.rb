class CreateReturnFileCritics < ActiveRecord::Migration
  def change
    create_table :return_file_critics do |t|
      t.string :file_name
      t.string :observation

      t.timestamps
    end
  end
end
