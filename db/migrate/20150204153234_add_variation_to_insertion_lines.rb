class AddVariationToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :variation_id, :integer
    add_index :insertion_lines, :variation_id
  end
end
