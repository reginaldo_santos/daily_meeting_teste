class AddMaritalStatusIdToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :marital_status_id, :integer
    add_index :public_employees, :marital_status_id
  end
end
