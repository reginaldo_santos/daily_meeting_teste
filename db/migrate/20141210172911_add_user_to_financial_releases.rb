class AddUserToFinancialReleases < ActiveRecord::Migration
  def change
    add_column :financial_releases, :user_id, :integer
    add_index :financial_releases, :user_id
  end
end
