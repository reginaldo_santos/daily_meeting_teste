class CreateLiteracies < ActiveRecord::Migration
  def change
    create_table :literacies do |t|
      t.string :name

      t.timestamps
    end
  end
end
