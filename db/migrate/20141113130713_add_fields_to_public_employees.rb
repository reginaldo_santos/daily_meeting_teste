class AddFieldsToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :cpf, :string
    add_column :public_employees, :organ_id, :integer
    add_column :public_employees, :registration, :string
    add_column :public_employees, :admission_date, :date
    add_column :public_employees, :situation, :string
    add_column :public_employees, :end_contract_date, :date
    add_column :public_employees, :suspension_age, :string
    add_column :public_employees, :margin_value, :decimal, precision: 7, scale: 2

    add_column :public_employees, :street, :string
    add_column :public_employees, :number, :string
    add_column :public_employees, :complement, :string
    add_column :public_employees, :neighborhood, :string
    add_column :public_employees, :city, :string
    add_column :public_employees, :zip_code, :string

    add_column :public_employees, :phone, :string
    add_column :public_employees, :birth_date, :date
    add_column :public_employees, :marital_status, :string
    add_column :public_employees, :voter_registration, :string
    add_column :public_employees, :zone, :string
    add_column :public_employees, :gender, :string
    add_column :public_employees, :rg, :string
    add_column :public_employees, :dispatcher_organ, :string
    add_column :public_employees, :dispatcher_uf, :string
    add_column :public_employees, :role, :string
    add_index :public_employees, :organ_id
  end
end
