class CreateLoanObservations < ActiveRecord::Migration
  def change
    create_table :loan_observations do |t|
      t.text :text
      t.timestamps
    end
  end
end
