class RemoveFieldsFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :rg
    remove_column :users, :phone_number
    remove_column :users, :gender
    remove_column :users, :birthdate
    remove_column :users, :admission_date
  end
end
