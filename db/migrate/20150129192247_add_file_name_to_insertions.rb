class AddFileNameToInsertions < ActiveRecord::Migration
  def change
    add_column :insertions, :file_name, :string
  end
end
