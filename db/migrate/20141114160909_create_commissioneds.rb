class CreateCommissioneds < ActiveRecord::Migration
  def change
    create_table :commissioneds do |t|
      t.string :name

      t.timestamps
    end
  end
end
