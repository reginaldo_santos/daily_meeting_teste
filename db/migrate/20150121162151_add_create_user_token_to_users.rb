class AddCreateUserTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :create_user_token, :string
  end
end
