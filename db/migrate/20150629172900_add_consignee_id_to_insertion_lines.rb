class AddConsigneeIdToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :consignee_id, :integer
    add_index :insertion_lines, :consignee_id
  end
end
