class AddLoanToInsertionLines < ActiveRecord::Migration
  def change
    add_column :insertion_lines, :loan_id, :integer
    add_index :insertion_lines, :loan_id
  end
end
