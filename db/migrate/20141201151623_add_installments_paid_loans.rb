class AddInstallmentsPaidLoans < ActiveRecord::Migration
  def change
    add_column :loans, :installments_paid, :integer, default: 0
  end
end
