class AddConsigneeIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :consignee_id, :integer
    add_index :products, :consignee_id
  end
end
