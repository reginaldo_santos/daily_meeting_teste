class AddSituationIdToPublicEmployees < ActiveRecord::Migration
  def change
    add_column :public_employees, :situation_id, :integer
    add_index :public_employees, :situation_id
  end
end
