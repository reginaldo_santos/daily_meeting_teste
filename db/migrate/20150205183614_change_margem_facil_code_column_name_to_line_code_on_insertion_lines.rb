class ChangeMargemFacilCodeColumnNameToLineCodeOnInsertionLines < ActiveRecord::Migration
  def change
    rename_column :insertion_lines, :margem_facil_code, :line_code
  end
end
