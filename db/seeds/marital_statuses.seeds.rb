puts 'Adicionando Estado Civil'

Seeds.add MaritalStatus, {name: 'Solteiro'}
Seeds.add MaritalStatus, {name: 'Casado'}
Seeds.add MaritalStatus, {name: 'Desquitado'}
Seeds.add MaritalStatus, {name: 'Divorciado'}
Seeds.add MaritalStatus, {name: 'Viúva'}
Seeds.add MaritalStatus, {name: 'Outras'}