puts 'Adicionando grau de instrução'

Seeds.add Literacy, {name: 'Analfabeto'}
Seeds.add Literacy, {name: 'Até a 4ª série do 1º grau incompleto'}
Seeds.add Literacy, {name: 'Até a 4ª série do 1º grau completo'}
Seeds.add Literacy, {name: '5ª a 8ª série do 1º grau incompleto'}
Seeds.add Literacy, {name: '1º grau completo'}
Seeds.add Literacy, {name: '2º grau incompleto'}
Seeds.add Literacy, {name: '2º grau completo'}
Seeds.add Literacy, {name: 'Superior incompleto'}
Seeds.add Literacy, {name: 'Superior completo'}
