puts 'Adicionando idades de suspenção'

Seeds.add SuspensionAge, {name: 'Pensão por tempo indeterminado', file_reference: 0}
Seeds.add SuspensionAge, {name: 'Pensão até os 18 anos', file_reference: 1}
Seeds.add SuspensionAge, {name: 'Pensão até os 21 anos', file_reference: 2}
