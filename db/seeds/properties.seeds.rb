puts 'Adicionando Propriedades'

Seeds.add Property, {name: Property::IN_GROUP, value: Property::IN_GROUP, observation: 'estilo de calculo de margem'}
Seeds.add Property, {name: Property::CARD, value: '0.25', observation: 'margem cartao'}
Seeds.add Property, {name: Property::PRODUCT_GROUP, value: '0.75', observation: 'product group'}
Seeds.add Property, {name: Property::PREFECTURE_NAME, value: 'Sape', observation: 'Nome do convenio'}
Seeds.add Property, {name: 'DESCONTO PARCIAL', value: '0', observation: 'Desconto parcial no arquivo de inserção. 1 - Ativa, 0 - Desativa', category: Property::PARTIAL_DISCOUNT}
Seeds.add Property, {name: 'Dia de corte', value: '30', observation: 'Dia de corte da geração do arquivo de inserção'}
Seeds.add Property, {name: 'Prioridade do empréstimo', value: '2', observation: 'Ordem de prioridade do emprestimo no arquivo de inserção, poder ir de 1 até 4, mas não pode ter a mesma prioridade de outro produto', category: Property::INSERTION_PRIORITY}
Seeds.add Property, {name: 'Prioridade do contrato assistencial', value: '1', observation: 'Ordem de prioridade do contrato assistencial no arquivo de inserção, poder ir de 1 até 4, mas não pode ter a mesma prioridade de outro produto', category: Property::INSERTION_PRIORITY}
Seeds.add Property, {name: 'Prioridade do cartão', value: '3', observation: 'Ordem de prioridade do cartão no arquivo de inserção, poder ir de 1 até 4, mas não pode ter a mesma prioridade de outro produto', category: Property::INSERTION_PRIORITY}
Seeds.add Property, {name: 'Nome do sistema', value: 'SISTEMA MARGEMFACIL', observation: 'Nome do sistema a ser inserido em e-mails ou documentos'}
Seeds.add Property, {name: 'Telefone de contato suporte ao usuário.', value: ', {85}8170.0722', observation: 'Telefone que será disponibilizado no sistema diretamente aos usuários.'}
Seeds.add Property, {name: 'Nome do convênio contratante.', value: 'Sape', observation: 'Nome do convênio que será exibido em relatórios.'}
Seeds.add Property, {name: 'E-mail de contato suporte ao usuário', value: 'suporte@margemfacil.com.br', observation: 'E-mail que será disponibilizado no sistema diretamente aos usuários.'}
Seeds.add Property, {name: 'Arquivo de inserção consolidado', value: '0', observation: '1 - Permitir download 0 - Não permitir download'}
Seeds.add Property, {name: 'Mudança de senha obrigatória.', value: '30', observation: 'Dias para obrigatoriedade de alteração de senha dos usuários. 0 - Destiva verificação.'}
Seeds.add Property, {name: 'Limite de Operação - Empréstimo', value: '0', observation: 'Limita a quantidade de emprestimo por servidor', category: Property::LOAN_LIMIT}
Seeds.add Property, {name: 'Limite de Operação - Cartão', value: '999', observation: 'Limita a quantidade de cartões por servidor', category: Property::CARD_LIMIT}
Seeds.add Property, {name: 'Limite de Operação - Assistencial', value: '999', observation: 'Limita a quantidade de contratos assistenciais por servidor', category: Property::ASSISTANCE_LIMIT}