puts 'Adicionando códigos de retorno'

Seeds.add ReturnCode, {code: '00', description: 'AVERBADO'}
Seeds.add ReturnCode, {code: '07', description: 'ERRO DE CRITICA PRE-FOLHA'}
Seeds.add ReturnCode, {code: '51', description: 'MATRICULA INEXISTENTE'}
Seeds.add ReturnCode, {code: '52', description: 'SERVIDOR FALECIDO'}
Seeds.add ReturnCode, {code: '53', description: 'VERBA BLOQUEADA NO AUTO'}
Seeds.add ReturnCode, {code: '54', description: 'SERVIDOR AFASTADO'}
Seeds.add ReturnCode, {code: '56', description: 'FAC. SALDO INSUFICIENTE'}
Seeds.add ReturnCode, {code: '57', description: 'OPD. SALDO INSUFICIENTE'}