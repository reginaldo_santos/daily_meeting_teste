puts 'Adicionando configurações'

Seeds.add Configuration, {name: 'Reserva/Aprovação Simultanea - Empréstimo', observation: 'Ao resevar a margem o contrato irá direto para aprovado por consignataria', category: 'LOAN'}
Seeds.add Configuration, {name: 'Reserva/Aprovação Simultanea - Assistencial', observation: 'Ao resevar a margem o contrato irá direto para aprovado por consignataria', category: 'ASSISTANCE'}
Seeds.add Configuration, {name: 'Reserva/Aprovação Simultanea - Cartão', observation: 'Ao resevar a margem o contrato irá direto para aprovado por consignataria', category: 'CARD'}