puts 'Adicionando servidores'

Seeds.add PublicEmployee, {name: 'Reginaldo', cpf:'11111111111', organ_id: 1, registration: '20091705',
                          admission_date: '1990-09-20', situation_id: 1, margin_value: 0.0, email: 'reginaldofrozen@gmail.com',
                          commissioned_id: 1, suspension_age_id: 1, end_contract_date: '2050-09-20', approval_password: Encryption.encrypt('12345')}
Seeds.add PublicEmployee, {name: 'Reginaldo Santos', cpf:'22222222222', organ_id: 8, registration: '20091705',
                          admission_date: '1990-09-20', situation_id: 1, margin_value: 4030.0,
                          commissioned_id: 1, suspension_age_id: 1, end_contract_date: '2050-09-20', approval_password: Encryption.encrypt('12345')}
