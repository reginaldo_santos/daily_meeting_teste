after "development:employees", "properties", 'development:operators' do
  puts 'Adicionando contratos assistenciais'

  Seeds.add AssistanceContract, {discount_value: 20.0, public_employee_id: 2, consignee_id: 1, operator_id: 2, approval_password: '12345'}
  Seeds.add AssistanceContract, {discount_value: 2.0, public_employee_id: 2, consignee_id: 1, operator_id: 2, approval_password: '12345'}

  AssistanceContract.all.each do |assistance |
    assistance.update(:status => AssistanceContract::APROVADO)
  end
end
