after "development:operators" do
  puts 'Adicionando usuários'

  Seeds.add User, { login: 'admin', login_attempts: 0, password: '23052013', password_confirmation: '23052013', blocked: false, operator_id: 1, last_password_change: Time.now, roles_mask: 1, role_ids: [1] }
  Seeds.add User, { login: 'lmaster', login_attempts: 0, password: '12345', password_confirmation: '12345', blocked: false, operator_id: 2, last_password_change: Time.now, roles_mask: 1, role_ids: [6, 12, 13, 14, 15] }
  Seeds.add User, { login: 'lmaster2', login_attempts: 0, password: '12345', password_confirmation: '12345', blocked: false, operator_id: 3, last_password_change: Time.now, roles_mask: 1, role_ids: [6, 12, 13, 14, 15] }
end
