puts 'Adicionando consignatárias'

Seeds.add Consignee, {name: 'consig - emprestimo', cnpj: '111111111111', street: 'teste', number:'40', complement: 'teste', neighborhood: 'teste', zip_code: '60712240',
                     city: 'teste', state: 'teste', country: 'teste', contact: 'teste', post: 'teste', phone_number: '3430008899'}
Seeds.add Consignee, {name: 'consig - assistance', cnpj: '222222222222', street: 'teste', number:'40', complement: 'teste', neighborhood: 'teste', zip_code: '60712240',
                     city: 'teste', state: 'teste', country: 'teste', contact: 'teste', post: 'teste', phone_number: '3430008899'}
Seeds.add Consignee, {name: 'consig - cartão', cnpj: '333333333333', street: 'teste', number:'40', complement: 'teste', neighborhood: 'teste', zip_code: '60712240',
                     city: 'teste', state: 'teste', country: 'teste', contact: 'teste', post: 'teste', phone_number: '3430008899'}
