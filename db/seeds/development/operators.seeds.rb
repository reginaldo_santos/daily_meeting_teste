after 'development:consignees' do
	puts 'Adicionando operadores'

	Seeds.add Operator, { name: 'admin', cpf: '11111111111', company_phone_number: '8545678976', email: 'reginaldo.freitas20@gmail.com' }
	Seeds.add Operator, { name: 'lmaster', cpf: '11111111112', company_phone_number: '8545678976', email: 'reginaldo.freitas20@gmail.com', consignee_id: 1 }
	Seeds.add Operator, { name: 'lmaster2', cpf: '11111111113', company_phone_number: '8545678976', email: 'reginaldo.freitas20@gmail.com', consignee_id: 2 }
end