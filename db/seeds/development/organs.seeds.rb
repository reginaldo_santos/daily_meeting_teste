puts 'Adicionando órgãos'

Seeds.add Organ, {code: '1', name: 'SEC DE EDUCAÇÃO', cnpj: '11111111111111'}
Seeds.add Organ, {code: '2', name: 'SEC DE ADMINISTRAÇÃO', cnpj: '11111111111112'}
Seeds.add Organ, {code: '3', name: 'SEC CULTURA, ESPORTE E TURISMO', cnpj: '11111111111113'}
Seeds.add Organ, {code: '4', name: 'SEC ASSISTENCIA SOCIAL', cnpj: '11111111111114'}
Seeds.add Organ, {code: '6', name: 'SEC DESENV ECON AGRICOLA', cnpj: '11111111111115'}
Seeds.add Organ, {code: '7', name: 'GABINETE DO PREFEITO', cnpj: '11111111111116'}
Seeds.add Organ, {code: '8', name: 'SEC EXTRAORDINARIA', cnpj: '11111111111117'}
Seeds.add Organ, {code: '11', name: 'SEC DE TRANSPORTE', cnpj: '11111111111119'}
Seeds.add Organ, {code: '10', name: 'SECRETARIA DE SAUDE', cnpj: '11111111111118'}
Seeds.add Organ, {code: '12', name: 'SEC DE AGRICULTURA', cnpj: '11111111111110'}
Seeds.add Organ, {code: '13', name: 'Outros'}
