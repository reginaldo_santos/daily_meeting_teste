after "development:employees", "properties" do

  puts 'Adicionando empréstimos'

  Seeds.add Loan, {contract_value: 300.0, liquid_value: 300.0, deadline: 30, installment_value: 340.0, public_employee_id: 2, operator_id: 2, consignee_id: 1, installments_paid: 0, contract_number: 'qwerty', origin_operation: 'NEW', approval_password: '12345'}
  Seeds.add Loan, {contract_value: 10.0, liquid_value: 10.0, deadline: 30, installment_value: 10.0, public_employee_id: 2, operator_id: 3, consignee_id: 2, installments_paid: 0, contract_number: 'asdf', origin_operation: 'NEW', approval_password: '12345'}


  Loan.all.each do |l|
    l.update(:status => Loan::EM_FOLHA)
  end
end