after "product_types" do
  puts 'Adicionando produtos'

  Seeds.add Product, {event_code: 'EMP', deadline: 20, product_type_id:4, consignee_id: 1}
  Seeds.add Product, {event_code: 'ASSIST', deadline: 15, product_type_id:1, consignee_id: 1}
  Seeds.add Product, {event_code: 'CARD', deadline: 50, product_type_id:2, consignee_id: 1}
  Seeds.add Product, {event_code: 'EMP2', deadline: 20, product_type_id:4, consignee_id: 2}
  Seeds.add Product, {event_code: 'ASSIST2', deadline: 15, product_type_id:1, consignee_id: 2}
  Seeds.add Product, {event_code: 'CARD2', deadline: 50, product_type_id:2, consignee_id: 2}
end
