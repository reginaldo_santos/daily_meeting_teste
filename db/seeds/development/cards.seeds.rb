after 'development:employees', "properties", 'development:operators' do
  puts 'Adicionando cartões'

  Seeds.add Card, {proposal_number: '123e', discount_value: 10.0, public_employee_id: 2, operator_id: 2, status: Card::AUTORIZADO_CONSIGNATARIA, consignee_id:1, approval_password: '12345'}

  if !Card.first.nil?
    Card.first.update(:status => Card::AUTORIZADO_CONSIGNATARIA)
    Card.first.update(:discount_value => 190.0)
  end
end
