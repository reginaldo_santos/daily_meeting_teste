require 'rails_helper'

RSpec.describe Commissioned, :type => :model do

  it 'has a valid factory' do
    expect(build(:commissioned)).to be_valid
  end

  it 'is invalid without name' do
    commissioned = build(:commissioned, name: nil)
    commissioned.valid?
    expect(commissioned.errors[:name]).to include('não pode ficar em branco')
  end
end
