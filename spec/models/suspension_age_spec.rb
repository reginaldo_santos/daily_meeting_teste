require 'rails_helper'

RSpec.describe SuspensionAge, :type => :model do

  it 'has a valid factory' do
    expect(build(:suspension_age)).to be_valid
  end

  it 'is invalid without name' do
    suspension = build(:suspension_age, name: nil)
    suspension.valid?
    expect(suspension.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid without file_reference ' do
    suspension = build(:suspension_age, file_reference: nil)
    suspension.valid?
    expect(suspension.errors[:file_reference]).to include('não pode ficar em branco')
  end

end
