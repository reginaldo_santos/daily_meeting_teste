require 'rails_helper'

RSpec.describe Literacy, :type => :model do

  it 'has a valid factory' do
    expect(build(:literacy)).to be_valid
  end

  it 'is invalid without name' do
    literacy = build(:literacy, name: nil)
    literacy.valid?
    expect(literacy.errors[:name]).to include('não pode ficar em branco')
  end
end
