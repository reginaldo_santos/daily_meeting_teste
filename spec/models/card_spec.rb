require 'rails_helper'

RSpec.describe Card, :type => :model do

  before :each do
    create(:product_group_property)
    create(:card_property)
  end

  it 'has a valid factory' do
    expect(build(:card)).to be_valid
  end

  it 'is invalid without proposal number' do
    card = build(:card, proposal_number: nil)
    card.valid?
    expect(card.errors[:proposal_number]).to include('não pode ficar em branco')
  end

  it 'is invalid without discount value' do
    card = build(:card, discount_value: nil)
    card.valid?
    expect(card.errors[:discount_value]).to include('não pode ficar em branco')
  end

  it 'is invalid without consignee' do
    card = build(:card, consignee_id: nil)
    card.valid?
    expect(card.errors[:consignee_id]).to include('não pode ficar em branco')
  end

  it 'is invalid without operator' do
    card = build(:card, operator_id: nil)
    card.valid?
    expect(card.errors[:operator_id]).to include('não pode ficar em branco')
  end

  it 'is invalid with duplicate proposal number'
  it 'is invalid with discount value greater than margin value'
  it 'is invalid with the wrong approval password'
  it 'has status equal to Loan::AGUARDANDO_SERVIDOR after create without approval password'
  it 'has status equal to Loan::AUTORIZADO_SERVIDOR after create with approval password'
end
