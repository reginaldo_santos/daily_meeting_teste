require 'rails_helper'

RSpec.describe Property, :type => :model do

  it 'has a valid factory' do
    expect(build(:card_property)).to be_valid
  end

  it 'is invalid without value' do
    property = build(:card_property, value: nil)
    property.valid?
    expect(property.errors[:value]).to include('não pode ficar em branco')
  end

  it 'is invalid without observation' do
    property = build(:card_property, observation: nil)
    property.valid?
    expect(property.errors[:observation]).to include('não pode ficar em branco')
  end
end
