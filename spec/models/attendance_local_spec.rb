require 'rails_helper'

RSpec.describe AttendanceLocal, :type => :model do

  it 'has a valid factory' do
    expect(build(:attendance_local)).to be_valid
  end

  it 'is invalid without name' do
    local = build(:attendance_local, name: nil)
    local.valid?
    expect(local.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid without consignee' do
    local = build(:attendance_local, consignee_id: nil)
    local.valid?
    expect(local.errors[:consignee_id]).to include('não pode ficar em branco')
  end
end
