require 'rails_helper'

RSpec.describe PublicEmployee, :type => :model do

  it 'has a valid factory' do
    expect(build(:public_employee)).to be_valid
  end

  it 'is invalid without name' do
    employee = build(:public_employee, name: nil)
    employee.valid?
    expect(employee.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid without cpf' do
    employee = build(:public_employee, cpf: nil)
    employee.valid?
    expect(employee.errors[:cpf]).to include('não pode ficar em branco')
  end

  it 'is invalid without organ' do
    employee = build(:public_employee, organ_id: nil)
    employee.valid?
    expect(employee.errors[:organ_id]).to include('não pode ficar em branco')
  end

  it 'is invalid without registration' do
    employee = build(:public_employee, registration: nil)
    employee.valid?
    expect(employee.errors[:registration]).to include('não pode ficar em branco')
  end

  it 'is invalid without admission_date' do
    employee = build(:public_employee, admission_date: nil)
    employee.valid?
    expect(employee.errors[:admission_date]).to include('não pode ficar em branco')
  end

  it 'is invalid without margin_value' do
    employee = build(:public_employee, margin_value: nil)
    employee.valid?
    expect(employee.errors[:margin_value]).to include('não pode ficar em branco')
  end

  it 'is invalid without commissioned' do
    employee = build(:public_employee, commissioned_id: nil)
    employee.valid?
    expect(employee.errors[:commissioned_id]).to include('não pode ficar em branco')
  end

  it 'is invalid without suspension_age' do
    employee = build(:public_employee, suspension_age_id: nil)
    employee.valid?
    expect(employee.errors[:suspension_age_id]).to include('não pode ficar em branco')
  end

  it 'is invalid without end_contract_date' do
    employee = build(:public_employee, end_contract_date: nil)
    employee.valid?
    expect(employee.errors[:end_contract_date]).to include('não pode ficar em branco')
  end

  it 'is invalid with duplicate registration in the same organ on create' do
     saved = create(:public_employee)
     employee = build(:public_employee, organ_id: saved.organ.id)
     employee.valid?
    expect(employee.errors[:registration]).to include('já existe para órgão especificado.qwerty123')
  end

  it 'is invalid with duplicate registration in the same organ on update' do
    saved = create(:public_employee)
    employee = build(:public_employee, cpf: '22222222222', organ_id: saved.organ.id)
    employee.valid?
    expect(employee.errors[:registration]).to include('já existe para órgão especificado.qwerty123')
  end

  it 'is invalid with email in the wrong format' do
    employee = build(:public_employee, email: 'test@test')
    employee.valid?
    expect(employee.errors[:email]).to include('não é válido')
  end

  it 'is invalid with cpf less than 11 characters' do
    employee = build(:public_employee, cpf: '111111111')
    employee.valid?
    expect(employee.errors[:cpf]).to include('não possui o tamanho esperado (11 caracteres)')
  end

  it 'is invalid with cpf grater than 11 characters' do
    employee = build(:public_employee, cpf: '111111111111111111111111111111')
    employee.valid?
    expect(employee.errors[:cpf]).to include('não possui o tamanho esperado (11 caracteres)')
  end

  it 'is invalid with phone less than 10 characters' do
    employee = build(:public_employee, phone: '852222222')
    employee.valid?
    expect(employee.errors[:phone]).to include('é muito curto (mínimo: 10 caracteres)')
  end

  it 'is invalid with phone greater than 11 characters' do
    employee = build(:public_employee, phone: '852222222222')
    employee.valid?
    expect(employee.errors[:phone]).to include('é muito longo (máximo: 11 caracteres)')
  end

end
