require 'rails_helper'

RSpec.describe Consignee, :type => :model do

  it 'has a valid factory' do
    expect(build(:consignee)).to be_valid
  end

  it 'is invalid without name' do
    consignee = build(:consignee, name: nil)
    consignee.valid?
    expect(consignee.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid without cnpj' do
    consignee = build(:consignee, cnpj: nil)
    consignee.valid?
    expect(consignee.errors[:cnpj]).to include('não pode ficar em branco')
  end

  it 'is invalid with duplicate cnpj' do
    create(:consignee, cnpj: '1111111111111111')
    consignee = build(:consignee, cnpj: '1111111111111111')
    consignee.valid?
    expect(consignee.errors[:cnpj]).to include('já está em uso')
  end

  it 'is invalid without street' do
    consignee = build(:consignee, street: nil)
    consignee.valid?
    expect(consignee.errors[:street]).to include('não pode ficar em branco')
  end

  it 'is invalid without number' do
    consignee = build(:consignee, number: nil)
    consignee.valid?
    expect(consignee.errors[:number]).to include('não pode ficar em branco')
  end

  it 'is invalid without neighborhood' do
    consignee = build(:consignee, neighborhood: nil)
    consignee.valid?
    expect(consignee.errors[:neighborhood]).to include('não pode ficar em branco')
  end

  it 'is invalid without zip_code' do
    consignee = build(:consignee, zip_code: nil)
    consignee.valid?
    expect(consignee.errors[:zip_code]).to include('não pode ficar em branco')
  end

  it 'is invalid without city' do
    consignee = build(:consignee, city: nil)
    consignee.valid?
    expect(consignee.errors[:city]).to include('não pode ficar em branco')
  end

  it 'is invalid without state' do
    consignee = build(:consignee, state: nil)
    consignee.valid?
    expect(consignee.errors[:state]).to include('não pode ficar em branco')
  end

  it 'is invalid without country' do
    consignee = build(:consignee, country: nil)
    consignee.valid?
    expect(consignee.errors[:country]).to include('não pode ficar em branco')
  end

  it 'is invalid without contact' do
    consignee = build(:consignee, contact: nil)
    consignee.valid?
    expect(consignee.errors[:contact]).to include('não pode ficar em branco')
  end

  it 'is invalid without post' do
    consignee = build(:consignee, post: nil)
    consignee.valid?
    expect(consignee.errors[:post]).to include('não pode ficar em branco')
  end

  it 'is invalid without phone_number' do
    consignee = build(:consignee, phone_number: nil)
    consignee.valid?
    expect(consignee.errors[:phone_number]).to include('não pode ficar em branco')
  end
end
