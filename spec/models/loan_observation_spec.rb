require 'rails_helper'

RSpec.describe LoanObservation, :type => :model do

  before :each do
    create(:product_group_property)
    create(:card_property)
  end

  it 'has a valid factory' do
    expect(build(:loan_observation)).to be_valid
  end

  it 'is invalid without text' do
    observation = build(:loan_observation, text: nil)
    observation.valid?
    expect(observation.errors[:text]).to include('não pode ficar em branco')
  end

  it 'is invalid without loan' do
    observation = build(:loan_observation, loan_id: nil)
    observation.valid?
    expect(observation.errors[:loan_id]).to include('não pode ficar em branco')
  end

  it 'is invalid without operator' do
    observation = build(:loan_observation, operator_id: nil)
    observation.valid?
    expect(observation.errors[:operator_id]).to include('não pode ficar em branco')
  end

end
