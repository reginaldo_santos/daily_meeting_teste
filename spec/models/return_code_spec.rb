require 'rails_helper'

RSpec.describe ReturnCode, :type => :model do

  it 'has a valid factory' do
    expect(build(:return_code)).to be_valid
  end

  it 'is invalid without a code' do
    return_code = build(:return_code, code: nil)
    return_code.valid?
    expect(return_code.errors[:code]).to include('não pode ficar em branco')
  end

  it 'is invalid without a description' do
    return_code = build(:return_code, description: nil)
    return_code.valid?
    expect(return_code.errors[:description]).to include('não pode ficar em branco')
  end

  it 'is valid without a obs' do
    return_code = build(:return_code, obs: nil)
    return_code.valid?
    expect(return_code.errors[:obs]).not_to include('não pode ficar em branco')
  end

  it 'is invalid with a duplicate code' do
    create(:return_code)
    return_code = build(:return_code)
    return_code.valid?
    expect(return_code.errors[:code]).to include('já está em uso')
  end

end
