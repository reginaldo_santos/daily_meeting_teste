require 'rails_helper'

RSpec.describe Situation, :type => :model do

  it 'has a valid factory' do
    expect(build(:situation)).to be_valid
  end

  it 'is invalid without name' do
    situation = build(:situation, name: nil)
    situation.valid?
    expect(situation.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid without file_reference ' do
    situation = build(:situation, file_reference: nil)
    situation.valid?
    expect(situation.errors[:file_reference]).to include('não pode ficar em branco')
  end
end
