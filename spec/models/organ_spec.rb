require 'rails_helper'

RSpec.describe Organ, :type => :model do

  it 'is valid with code, name and cnpj' do
    expect(build(:organ)).to be_valid
  end

  it 'is invalid without code' do
    organ = build(:organ, code: nil)
    organ.valid?
    expect(organ.errors[:code]).to include('não pode ficar em branco')
  end

  it 'is invalid with duplicate code' do
    saved = create(:organ)
    organ = build(:organ, code: saved.code)
    organ.valid?
    expect(organ.errors[:code]).to include('já está em uso')
  end

  it 'is invalid without name' do
    organ = build(:organ, name: nil)
    organ.valid?
    expect(organ.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid with duplicate name' do
    saved = create(:organ)
    organ = build(:organ, name: saved.name)
    organ.valid?
    expect(organ.errors[:name]).to include('já está em uso')
  end

  it 'is invalid without cnpj' do
    organ = build(:organ, cnpj: nil)
    organ.valid?
    expect(organ.errors[:cnpj]).to include('não pode ficar em branco')
  end

  it 'is invalid with duplicate cpnj' do
    saved = create(:organ)
    organ = build(:organ, cnpj: saved.cnpj)
    organ.valid?
    expect(organ.errors[:cnpj]).to include('já está em uso')
  end

end
