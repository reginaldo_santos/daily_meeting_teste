require 'rails_helper'

RSpec.describe Loan, :type => :model do

  before :each do
    create(:product_group_property)
    create(:card_property)
  end

  it 'has a valid factory' do
    expect(build(:with_approval_password)).to be_valid
  end

  it 'is invalid without contract_value' do
    loan = build(:loan, contract_value: nil)
    loan.valid?
    expect(loan.errors[:contract_value]).to include('não pode ficar em branco')
  end

  it 'is invalid without liquid_value' do
    loan = build(:loan, liquid_value: nil)
    loan.valid?
    expect(loan.errors[:liquid_value]).to include('não pode ficar em branco')
  end

  it 'is invalid without deadline' do
    loan = build(:loan, deadline: nil)
    loan.valid?
    expect(loan.errors[:deadline]).to include('não pode ficar em branco')
  end

  it 'is invalid without installment_value' do
    loan = build(:loan, installment_value: nil)
    loan.valid?
    expect(loan.errors[:installment_value]).to include('não pode ficar em branco')
  end

  it 'is invalid with installment_value greater than margin value' do
    loan = build(:huge_installment_value)
    loan.valid?
    expect(loan.errors[:installment_value]).to include('maior do que margem disponivel')
  end

  it 'is invalid without origin_operation' do
    loan = build(:loan, origin_operation: nil)
    loan.valid?
    expect(loan.errors[:origin_operation]).to include('não pode ficar em branco')
  end

  it 'is invalid with the wrong approval password' do
    loan = build(:with_approval_password, approval_password: '123')
    loan.valid?
    expect(loan.errors[:approval_password]).to include('incorreta')
  end

  it 'has status equal to Loan::AGUARDANDO_SERVIDOR after create without approval password' do
    loan = create(:loan)
    expect(loan.status).to eq Loan::AGUARDANDO_SERVIDOR
  end

  it 'has status equal to Loan::AUTORIZADO_SERVIDOR after create with approval password' do
    loan = create(:with_approval_password)
    expect(loan.status).to eq Loan::AUTORIZADO_SERVIDOR
  end

  it 'has a portability request when a loan has contract type equals to Loan::PORT' do
    loan = create(:contract_type_port)
    expect(loan.portability_requests.count).to eq 1
  end

  it 'has children with status equals to Loan::REFINANCIADO when an loan with contract_type equals to Loan::REFIN is created' do
    child = create(:loan)
    father = build(:with_approval_password, contract_type: Loan::REFIN)
    father.children << child
    father.save!
    expect(child.status).to eq Loan::REFINANCIADO
  end

  it 'has children with status equals to Loan::AGUARDANDO_PORTABLIDADE when loan with contract_type equals to Loan::PORT is created' do
    child = create(:loan)
    father = build(:with_approval_password, contract_type: Loan::PORT)
    father.children << child
    father.save!
    expect(child.status).to eq Loan::AGUARDANDO_PORTABLIDADE
  end

end
