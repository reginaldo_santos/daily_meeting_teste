require 'rails_helper'

RSpec.describe Operator, :type => :model do

  it 'has a valid factory with consignee' do
    expect(build(:operator, :with_consignee)).to be_valid
  end

  it 'has a valid factory with attendace_local' do
    expect(build(:operator, :with_local)).to be_valid
  end

  it 'is invalid without name'do
    operator = build(:operator, name: nil)
    operator.valid?
    expect(operator.errors[:name]).to include('não pode ficar em branco')
  end

  it 'is invalid without cpf' do
    operator = build(:operator, cpf: nil)
    operator.valid?
    expect(operator.errors[:cpf]).to include('não pode ficar em branco')
  end

  it 'is invalid with a duplicate cpf' do
    saved = create(:operator, :with_consignee)
    operator = build(:operator, :with_consignee, cpf: saved.cpf)
    operator.valid?
    expect(operator.errors[:cpf]).to include('O CPF informado já está sendo utilizado.')
  end

  it 'is invalid without company_phone_number' do
    operator = build(:operator, company_phone_number: nil)
    operator.valid?
    expect(operator.errors[:company_phone_number]).to include('não pode ficar em branco')
  end

  it 'is invalid without consignee and attendace_local'do
    operator = build(:operator, :without_consignee_or_local)
    operator.valid?
    expect(operator.errors[:consignee_id]).to include('Consignataria ou Local de atendimento deve ser preenchido')
    expect(operator.errors[:attendance_local_id]).to include('Consignataria ou Local de atendimento deve ser preenchido')
  end

  it 'is invalid without cpf with more or equal than 11 characters' do
    operator = build(:operator, cpf: '111111')
    operator.valid?
    expect(operator.errors[:cpf]).to include('é muito curto (mínimo: 11 caracteres)')
  end

  it 'is invalid without cpf with less or equal than 11 characters' do
    operator = build(:operator, cpf: '11111111111111111111111111111111')
    operator.valid?
    expect(operator.errors[:cpf]).to include('é muito longo (máximo: 11 caracteres)')
  end
end
