# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :attendance_local do
    name 'attendance'
    association :consignee
  end
end
