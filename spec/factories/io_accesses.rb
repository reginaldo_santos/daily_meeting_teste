# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :io_access do
    successful false
    finished false
    category "MyString"
  end
end
