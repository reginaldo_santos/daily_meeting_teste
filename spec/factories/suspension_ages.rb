# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :suspension_age do
    name 'Pensão por tempo indeterminado'
    file_reference 0
  end
end
