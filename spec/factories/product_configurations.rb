# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_configuration do
    product_id 1
    configuration_id 1
    value false
  end
end
