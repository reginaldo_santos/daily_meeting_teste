# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :configuration do
    name "MyString"
    observation "MyText"
  end
end
