# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :property do

    factory :product_group_property do
      name 'GRUPO_PRODUTO'
      value '0.75'
      observation 'product group'
    end

    factory :card_property do
      name 'CARTAO'
      value '0.25'
      observation 'margem cartao'
    end
  end
end
