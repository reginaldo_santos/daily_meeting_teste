require 'cpf_faker'

FactoryGirl.define do
  factory :operator do
    name 'test'
    cpf { Faker::CPF.numeric }
    company_phone_number '8500000000'
    association :consignee

    trait :with_consignee do
      association :consignee
      attendance_local_id nil
    end

    trait :with_local do
      association :attendance_local
      consignee_id nil
    end

    trait :without_consignee_or_local do
      consignee_id nil
      attendance_local_id nil
    end

  end
end
