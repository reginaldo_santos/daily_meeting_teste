# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :consignee do
    name 'consignee'
    sequence(:cnpj) { |n| "00#{n}000#{n}000#{n}000#{n}0"}
    street 'street'
    number 'number'
    neighborhood 'neighborhood'
    zip_code '21212212'
    city 'city'
    state 'state'
    country 'country'
    contact 'contact'
    post 'post'
    phone_number '8511111111'
  end
end
