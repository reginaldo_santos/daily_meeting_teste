# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :file_exception_notification do
    file_type "MyString"
    fail false
  end
end
