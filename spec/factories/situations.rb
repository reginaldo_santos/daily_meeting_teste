# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :situation do
    name 'Aposentado'
    file_reference 'AP'
  end
end
