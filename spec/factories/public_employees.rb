require 'cpf_faker'

FactoryGirl.define do
  factory :public_employee do
    sequence(:name) { |n| "test_#{n}" }
    cpf { Faker::CPF.numeric }
    association :organ
    registration 'qwerty123'
    admission_date Date.today
    association :situation
    margin_value 30000.0
    association :commissioned
    association :suspension_age
    end_contract_date Date.today
    approval_password '12345' #senha: 12345
  end
end
