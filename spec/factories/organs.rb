# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :organ do
    sequence(:code) { |n| "#{n}" }
    sequence(:name) { |q| "test_#{q}" }
    sequence(:cnpj) { |k| "111#{k}1111#{k}11111#{k}1" }
  end
end
