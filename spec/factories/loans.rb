# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :loan do
    contract_value 3000.0
    liquid_value 3000.0
    deadline 10
    installment_value 300.0
    origin_operation 'NEW'
    association :public_employee
    association :consignee
    association :operator

    factory :with_approval_password do
      approval_password '12345'
    end

    factory :contract_type_port do
      contract_type Loan::PORT
    end

    factory :huge_installment_value do
      installment_value 3000000.0
    end
  end
end
