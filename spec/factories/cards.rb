# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :card do
    proposal_number { Faker::Lorem.characters(10) }
    discount_value 1.0
    association :public_employee
    association :consignee
    association :operator
  end
end
