# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :loan_observation do
    text 'text'
    association :loan
    association :operator
  end
end
