class SearchContractsController < ApplicationController

  authorize_resource :class => :search_contract

  def search_contract
    @search_contract = SearchContract.new
    @contracts = []
  end

  def search
  end

end
