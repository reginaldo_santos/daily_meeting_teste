class AttendanceLocalsController < ApplicationController

  authorize_resource :class => :attendance_local

  def index
    user = current_user
    if user.isS?(User::ADMIN)
      @attendance_locals_grid = initialize_grid(AttendanceLocal,
                                                :include => [:consignee])
    else
      @attendance_locals_grid = initialize_grid(AttendanceLocal,
                                                :include => [:consignee],
                                                :conditions => {:consignee_id => user.get_consignee.id})
    end
  end

  def show
    load_attendance_local
  end

  def new
    build_attendance_local
  end

  def edit
    load_attendance_local
    build_attendance_local
  end

  def create
    build_attendance_local
    save_attendance_local or render :new
  end

  def update
    load_attendance_local
    build_attendance_local
    save_attendance_local or render :edit
  end

  def destroy
    load_attendance_local
    if @attendance_local.operators.empty?
      flash[:success] = 'Local de atendimento removido com sucesso.'
      @attendance_local.destroy
    else
      flash[:warning] = 'Local de atendimento não pode ser removido, pois está sendo utilizado.'
    end
      redirect_to attendance_locals_path
  end

  private

    def load_attendance_locals
      @attendance_locals ||= attendance_local_scope.to_a
    end

    def load_attendance_local
      @attendance_local ||= attendance_local_scope.find(params[:id])
    end

    def build_attendance_local
      @attendance_local ||= attendance_local_scope.build
      @attendance_local.attributes = attendance_local_params
    end

    def save_attendance_local
      if @attendance_local.save
        flash[:success] = 'Local de atendimento cridado com sucesso.' if action_name == 'create'
        flash[:success] = 'Local de atendimento atualizado com sucesso.' if action_name == 'update'
        redirect_to attendance_locals_path
      end
    end

    def attendance_local_scope
      AttendanceLocal.where(nil)
    end

    def attendance_local_params
      attendance_local_params = params[:attendance_local]
      attendance_local_params ? attendance_local_params.permit(:name, :cnpj, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :consignee_id, :user_id, :is_admin) : {}
    end
end
