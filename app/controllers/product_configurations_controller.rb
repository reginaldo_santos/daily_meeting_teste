class ProductConfigurationsController < ApplicationController

  load_and_authorize_resource

  before_action :set_product_configuration, only: [:show, :edit, :update, :destroy]

  # GET /product_configurations
  # GET /product_configurations.json
  def index
    @grid = initialize_grid(ProductConfiguration.joins(:product).where('products.consignee_id = ?', current_user.get_consignee.id),
    :include => [:configuration])
  end

  def active
    configuration = ProductConfiguration.find(params[:id])
    configuration.value = true
    configuration.save
    flash[:success] = 'Configuração Ativada com sucesso'
    redirect_to product_configurations_path
  end

  def disable
    configuration = ProductConfiguration.find(params[:id])
    configuration.value = false
    configuration.save
    flash[:success] = 'Configuração Desativada com sucesso'
    redirect_to product_configurations_path
  end

  # DELETE /product_configurations/1
  # DELETE /product_configurations/1.json
  def destroy
    @product_configuration.destroy
    respond_to do |format|
      format.html { redirect_to product_configurations_url, notice: 'Product configuration was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_configuration
      @product_configuration = ProductConfiguration.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_configuration_params
      params.require(:product_configuration).permit(:product_id, :configuration_id, :value)
    end
end
