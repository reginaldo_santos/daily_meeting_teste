class AssistanceController < ApplicationController

  authorize_resource :class => :assistance

  def new

    if !params[:format].blank?
      session[:employee_id] = params[:format].to_i
    end

    @employee = PublicEmployee.where(:id => session[:employee_id]).first

    @assistance_grid = make_grid
    @assistance = AssistanceContract.new
    @action = 'create'

  end

  def edit
    @assistance = AssistanceContract.find(params[:id])

    @employee = @assistance.public_employee
    @assistance_grid = make_grid
    @action = 'update'
  end

  def show
    @assistance = AssistanceContract.find(params[:id])
  end

  def update
    @assistance = AssistanceContract.find(params[:id])
    @assistance.approval_password = params[:assistance_contract][:approval_password]

    if @assistance.valid?
      @assistance.update_attribute(:status, AssistanceContract::AUTORIZADO_SERVIDOR)
      flash[:success] = 'Contrato Assistencial atualizado com sucesso.'
      redirect_to manage_assistance_contract_path
    else
      @employee = @assistance.public_employee
      @assistance_grid = make_grid
      @action = 'update'
      render :edit
    end

  end

  def manage_assistance_contract

    @assistances = AssistanceContract.includes(:public_employee).where(:consignee_id => current_user.operator.consignee_id)

    if current_user.get_consignee
      if current_user.isS?(User::CONSIGNEE_ASSISTANCE_MASTER) || current_user.isS?(User::FORMALIZATION_ASSISTANCE)
        @manage_assistance_grid = initialize_grid(AssistanceContract,
                                                  :include => [:public_employee],
                                                  :conditions => { :consignee_id => current_user.get_consignee },
                                                  order: 'assistance_contracts.created_at',
                                                  order_direction: 'desc')
      else
        @manage_assistance_grid = initialize_grid(AssistanceContract,
                                                  :include => [:public_employee],
                                                  :conditions => { :consignee_id => current_user.get_consignee,
                                                                   :operator_id => current_user.operator.id },
                                                  order: 'assistance_contracts.created_at',
                                                  order_direction: 'desc')
      end
    else
      @manage_assistance_grid = initialize_grid(AssistanceContract,
                                                :include => [:public_employee],
                                                order: 'assistance_contracts.created_at',
                                                order_direction: 'desc')
    end


    @variation = Variation.new

  end

  def approve
    load_assistance(AssistanceContract::AsApprove)
    build_approve_assistance(AssistanceContract::AsApprove)
  end

  def approve_execute
    load_assistance(AssistanceContract::AsApprove)
    build_approve_assistance(AssistanceContract::AsApprove)

    ActiveRecord::Base.transaction do
      save_assistance or render :approve
    end
  end

  def refuse
    load_assistance(AssistanceContract::AsRefuse)
    build_refuse_assistance(AssistanceContract::AsRefuse)
  end

  def refuse_execute
    load_assistance(AssistanceContract::AsRefuse)
    build_refuse_assistance(AssistanceContract::AsRefuse)

    ActiveRecord::Base.transaction do
      save_assistance or render :refuse
    end
  end

  def suspended
    load_assistance(AssistanceContract::AsSuspend)
    build_suspend_assistance(AssistanceContract::AsSuspend)
  end

  def suspended_execute

    load_assistance(AssistanceContract::AsSuspend)
    build_suspend_assistance(AssistanceContract::AsSuspend)

    ActiveRecord::Base.transaction do
      save_assistance or render :suspended
    end

  end

  def cancel
    load_assistance(AssistanceContract::AsCancel)
    build_cancel_assistance(AssistanceContract::AsCancel)
  end

  def cancel_execute
    load_assistance(AssistanceContract::AsCancel)
    build_cancel_assistance(AssistanceContract::AsCancel)

    ActiveRecord::Base.transaction do
      save_assistance or render :cancel
    end
  end

  def create

    @assistance = AssistanceContract.new(assistance_contract_params)
    @assistance.operator = current_user.operator
    @assistance.public_employee_id = session[:employee_id].to_i
    @assistance.consignee_id = current_user.get_consignee.id

    property = Property.where(:category => Property::ASSISTANCE_LIMIT).take
    if @assistance.public_employee.assistance_contracts.where(:status => [AssistanceContract::AUTORIZADO_SERVIDOR, AssistanceContract::APROVADO, AssistanceContract::EM_FOLHA]).count > property.value.to_i
      flash[:error] = 'A operação não pode ser realizada, pois o limite de contratos assistenciais para o servidor especificado foi atingido.'
      redirect_to search_employee_path
    else
      respond_to do |format|
        if @assistance.save
          flash[:success] = I18n.t('feedback_messages.assistance_success')
          format.html { redirect_to search_employee_path }
        else
          @employee = @assistance.public_employee
          @assistance_grid = make_grid
          @action = 'create'
          format.html { render :new }
        end
      end
    end

  end

  private

    def set_assistance_contract
      @assistance = AssistanceContract.find(params[:id])
    end

    def assistance_contract_params
      params.require(:assistance_contract).permit(:discount_value, :approval_password)
    end

    def make_grid

      grid = initialize_grid(AssistanceContract,
                            :include => [:consignee],
                            :custom_order => {
                                'assistance_contracts.consignee_id' => 'consignees.name'
                            },
                            :conditions => ["public_employee_id = ? and status in (?)", @employee.id,
                                            [AssistanceContract::EM_FOLHA, AssistanceContract::APROVADO, AssistanceContract::AUTORIZADO_SERVIDOR]])

    end

    def load_assistance(scope)
      # TODO como nao consegui encontrar aonde estava sendo setado @assistance como Assistance tive que forcar @assistance para nil, obtendo o comportamento esperado.
      if @assistance.class == AssistanceContract
        @assistance = nil
      end
      @assistance ||= assistance_scope(scope).find(params[:id])
    end

    def assistance_scope(scope)
      scope.where(nil)
    end

    def build_approve_assistance(scope)
      @assistance ||= assistance_scope(scope).build
      @assistance.attributes = approve_assistance_params
    end

    def build_refuse_assistance(scope)
      @assistance ||= assistance_scope(scope).build
      @assistance.attributes = refuse_assistance_params
    end

    def build_suspend_assistance(scope)
      @assistance ||= assistance_scope(scope).build
      @assistance.attributes = suspend_assistance_params
    end

    def build_cancel_assistance(scope)
      @assistance ||= assistance_scope(scope).build
      @assistance.attributes = cancel_params
    end

    def approve_assistance_params
      approve_assistance_params = params[:assistance_contract]
      approve_assistance_params ? approve_assistance_params.permit(:contract_number, :observation, :operator_id) : {}
    end

    def refuse_assistance_params
      refuse_assistance_params = params[:assistance_contract]
      refuse_assistance_params ? refuse_assistance_params.permit(:observation, :operator_id) : {}
    end

    def suspend_assistance_params
      suspend_assistance_params = params[:assistance_contract]
      suspend_assistance_params ? suspend_assistance_params.permit(:observation, :operator_id) : {}
    end

    def cancel_params
      cancel_params = params[:assistance_contract]
      cancel_params ? cancel_params.permit(:observation, :operator_id, :password) : {}
    end

    def save_assistance
      if @assistance.save

        case params[:assistance_contract][:action]
          when 'approve_execute'
            flash[:success] = 'Contrato Aprovado com sucesso'
          when 'refuse_execute'
            flash[:success] = 'Contrato Recusado com sucesso'
          when 'suspended_execute'
            flash[:success] = 'Contrato Suspenso com sucesso.'
          when 'cancel_execute'
            flash[:success] = 'Contrato Cancelado com sucesso.'
        end

        redirect_to manage_assistance_contract_path
      end
    end
end
