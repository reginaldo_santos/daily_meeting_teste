class FileController < ApplicationController

  authorize_resource :class => :file

  def margin_file
    @criticisms = MarginCriticism.all
    @criticisms_grid = initialize_grid(MarginCriticism)

    access = IoAccess.where(:category => IoAccess::IMPORT_MARGIN).order(created_at: :desc).first

    if access && access.finished == false
      flash[:warning] = 'O arquivo esta sendo processado, por favor aguarde, a tela estará disponivel após o processamento'
      redirect_to dashboard_path
      return
    elsif access && access.finished == true && access.successful == false
      flash[:warning] = 'Arquivo fora do formato ou possui dados invalidos, por favor verifique os erros abaixo.'
    end
  end

  def return_file
    @critics = ReturnFileCritic.all
    @return_criticism_grid = initialize_grid(ReturnFileCritic)

    access = IoAccess.where(:category => IoAccess::IMPORT_RETURN).order(created_at: :desc).first

    if access && access.finished == false
      flash[:warning] = 'O arquivo esta sendo processado, por favor aguarde, a tela estará disponivel após o processamento'
      redirect_to dashboard_path
      return
    elsif access && access.finished == true && access.successful == false
      flash[:warning] = 'Arquivo fora do formato ou possui dados invalidos, por favor verifique os erros abaixo.'
    end
  end

  def upload_return_file

    uploaded_file = params['return_file']

    if uploaded_file
      if 'text/plain' == uploaded_file.content_type

        access = IoAccess.create(:category => IoAccess::IMPORT_RETURN)

        tempfile = params['return_file'].tempfile
        file_name = params['return_file'].original_filename
        encoding = uploaded_file.tempfile.external_encoding.name
        file = File.join("public", file_name)
        FileUtils.cp tempfile.path, file

        access.delay.import_return(encoding, file_name)
        
        flash[:success] = 'O arquivo esta sendo processado, por favor aguarde, a tela estará disponivel após o processamento'

        redirect_to dashboard_path
      else
        flash[:error] = 'Arquivo de formato invalido'
        redirect_to :action => 'return_file'
      end
    else
      flash[:error] = 'Selecione um arquivo'
      redirect_to :action => 'return_file'
    end

  end

  def dead_employee(insertion_code, averbation_code)
    insertion_line = InsertionLine.find_by(:line_code => insertion_code)
    case insertion_line.insertion_type
      when InsertionLine::LOAN
        insertion_line.loan.update_attribute(:status, Loan::SERVIDOR_FALECIDO)
      when InsertionLine::ASSISTANCE
        insertion_line.assistance_contract.update_attribute(:status, AssistanceContract::SERVIDOR_FALECIDO)
      when InsertionLine::VARIATION
        insertion_line.variation.update_attribute(:status, Variation::SERVIDOR_FALECIDO)
      when InsertionLine::FINANCIAL_RELEASE
        insertion.financial_release.update_attribute(:status, FinancialRelease::SERVIDOR_FALECIDO)
    end
    insertion_line.line_code = nil
    insertion_line.averbation_code = averbation_code
    insertion_line.save!
  end

  def averbar_contrato(insertion_code, averbation_code)
    insertion_line = InsertionLine.includes(:loan, :assistance_contract, :variation, :financial_release).where(:line_code => insertion_code).first
    case insertion_line.insertion_type
      when InsertionLine::LOAN
        loan = insertion_line.loan
        loan.installments_paid += 1
        if loan.installments_paid == loan.deadline
          loan.status = Loan::PAGO_SERVIDOR
        end
        loan.save!
      when InsertionLine::ASSISTANCE
        assistance = insertion_line.assistance_contract
        assistance.paid_installments += 1
        assistance.save!
      when InsertionLine::VARIATION
        variation = insertion_line.variation
        variation.paid_installments += 1
        variation.save!
      when InsertionLine::FINANCIAL_RELEASE
        release = insertion.financial_release
        release.status = FinancialRelease::PAGO
        release.save!
    end
    insertion_line.line_code = nil
    insertion_line.averbation_code = averbation_code
    insertion_line.save!
  end

  def return_line_valid?(averbation_code, insertion_code, critics_messages, line_counter)

    if averbation_code.blank?
      critics_messages << "Linha #{line_counter}: Código de averbação não informado"
    else
      return_code = ReturnCode.find_by(:code => averbation_code)
      if !return_code
        critics_messages << "Linha #{line_counter}: Código de averbação inexistente"
      end
    end

    if insertion_code.blank?
      critics_messages << "Linha #{line_counter}: Código de inserção não informado"
    else
      code = InsertionLine.find_by(:line_code => insertion_code)
      if !code
        critics_messages << "Linha #{line_counter}: Código de inserção inexistente"
      end
    end

    critics_messages.empty? ? true : false
  end

  def upload_margin_file

    uploaded_file = params['margin_file']
    fail = false
    fail_message = ''

    if params['margin_file']
      if 'text/plain' == uploaded_file.content_type
        access = IoAccess.create(:category => IoAccess::IMPORT_MARGIN)

        tempfile = params['margin_file'].tempfile
        original_name = params['margin_file'].original_filename
        encoding = uploaded_file.tempfile.external_encoding.name
        file = File.join("public", original_name)
        FileUtils.cp tempfile.path, file

        access.delay.import_margin(encoding, original_name)

        flash[:success] = 'O arquivo esta sendo processado, por favor aguarde, a tela estará disponivel após o processamento'

        redirect_to dashboard_path
      else
        flash[:error] = 'Arquivo com extensão invalida'
        redirect_to :action => 'margin_file'
      end
    else
      flash[:error] = 'Selecione um arquivo'
      redirect_to :action => 'margin_file'
    end

  end


  private

    def create_margin_history(employee, margin_value)
      begin
        history = MarginHistory.new
        history.public_employee = employee
        history.margin_value = margin_value
        history.save!
      rescue Exception => ex
        raise Exception, ex.message
      end
    end

  def create_or_update_margin_history(employee, margin_value)
    begin
      exist_history = MarginHistory.where(:public_employee_id => employee.id).order(id: :asc).first

      history = nil

      if exist_history && (exist_history.created_at.year == Date.today.year && exist_history.created_at.month == Date.today.month)

        attributes = {:margin_value => margin_value}
        exist_history.update(attributes)

      else
        history = MarginHistory.new
        history.public_employee = employee
        history.margin_value = margin_value
        history.save!
      end
    rescue Exception => ex
      raise Exception, ex.message
    end
  end

    def fill_public_employee(line, organs, situations, marital_status, literacy, suspension_age, commissioned)
      employee = PublicEmployee.new

      employee.name = line[0, 30]
      employee.rg = line[30, 13]
      employee.dispatcher_organ = line[43, 4]
      employee.dispatcher_uf = line[47, 02]
      employee.cpf = line[49, 11]
      employee.organ = organs.where(:code => line[60, 5].to_i.to_s).first
      employee.registration = line[65, 8]
      employee.role = line[73, 15]
      employee.admission_date = Date.parse(line[92, 4]+'-'+line[90, 2]+'-'+line[88, 2]) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[88..95]
      employee.situation = situations.where(:file_reference => line[96, 2]).first
      employee.end_contract_date = Date.new(line[102, 4].to_i, line[100, 2].to_i, line[98, 2].to_i) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[98..105]
      employee.street = line[106, 32]
      employee.number = line[138, 5]
      employee.complement = line[143, 20]
      employee.neighborhood = line[163, 20]
      employee.city = line[183, 15]
      employee.zip_code = line[198, 8]
      employee.phone = line[206, 8].rjust(10, '0')
      employee.birth_date = Date.new(line[218, 4].to_i, line[216, 2].to_i, line[214, 2].to_i) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[214..221]
      employee.marital_status_id = line[222, 1] if marital_status.include?(line[222, 1].to_i)
      employee.voter_registration = line[223, 12]
      employee.zone = line[235, 3]
      employee.section = line[238, 4]
      employee.gender = line[242, 1]
      employee.literacy_id = line[243, 1] if literacy.include?(line[243, 1].to_i)
      employee.margin_value = BigDecimal.new(line[244, 11])/ BigDecimal.new(100) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[244, 11]
      employee.suspension_age = suspension_age.where(:file_reference => line[255, 1]).first
      employee.commissioned_id = line[256, 1].to_i if commissioned.include?(line[256, 1].to_i)

      return employee
    end

    def update_public_employee(employee, modification)
      begin
        attributes = {name: modification.name, rg: modification.rg, dispatcher_organ: modification.dispatcher_organ,
                      dispatcher_uf: modification.dispatcher_uf, cpf: modification.cpf, organ_id: modification.organ_id,
                      registration: modification.registration, role: modification.role, admission_date: modification.admission_date,
                      situation_id: modification.situation_id, end_contract_date: modification.end_contract_date, street: modification.street,
                      number: modification.number, complement: modification.complement, neighborhood: modification.neighborhood,
                      city: modification.city, zip_code: modification.zip_code, phone: modification.phone,
                      birth_date: modification.birth_date, marital_status_id: modification.marital_status_id, voter_registration: modification.voter_registration,
                      zone: modification.zone, section: modification.section, gender: modification.gender,
                      literacy_id: modification.literacy_id, margin_value: modification.margin_value, suspension_age_id: modification.suspension_age_id,
                      commissioned_id: modification.commissioned_id}

        employee.assign_attributes(attributes)
        employee.update(attributes)
      rescue Exception => ex
        raise Exception, ex.message
      end
    end

    def update_not_found_employees(found_employees_id)
      begin

        employees_to_update = PublicEmployee.where('id not in (?)', found_employees_ids)

        employees_to_update.each do |employee|
          attributes = {margin_value: BigDecimal.new('0')}
          employee.update(attributes)
        end
      rescue Exception => ex
        raise Exception, ex.message
      end

    end

end
