#encoding: utf-8

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :require_login, :except => [:login, :login_attempt, :inform_email,
                                            :reset, :send_reset_password_instruction, :inform_new_password,
                                            :new_employee_user, :create_employee_user, :inform_login_and_password,
                                            :update_login_for_employee_user]
  before_action :set_last_interaction
  skip_before_action :set_last_interaction, :only => [:login, :login_attempt, :inform_email,
                                                      :reset, :send_reset_password_instruction, :inform_new_password,
                                                      :new_employee_user, :create_employee_user, :inform_login_and_password, :update_login_for_employee_user]

  before_action :redirect_if_didnt_change_password, if: :current_user_need_password_change?
  skip_before_action :redirect_if_didnt_change_password, :only => [:change_password, :update_password]
  check_authorization

  rescue_from CanCan::AccessDenied do |exception|
    flash[:warning] = 'Você não possui autorização para acessar a tela, qualquer duvida entre em contato com o setor responsavel.'
    redirect_to dashboard_path
  end

  private

  def require_login
    if !user_sign_in?
      flash[:warning] = I18n.t('authentication.require_login')
      redirect_to guest_root_path
    end
  end

  def redirect_if_didnt_change_password
    flash[:warning] = 'Por favor troque sua senha'
    redirect_to change_password_path(current_user)
  end

  def current_user_need_password_change?

    if current_user
      if current_user.last_password_change.nil?
        true
      else
        number_of_days = Property.where(:name => Property::MANDATORY_PASSWORD_CHANGE).first.value.to_i
        if current_user.last_password_change < number_of_days.days.ago
          true
        else
          false
        end
      end
    else
      false
    end
  end

  def set_last_interaction
    time_now = Time.now
    last_interaction = Time.parse(session[:last_interaction])

    if time_now - last_interaction < 20 * 60.seconds
      session[:last_interaction] = time_now
    else
      session[:user_id] = nil
      session[:last_interaction] = nil

      flash[:warning] = I18n.t('authentication.session_expiration')

      redirect_to root_path
    end

  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def user_sign_in?
    !session[:user_id].nil? ? true : false
  end

  def check_permission(method, resource)
    can? method, resource
  end

  helper_method :current_user
  helper_method :user_sign_in?
  helper_method :check_permission


end
