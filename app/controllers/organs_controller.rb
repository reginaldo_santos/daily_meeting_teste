class OrgansController < ApplicationController
  load_and_authorize_resource

  def index
    load_organs
    @organs_grid = initialize_grid(Organ)
  end

  def show
    load_organ
  end

  def new
    build_organ
  end

  def edit
    load_organ
    build_organ
  end

  def create
    build_organ
    save_organ or render :new
  end

  def update
    load_organ
    build_organ
    save_organ or render :edit
  end

  def destroy
    load_organ
    if @organ.public_employees.empty?
      flash.now[:success] = 'Órgão removido com sucesso.'
      @organ.destroy
    else
      flash[:warning] = 'Órgão não pode ser removido, pois está sendo utilizado.'
    end
    redirect_to organs_path
  end

  private

    def load_organs
      @organs ||= organ_scope.to_a
    end

    def load_organ
      @organ ||= organ_scope.find(params[:id])
    end

    def build_organ
      @organ ||= organ_scope.build
      @organ.attributes = organ_params
    end

    def save_organ
      if @organ.save
        flash[:success] = 'Órgão cridado com sucesso.' if action_name == 'create'
        flash[:success] = 'Órgão atualizado com sucesso.' if action_name == 'update'
        redirect_to organs_path
      end
    end

    def organ_scope
      Organ.where(nil)
    end

    def organ_params
      organ_params = params[:organ]
      organ_params ? organ_params.permit(:code, :name, :cnpj, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country) : {}
    end
end
