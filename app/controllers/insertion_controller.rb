class InsertionController < ApplicationController

  authorize_resource :class => :insertion

  def insertion

    access = IoAccess.where(:category => IoAccess::EXPORT_INSERTION).order(created_at: :desc).first

    if access && access.finished == false
      flash.now[:warning] = 'O arquivo esta sendo gerado, por favor aguarde, a tela estará disponivel após o processamento'
      redirect_to dashboard_path
      return
    elsif access && access.finished == true && access.successful == false
      flash.now[:warning] = 'Arquivo não pôde ser gerado, por favor tente novamente'
    end

    @month = nil
    @year = nil
    @insertion = Insertion.new

    insertion = Insertion.order('created_at DESC').first

    if insertion
      month = insertion.month + 1
      year = insertion.year

      if month < 13
        @month = month
        @year = year
      else
        @month = 1
        @year = year + 1
      end
    else
      today = Date.today
      @month = today.month
      @year = today.year
    end

    @insertions = Insertion.all
    @insertions_grid = initialize_grid(Insertion)

  end

  def generate_file
    access = IoAccess.create(:category => IoAccess::EXPORT_INSERTION)
    access.delay.export_insertion(params['month'], params['year'])
    flash[:success] = 'O arquivo esta sendo gerado, por favor aguarde, a tela estará disponivel após o processamento'
    redirect_to dashboard_path  
  end

  def download
    send_file(Insertion.find(params[:insertion_id]).file_path)
  end

  private

    def generate_detailed_insertion_file


      ActiveRecord::Base.transaction do
        cut_day = Property.where(:name => Property::CUT_DAY).first.value.to_i

        month = params['month'].to_i
        year = params['year'].to_i

        mon_str = month.to_s.rjust(2, '0')

        cut_date = DateTime.new(year, month, cut_day).end_of_day

        file_name = "#{month}_#{year}_detalhado.txt"
        path = "/home/reginaldo/Área de Trabalho/file/#{file_name}"

        insertion = Insertion.new
        insertion.month = month
        insertion.year = year
        insertion.file_name = file_name
        insertion.file_path = path
        insertion.save!


        employees = PublicEmployee.includes(:loans, :assistance_contracts, :cards, :organ).all
        priority_properties = Property.where(:category => Property::INSERTION_PRIORITY).order('value ASC')
        partial_discount_value = Property.where(:category => Property::PARTIAL_DISCOUNT).first.value.to_i
        partial_discount_activated = 1

        File.open(path, 'w+') do |f|

          employees.each do |employee|
            margin_in_file = 0
            employee_margin = employee.margin_value
            organ = employee.organ.code.rjust(5, '0')
            registration = employee.registration.rjust(8, '0')
            blank_field = '%-2.2s' % nil
            uso_emp = '%-29.29s' % nil
            final_fill = '0'.rjust(8, '.')
            if employee.cpf
              cpf = employee.cpf
            else
              cpf = '%-11.11s' % nil
            end
            priority_properties.each do |priority_property|

              ActiveRecord::Base.transaction do
                name = priority_property.name

                case name
                  when Property::PRIORITY_ASSISTANCE
                    product_type = ProductType.where(:name => ProductType::ASSISTANCE).take
                    get_assistance_contract(employee, cut_date).each do |assistance|

                      have_margin = validate_margin_employee(employee_margin, margin_in_file, assistance.discount_value)
                      do_partial_discount = (!have_margin && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                      if have_margin || do_partial_discount

                        product = Product.joins(:product_type, :consignee).where('product_types.id = ? and consignees.id = ?', product_type.id, assistance.consignee.id).take
                        event_code = product.event_code.to_s.rjust(6, '0')
                        discount_value = (assistance.discount_value * 100).to_i.to_s.rjust(11, '0') if have_margin
                        discount_value = ((assistance.discount_value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount

                        if assistance.contract_number
                          contract_number = assistance.contract_number.rjust(18, '0')
                        else
                          contract_number = '%-18.18s' % nil
                        end

                        if assistance.paid_installments
                          installment_to_pay = (assistance.paid_installments + 1).to_s.rjust(3, '0')
                        else
                          installment_to_pay = 1.to_s.rjust(3, '0')
                        end
                        deadline = 999
                        segment = '02' #segmento assistencial
                        id_consignee = assistance.consignee.id

                        assistance_insertion_code = StringGenerator.generate_unique_code_to_insertion
                        assistance_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                      :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                      :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                      :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                      :final_fill => final_fill, :insertion_type => InsertionLine::ASSISTANCE,
                                                                      :assistance_contract_id => assistance.id, :line_code => assistance_insertion_code,
                                                                      :partial_discount => do_partial_discount, :insertion_id => insertion.id, :consignee_id => id_consignee)
                        assistance_insertion_line.save!

                        assistance_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{assistance_insertion_code}"
                        f.puts(assistance_line)
                        margin_in_file += assistance.discount_value
                        assistance.variations.each do |variation|

                          have_margin_variation = validate_margin_employee(employee_margin, margin_in_file, variation.value)
                          do_partial_discount_variation = (!have_margin_variation && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                          if have_margin_variation || do_partial_discount_variation

                            #TODO falta buscar codigo do evento
                            event_code = product.event_code.to_s.rjust(6, '0')
                            discount_value = (variation.value * 100).to_i.to_s.rjust(11, '0') if have_margin_variation
                            discount_value = ((variation.value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount_variation

                            contract_number = '%-6.6s' % nil

                            if variation.paid_installments
                              installment_to_pay = (variation.paid_installments + 1).to_s.rjust(3, '0')
                            else
                              installment_to_pay = 1.to_s.rjust(3, '0')
                            end
                            deadline = 999
                            segment = '02' #segmento assistencial( ja que nao existe segmento de variação)
                            consignee_id = assistance.consignee.id
                            variation_insertion_code = StringGenerator.generate_unique_code_to_insertion
                            variation_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                          :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                          :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                          :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                          :final_fill => final_fill, :insertion_type => InsertionLine::VARIATION,
                                                                          :variation_id => variation.id, :line_code => variation_insertion_code,
                                                                          :partial_discount => do_partial_discount_variation, :insertion_id => insertion.id, :consignee_id => consignee_id)
                            variation_insertion_line.save!
                            variation_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{variation_insertion_code}"

                            f.puts(variation_line)
                            margin_in_file += variation.value
                          end
                        end
                      end
                    end
                  when Property::PRIORITY_LOAN
                    product_type = ProductType.where(:name => ProductType::LOAN).take
                    get_loan(employee, cut_date).each do |loan|
                      have_margin = validate_margin_employee(employee_margin, margin_in_file, loan.installment_value)
                      do_partial_discount = (!have_margin && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                      if have_margin || do_partial_discount

                        product = Product.joins(:consignee, :product_type).where('consignees.id = ? and product_types.id = ?', loan.consignee.id, product_type.id).take

                        event_code = product.event_code.to_s.rjust(6, '0')

                        discount_value = (loan.installment_value * 100).to_i.to_s.rjust(11, '0') if have_margin
                        discount_value = ((loan.installment_value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount

                        if loan.contract_number
                          contract_number = loan.contract_number.rjust(18, '0')
                        else
                          contract_number = '%-18.18s' % nil
                        end

                        if loan.installments_paid
                          installment_to_pay = (loan.installments_paid + 1).to_s.rjust(3, '0')
                        else
                          installment_to_pay = 1.to_s.rjust(3, '0')
                        end
                        deadline = loan.deadline.to_s.rjust(3, '0')
                        segment = '01' #segmento financeiro
                        consignee_id = loan.consignee.id
                        loan_insertion_code = StringGenerator.generate_unique_code_to_insertion
                        loan_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                      :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                      :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                      :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                      :final_fill => final_fill, :insertion_type => InsertionLine::LOAN,
                                                                      :loan_id => loan.id, :line_code => loan_insertion_code,
                                                                      :partial_discount => do_partial_discount, :insertion_id => insertion.id, :consignee_id => consignee_id)
                        loan_insertion_line.save!
                        loan_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{loan_insertion_code}"

                        f.puts(loan_line)
                        margin_in_file += loan.installment_value
                      end
                    end
                  when Property::PRIORITY_CARD
                    product_type = ProductType.where(:name => ProductType::CARD).take
                    get_card(employee, cut_date).each do |card|
                      get_financial_releases(card, cut_date).each do |release|

                        have_margin = validate_margin_employee(employee_margin, margin_in_file, release.value)
                        do_partial_discount = (!have_margin && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                        if have_margin || do_partial_discount

                          product = Product.joins(:product_types, :consignees).where('consignees.id = ? and product_types.id = ?', card.consignee.id, product_type.id)
                          event_code = product.event_code.to_s.rjust(3, '0')

                          discount_value = (release.value * 100).to_i.to_s.rjust(11, '0') if have_margin
                          discount_value = ((release.value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount

                          contract_number = '%-6.6s' % nil

                          installment_to_pay = 0.to_s.rjust(3, '0')

                          deadline = 0.to_s.rjust(3, '0')
                          segment = '03' #segmento financeiro
                          consignee_id = card.consignee.id
                          release_insertion_code = StringGenerator.generate_unique_code_to_insertion
                          release_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                        :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                        :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                        :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                        :final_fill => final_fill, :insertion_type => InsertionLine::FINANCIAL_RELEASE,
                                                                        :financial_release_id => release.id, :line_code => release_insertion_code,
                                                                        :partial_discount => do_partial_discount, :insertion_id => insertion.id, :consignee_id => consignee_id)
                          release_insertion_line.save!
                          release_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{release_insertion_code}"

                          f.puts(release_line)
                          margin_in_file += release.value
                        end

                      end
                      #end get_financial_releases
                    end
                    #end get_cards
                end
                #end case card
              end

            end
          end

        end
      end
      # end transaction
      redirect_to dashboard_path
    end

    def generate_consolidated_insertion_file

    end

    def generate_lines_to_assistance_contracts(file)

    end

    def validate_margin_employee(margin, margin_in_file, discount_value)
      if (margin - margin_in_file) >= discount_value
        return true
      else
        return false
      end
    end

    def get_assistance_contract(employee, cut_date)
      assistance_list = employee.assistance_contracts.includes(:consignee, :variations).where('created_at <= ? and status in (?)', cut_date, [AssistanceContract::APROVADO, AssistanceContract::EM_FOLHA]).order('created_at ASC')
      return assistance_list
    end

    def get_loan(employee, cut_date)
      loan_list = employee.loans.where('created_at <= ? and status in (?)', cut_date, [Loan::APROVADO, Loan::EM_FOLHA]).order('created_at ASC')
      return loan_list
    end

    def get_card(employee, cut_date)
      card_list = employee.cards.where('created_at <= ? and status in (?)', cut_date, [Card::AUTORIZADO_CONSIGNATARIA]).order('created_at ASC')
      return card_list
    end

    def get_financial_releases(card, cut_date)
      releases = card.financial_releases.where('created_at <= ? and status in (?)', cut_date, [FinancialRelease::AUTORIZADO_CONSIGNATARIA]).order('created_at ASC')
      return releases
    end

end
