class InsertionLinesController < ApplicationController
  before_action :set_insertion_line, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource

  # GET /insertion_lines
  # GET /insertion_lines.json
  def index
    if current_user.isS?(User::ADMIN)
      @insertion_line_grid = initialize_grid(InsertionLine.all,
                                             :include => [:loan, :financial_release, :variation, :assistance_contract])
    else
      @insertion_line_grid = initialize_grid(InsertionLine.where(:consignee_id => current_user.get_consignee.id),
                                             :include => [:loan, :financial_release, :variation, :assistance_contract])
    end

  end

  # GET /insertion_lines/1
  # GET /insertion_lines/1.json
  def show
  end

  # GET /insertion_lines/new
  def new
    @insertion_line = InsertionLine.new
  end

  # GET /insertion_lines/1/edit
  def edit
  end

  # POST /insertion_lines
  # POST /insertion_lines.json
  def create
    @insertion_line = InsertionLine.new(insertion_line_params)

    respond_to do |format|
      if @insertion_line.save
        format.html { redirect_to @insertion_line, notice: 'Insertion line was successfully created.' }
        format.json { render :show, status: :created, location: @insertion_line }
      else
        format.html { render :new }
        format.json { render json: @insertion_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /insertion_lines/1
  # PATCH/PUT /insertion_lines/1.json
  def update
    respond_to do |format|
      if @insertion_line.update(insertion_line_params)
        format.html { redirect_to @insertion_line, notice: 'Insertion line was successfully updated.' }
        format.json { render :show, status: :ok, location: @insertion_line }
      else
        format.html { render :edit }
        format.json { render json: @insertion_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /insertion_lines/1
  # DELETE /insertion_lines/1.json
  def destroy
    @insertion_line.destroy
    respond_to do |format|
      format.html { redirect_to insertion_lines_url, notice: 'Insertion line was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def download_file
    year = params[:grid][:f][:year] if !params[:grid].blank?
    month = params[:grid][:f][:month] if !params[:grid].blank?
    registration = params[:grid][:f][:registration] if !params[:grid].blank?

    if params[:commit] == 'CSV'
      download_csv(year, month, registration)
    else
      download_txt(year, month, registration)
    end

  end

  private

    def download_txt(year, month, registration)

      file = Tempfile.new(['retorno_consignataria', '.txt'], 'tmp')
      user = current_user

      query_values = Hash.new
      query_values[:year] = year if !year.blank?
      query_values[:month] = month if !month.blank?
      query_values[:registration] = registration if !registration.blank?

      if user.get_consignee
        query_values[:consignee_id] = user.get_consignee.id
        lines = InsertionLine.where(query_values)
      else
        if query_values.empty?
          lines = InsertionLine.where(nil)
        else
          lines = InsertionLine.where(query_values)
        end
      end

      lines.each do |line|
        year = line.year
        month = line.month
        organ = line.organ_code
        registration_code = line.registration.rjust(8, '0')
        event_code = line.event_code.rjust(6, '0')
        discount_value = line.discount_value.rjust(11, '0')
        line.averbation_code.blank? ? averbation_code = '' : averbation_code = line.averbation_code
        averbation_code = averbation_code.rjust(2, ' ')
        employee = line.get_financial_operation.public_employee
        employee.cpf.blank? ? cpf = '' : cpf = employee.cpf
        cpf = cpf.rjust(11, ' ')
        to_pay = line.installment_to_pay.to_s.rjust(3, '0')
        deadline = line.deadline.to_s.rjust(3, '0')
        segment = line.segment.rjust(2, '0')
        contract_number = line.get_contract_number.rjust(18, ' ')



        file.puts "#{year}#{month}#{organ}#{registration_code}#{event_code}#{discount_value}#{averbation_code}#{cpf}#{contract_number}#{to_pay}#{deadline}#{segment}"
      end

      file.close
      send_file file.path

    end

    def download_csv(year, month, registration)
      file = Tempfile.new(['retorno_consignataria', '.csv'], 'tmp')
      user = current_user

      query_values = Hash.new
      query_values[:year] = year if !year.blank?
      query_values[:month] = month if !month.blank?
      query_values[:registration] = registration if !registration.blank?

      if user.get_consignee
        query_values[:consignee_id] = user.get_consignee.id
        lines = InsertionLine.where(query_values)
      else
        if query_values.empty?
          lines = InsertionLine.where(nil)
        else
          lines = InsertionLine.where(query_values)
        end
      end

      file.puts 'Ano;Mês;Órgão;Matricula;CodigoEvento;ValorDescontado;CodigodeRetorno;CPF;Contrato;ParcelaDescontada;Prazo;Segmento'

      lines.each do |line|
        year = line.year
        month = line.month
        organ = line.organ_code
        registration_code = line.registration.rjust(8, '0')
        event_code = line.event_code.rjust(6, '0')
        discount_value = line.discount_value.rjust(11, '0')
        line.averbation_code.blank? ? averbation_code = '' : averbation_code = line.averbation_code
        averbation_code = averbation_code.rjust(2, ' ')
        employee = line.get_financial_operation.public_employee
        employee.cpf.blank? ? cpf = '' : cpf = employee.cpf
        cpf = cpf.rjust(11, ' ')
        to_pay = line.installment_to_pay.to_s.rjust(3, '0')
        deadline = line.deadline.to_s.rjust(3, '0')
        segment = line.segment.rjust(2, '0')
        contract_number = line.get_contract_number.rjust(18, ' ')



        file.puts "#{year};#{month};#{organ};#{registration_code};#{event_code};#{discount_value.to_i/100.0};#{averbation_code};#{cpf};#{contract_number};#{to_pay};#{deadline};#{segment}"
      end

      file.close
      send_file file.path
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_insertion_line
      @insertion_line = InsertionLine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def insertion_line_params
      params[:insertion_line]
    end
end
