class PortabilityRequestsController < ApplicationController

  load_and_authorize_resource

  def manage_requests
    consignee_id = current_user.get_consignee.id
    @portability_grid = initialize_grid(PortabilityRequest,
    :include => [:loan],
    :conditions => ['seller_id = ? or buyer_id = ?', consignee_id, consignee_id],
    :order => 'created_at',
    :order_direction => 'desc')
  end

  def answer_request
    load_portability_request(PortabilityRequest::AsAnswerPortability)
    build_portability_request(PortabilityRequest::AsAnswerPortability)
  end

  def answer_request_execute
    load_portability_request(PortabilityRequest::AsAnswerPortability)
    build_portability_request(PortabilityRequest::AsAnswerPortability)

    ActiveRecord::Base.transaction do
      save_portability_request or render :answer_request
    end
  end

  private

    def load_portability_request(scope)
      if @portability_request.class == PortabilityRequest
        @portability_request = nil
      end
      @portability_request ||= portability_request_scope(scope).find(params[:id])
    end

    def build_portability_request(scope)
      @portability_request ||= portability_request_scope(scope).build
      @portability_request.attributes = portability_request_params
    end

    def portability_request_scope(scope)
      scope.where(nil)
    end

    def save_portability_request
      if @portability_request.save
        flash[:success] = 'Solicitação respondida com sucesso.'
        redirect_to manage_portability_path
      end
    end

    def portability_request_params
      portability_request_params = params[:portability_request]
      portability_request_params ? portability_request_params.permit( :portability_code, :observation_seller, :balance_due_release) : {}
    end

end
