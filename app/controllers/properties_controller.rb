class PropertiesController < ApplicationController
  load_and_authorize_resource

  def index
    load_properties
    @properties_grid = initialize_grid(Property)
  end

  def show
    load_property
  end

  def new
    build_property
  end

  def edit
    load_property
    build_property
  end

  def create
    build_property
    save_property or render :new
  end

  def update
    load_property
    build_property
    save_property or render :edit
  end

  def destroy
    load_property
    @property.destroy
    flash[:success] = 'Propriedade removida com sucesso.'
    redirect_to properties_path
  end

  private

    def load_properties
      @properties ||= property_scope.to_a
    end

    def load_property
      @property ||= property_scope.find(params[:id])
    end

    def build_property
      @property ||= property_scope.build
      @property.attributes = property_params
    end

    def save_property
      if @property.save
        flash[:success] = 'Propriedade criada com sucesso.' if action_name == 'create'
        flash[:success] = 'Propriedade atualizada com sucesso.' if action_name == 'update'
        redirect_to properties_path
      end
    end

    def property_scope
      Property.where(nil)
    end

    def property_params
      property_params = params[:property]
      property_params ? property_params.permit(:name, :value, :observation) : {}
    end
end
