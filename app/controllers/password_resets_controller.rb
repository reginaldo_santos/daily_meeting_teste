class PasswordResetsController < ApplicationController

  authorize_resource :class => :password_reset

  layout 'login_layout'

  def inform_email
  end

  def send_reset_password_instruction



    regex = /^.+@.+$/

    if regex.match(params[:email]).nil?
      flash.now[:error] = 'informe um email válido.'
      render :inform_email
      return
    else

      user = nil

      employee = PublicEmployee.joins(:user).where('email = ? and users.login = ?', params[:email], params[:user]).first

      if employee
        user = employee.user
      else
        operator = Operator.joins(:user).where('email = ? and users.login = ?', params[:email], params[:user]).first
        if operator
          user = operator.user
        end
      end

      if user
        user.send_password_reset
        flash[:success] = 'Instruções para resetar sua senha foram enviadas para seu e-mail.'
        redirect_to :action => 'inform_email'
        return
      else
        flash.now[:error] = 'E-mail ou usuário não encontrado'
        render :inform_email
        return
      end
    end

  end

  def inform_new_password
    @user = User.find_by_password_reset_token!(params[:id])
  end

  def reset

    @user = User.where('password_reset_token = ?', params[:id]).first

    if @user
      if @user.password_reset_sent_at < 1.day.ago
        flash[:warning] = 'O link de alteração de senha dura apenas 24h, por favor solicite mudança de senha novamente.'
        redirect_to :action => 'inform_email'
      elsif @user.update(user_params)
        flash[:success] = 'Senha alterada com sucesso.'
        redirect_to root_path
      else
        render :inform_new_password
      end
    else
      flash[:error] = 'Não foi possivel encontrar o usuário associado ao token informado'
      redirect_to root_path
    end


  end

  private

    def user_params
      params.require(:user).permit(:login, :email, :name, :login_attempts, :password, :password_confirmation, :current_password, :origin)
    end
end
