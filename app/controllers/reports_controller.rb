class ReportsController < ApplicationController

  authorize_resource :class => :report

  def analytical
  end

  def analytical_employee
    error_messages = []

    parameters = Hash.new
    employee_parameters = Hash.new

    if params['registration'].blank?
      error_messages << 'Mátricula deve ser informada.'
    else
      employee_parameters[:registration] = params['registration']
    end

    if params['organ'].blank?
      error_messages << 'Órgão deve ser selecionado.'
    else
      employee_parameters[:organ_id] = params['organ']
    end

    if !params['consignee'].blank?
      if current_user.isS?(User::ADMIN)
        parameters[:consignee_id] = params['consignee']
      else
        parameters[:consignee_id] = current_user.get_consignee.id
      end
    end

    parameters[:public_employees] = employee_parameters

    product = params['segment']

    case product
      when ''  #todos
        @loans = Loan.joins(:public_employee).where(parameters)
        @assistance = AssistanceContract.joins(:public_employee).where(parameters)
        @card = Card.joins(:public_employee).where(parameters)
      when '1' #emprestimo
        @loans = Loan.joins(:public_employee).where(parameters)
        @assistance = AssistanceContract.none
        @card = Card.none
      when '2' #assistencial
        @loans = Loan.none
        @assistance = AssistanceContract.joins(:public_employee).where(parameters)
        @card = Card.none
      when '3' #cartao
        @loans = Loan.none
        @assistance = AssistanceContract.none
        @card = Card.joins(:public_employees).where(parameters)
    end

    if @loans.empty? && @assistance.empty? && @card.empty?
      error_messages << 'Nada foi encontrado para os parâmetros informados'
    end

    if error_messages.empty?
      csv_string = CSV.generate do |csv|
        csv << ['Nome', 'CPF', 'Matricula', 'Órgão',
                'Produto', 'Tipo de Produto', 'Valor do desconto',
                'Parcelas Pagas', 'Prazo', 'Data', 'Status']

        @loans.each do |loan|
          csv << [loan.public_employee.name, loan.public_employee.cpf, loan.public_employee.registration, loan.public_employee.organ.name,
                  'Empréstimo', nil, loan.installment_value,
                  loan.installments_paid, loan.deadline, loan.created_at.strftime('%d/%m/%Y'),
                  loan.status]
        end

        @assistance.each do |assistance|
          name = assistance.public_employee.name
          cpf = assistance.public_employee.cpf
          registration = assistance.public_employee.registration
          organ = assistance.public_employee.organ.name
          csv << [name, cpf, registration, organ,
                  'Assistencial', 'Contrato', assistance.discount_value,
                  assistance.paid_installments, '999', assistance.created_at.strftime('%d/%m/%Y'),
                  assistance.status]
          assistance.variations.each do |variation|
            csv << [name, cpf, registration, organ,
                    'Assistencial', 'Variação', variation.value,
                    variation.paid_installments, '999', variation.created_at.strftime('%d/%m/%Y'),
                    variation.status]
          end
        end

        @card.each do |card|
          name = card.public_employee.name
          cpf = card.public_employee.cpf
          registration = card.public_employee.registration
          organ = card.public_employee.organ.name
          csv << [name, cpf, registration, organ, 'Cartão', 'Cartão', card.discount_value, nil, nil, card.created_at, card.status]
          card.financial_releases.each do |release|
            csv << [name, cpf, registration, organ, 'Cartão', 'Fatura', release.value, 1, nil, card.created_at, card.status]
          end
        end
      end
      send_data csv_string,
                :type => 'text/csv; header=present',
                :disposition => "attachment; filename=analitico.csv"
    else
      flash.now[:error] = error_messages.join('<br/>').html_safe
      render 'analytical'
    end

  end

  def analytical_period

    error_messages = []

    if params['init_period'].blank?
      error_messages << 'Data de inicio deve ser selecionada'
    end

    if params['end_period'].blank?
      error_messages << 'Data de fim deve ser selecionada.'
    end

    if !error_messages.empty?
      flash.now[:error] = error_messages.join('<br/>').html_safe
      render 'analytical'
    else
      product = params['product']

      init_period = Date.strptime(params['init_period'], '%d/%m/%Y').beginning_of_day
      end_period = Date.strptime(params['end_period'], '%d/%m/%Y').end_of_day

      if !params['consignee_period'].blank?
        if current_user.isS?(User::ADMIN)
          consignee_id = params['consignee_period'].to_i
        else
          consignee_id = current_user.get_consignee.id
        end
      end


      case product
        when ''  #todos
          @loans = Loan.where(:created_at => init_period .. end_period) if params['consignee_period'].blank?
          @loans = Loan.where(:created_at => init_period .. end_period, :consignee_id => consignee_id) if !params['consignee_period'].blank?
          @assistance = AssistanceContract.where(:created_at => init_period .. end_period) if params['consignee_period'].blank?
          @assistance = AssistanceContract.where(:created_at => init_period .. end_period, :consignee_id => consignee_id) if !params['consignee_period'].blank?
          @card = Card.where(:created_at => init_period .. end_period) if params['consignee_period'].blank?
          @card = Card.where(:created_at => init_period .. end_period, :consignee_id => consignee_id) if !params['consignee_period'].blank?
        when '1' #emprestimo
          @loans = Loan.where(:created_at => init_period .. end_period) if params['consignee_period'].blank?
          @loans = Loan.where(:created_at => init_period .. end_period, :consignee_id => consignee_id) if !params['consignee_period'].blank?
          @assistance = AssistanceContract.none
          @card = Card.none
        when '2' #assistencial
          @loans = Loan.none
          @assistance = AssistanceContract.where(:created_at => init_period .. end_period) if params['consignee_period'].blank?
          @assistance = AssistanceContract.where(:created_at => init_period .. end_period, :consignee_id => consignee_id) if !params['consignee_period'].blank?
          @card = Card.none
        when '3' #cartao
          @loans = Loan.none
          @assistance = AssistanceContract.none
          @card = Card.where(:created_at => init_period .. end_period) if params['consignee_period'].blank?
          @card = Card.where(:created_at => init_period .. end_period, :consignee_id => consignee_id) if !params['consignee_period'].blank?
      end

      if @loans.empty? && @assistance.empty? && @card.empty?
        error_messages << 'Nada foi encontrado para os parâmetros informados'
      end

      if error_messages.empty?
        csv_string = CSV.generate do |csv|
          csv << ['Nome', 'CPF', 'Matricula', 'Órgão',
                  'Produto', 'Tipo de Produto', 'Valor do desconto',
                  'Parcelas Pagas', 'Prazo', 'Data', 'Status']

          @loans.each do |loan|
            csv << [loan.public_employee.name, loan.public_employee.cpf, loan.public_employee.registration, loan.public_employee.organ.name,
                    'Empréstimo', nil, loan.installment_value,
                    loan.installments_paid, loan.deadline, loan.created_at.strftime('%d/%m/%Y'),
                    loan.status]
          end

          @assistance.each do |assistance|
            name = assistance.public_employee.name
            cpf = assistance.public_employee.cpf
            registration = assistance.public_employee.registration
            organ = assistance.public_employee.organ.name
            csv << [name, cpf, registration, organ,
                    'Assistencial', 'Contrato', assistance.discount_value,
                    assistance.paid_installments, '999', assistance.created_at.strftime('%d/%m/%Y'),
                    assistance.status]
            assistance.variations.each do |variation|
              csv << [name, cpf, registration, organ,
                      'Assistencial', 'Variação', variation.value,
                      variation.paid_installments, '999', variation.created_at.strftime('%d/%m/%Y'),
                      variation.status]
            end
          end

          @card.each do |card|
            name = card.public_employee.name
            cpf = card.public_employee.cpf
            registration = card.public_employee.registration
            organ = card.public_employee.organ.name
            csv << [name, cpf, registration, organ, 'Cartão', 'Cartão', card.discount_value, nil, nil, card.created_at, card.status]
            card.financial_releases.each do |release|
              csv << [name, cpf, registration, organ, 'Cartão', 'Fatura', release.value, 1, nil, card.created_at, card.status]
            end
          end
        end
        send_data csv_string,
                  :type => 'text/csv; header=present',
                  :disposition => "attachment; filename=analitico.csv"
      else
        flash.now[:error] = error_messages.join('<br/>').html_safe
        render 'analytical'
      end
    end


  end

  def synthetic
  end

end
