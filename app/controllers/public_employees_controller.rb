class PublicEmployeesController < ApplicationController
  load_and_authorize_resource
  before_action :set_public_employee, only: [:show, :edit, :update, :destroy]

  # GET /public_employees
  # GET /public_employees.json
  def index
    #@public_employees = PublicEmployee.includes(:organ, :commissioned).all
    @public_employees_grid = initialize_grid(PublicEmployee,
    :include => [:organ, :commissioned],
    :order => 'name')
  end

  # GET /public_employees/1
  # GET /public_employees/1.json
  def show
  end

  # GET /public_employees/new
  def new
    @public_employee = PublicEmployee.new
    @organs = Organ.all
    @situations = Situation.all
    @suspension_ages = SuspensionAge.all
    @commissioneds = Commissioned.all
    @marital_statuses = MaritalStatus.all
  end

  # GET /public_employees/1/edit
  def edit
    @organs = Organ.all
    @situations = Situation.all
    @suspension_ages = SuspensionAge.all
    @commissioneds = Commissioned.all
    @marital_statuses = MaritalStatus.all
  end

  def manual
    @public_employee = PublicEmployee.new
  end

  def upload_file
  end

  def default_password

  end

  def set_default_password
    if params['default_password'].blank?
      flash[:warning] = 'Por Favor, Informe a senha padrão'
    else
      PublicEmployee.where('approval_password is null').update_all(:approval_password => Encryption.encrypt(params['default_password']))
      flash[:success] = 'Operação Realizada com Sucesso'
    end
    redirect_to default_password_path
  end


  def search_for_operation
    if params[:cpf].blank? && params[:registration].blank?
      if params[:search] == 'true'
        flash.now[:warning] = 'Por favor, preencha pelo menos um dos campos para poder realizar a pesquisa'
        @employees = []
      else
        @employees = []
      end
    else
      parameters = Hash.new
      parameters[:cpf] = params[:cpf] if !params[:cpf].blank?
      parameters[:registration]= params[:registration] if !params[:registration].blank?
      employees = PublicEmployee.where(parameters)
      employees.blank? ? @employees= [] : @employees = employees
      if @employees.blank?
        flash.now[:warning] = 'Nenhum servidor encontrado.'
      end
    end
  end

  def transfer_employee
    load_public_employee_as_transfer_employee
    build_transfer_employee
  end

  def execute_transference
    #load_public_employee_as_transfer_employee
    #build_transfer_employee
    @public_employee.attributes = public_employee_params
    save_as_transfer or render :transfer_employee
  end

  def edit_contacts
    load_public_employee_as_update_contacts
    build_update_contacts
  end

  def update_contacts
    load_public_employee_as_update_contacts
    build_update_contacts
    save_as_update_contacts or render :edit_contacts
  end

  # POST /public_employees
  # POST /public_employees.json
  def create
    @public_employee = PublicEmployee.new(public_employee_params)

    respond_to do |format|

      save = true

      begin
        ActiveRecord::Base.transaction do
          @public_employee.save!
          employee_history = MarginHistory.new
          employee_history.public_employee_id = @public_employee.id
          employee_history.margin_value = @public_employee.margin_value
          employee_history.save!
        end
      rescue Exception => ex
        save = false
      end


      if save
        flash[:success] = I18n.t('feedback_messages.public_employee_create_sucess')
        format.html { redirect_to public_employees_path }
      else
        @organs = Organ.all
        @situations = Situation.all
        @suspension_ages = SuspensionAge.all
        @commissioneds = Commissioned.all
        @marital_statuses = MaritalStatus.all
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /public_employees/1
  # PATCH/PUT /public_employees/1.json
  def update
    respond_to do |format|

      valid_process = true
      message = nil

      begin
        ActiveRecord::Base.transaction do

          current_margin = @public_employee.margin_value
          margin_param = BigDecimal.new(public_employee_params[:margin_value])

          @public_employee.update!(public_employee_params)

          if current_margin != margin_param
            history = MarginHistory.new
            history.public_employee_id = @public_employee.id
            history.margin_value = @public_employee.margin_value
            history.save!
          end

        end
      rescue Exception => ex
        valid_process = false
        message = ex.message
      end

      if valid_process
        flash[:success] = I18n.t('feedback_messages.public_employee_update_sucess')
        format.html { redirect_to public_employees_path }
      else
        @organs = Organ.all
        @situations = Situation.all
        @suspension_ages = SuspensionAge.all
        @commissioneds = Commissioned.all
        @marital_statuses = MaritalStatus.all
        flash[:error] = message
        format.html { render :edit }
      end

    end
  end

  # DELETE /public_employees/1
  # DELETE /public_employees/1.json
  def destroy

    if @public_employee.loans.empty? && @public_employee.assistance_contracts.empty? && @public_employee.cards.empty?
      @public_employee.destroy
      flash[:success] = 'Servidor removido com sucesso.'
    else
      flash[:warning] = 'Servidor não pode ser removido, pois está sendo utilizado'
    end
    redirect_to public_employees_path

  end

  def generate_approval_password
    @employee = PublicEmployee.where(:id => params['id']).first
    if params['generate']
      @employee.approval_password = Encryption.encrypt(rand.to_s[2..9])
      @employee.save!
    end

    @phone = Property.where(:name => 'Telefone de contato suporte ao usuário.').take.value
    @email = Property.where(:name => 'E-mail de contato suporte ao usuário').take.value

    @approval_password = Encryption.decrypt(@employee.approval_password)
    respond_to do |format|
      format.pdf do
        pdf = ApprovalPasswordPdf.new(@employee, @phone, @email, @approval_password)
        send_data pdf.render, filemane: 'test', type:'application/pdf'
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_public_employee
      @public_employee = PublicEmployee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def public_employee_params
      params.require(:public_employee).permit(:name, :cpf, :organ_id, :registration, :admission_date, :situation_id,
                                              :phone, :birth_date, :marital_status_id, :commissioned_id,
                                              :suspension_age_id, :margin_value, :end_contract_date,
                                              :street, :number, :complement, :neighborhood, :zip_code,
                                              :city, :voter_registration, :zone, :section, :gender,
                                              :rg, :dispatcher_organ, :dispatcher_uf, :role)
    end

    def verify_margin_history_to_current_month(employee)
      exist_history = MarginHistory.where(:public_employee_id =>  employee.id).order(created_at: :asc).last

      if exist_history && (exist_history.created_at.year == Date.today.year && exist_history.created_at.month == Date.today.month)
        return exist_history
      else
        return nil
      end
    end

    def load_public_employee_as_update_contacts
      if @public_employee.class == PublicEmployee
        @public_employee = nil
      end
      @public_employee ||= update_contacts_scope.find(params[:id])
    end

    def build_update_contacts
      @public_employee ||= update_contacts_scope.build
      @public_employee.attributes = update_contacts_params
    end

    def save_as_update_contacts
      if @public_employee.save
        redirect_to public_employees_path
      end
    end

    def update_contacts_scope
      PublicEmployee::AsUpdateContacts.where(nil)
    end

    def update_contacts_params
      update_contacts_params = params[:public_employee]
      update_contacts_params ? update_contacts_params.permit(:email, :phone) : {}
    end

    def transfer_employee_scope
      PublicEmployee::AsTransferEmployee.where(nil)
    end

    def load_public_employee_as_transfer_employee
      if @public_employee.class == PublicEmployee
        @public_employee = nil
      end
      @public_employee ||= transfer_employee_scope.find(params[:id])
    end

    def public_employee_transfer_params
      public_employee_transfer_params = params[:public_employee]
      public_employee_transfer_params ? public_employee_transfer_params.permit(:organ_id, :registration) : {}
    end

    def build_transfer_employee
      @public_employee ||= transfer_employee_scope.build
      @public_employee.attributes = public_employee_transfer_params
    end

  def save_as_transfer
    if @public_employee.save
      flash[:success] = 'Servidor Transferido com sucesso'
      redirect_to public_employees_path
    end
  end

end
