class SearchFormController < ApplicationController

  authorize_resource :class => :search_form

  def search_employee
    @search_employee = SearchEmployee.new()
    @employees = []
  end

  def search
    @search_employee = SearchEmployee.new(params[:search_employee])

    if !@search_employee.cpf.blank? && !@search_employee.registration.blank?
      @employees = PublicEmployee.where(:cpf => @search_employee.cpf, :registration => @search_employee.registration)
    elsif !@search_employee.cpf.blank? && @search_employee.registration.blank?
      @employees = PublicEmployee.where(:cpf => @search_employee.cpf)
    elsif @search_employee.cpf.blank? && !@search_employee.registration.blank?
      @employees = PublicEmployee.where(:registration => @search_employee.registration)
    else
      @employees = []
    end

    respond_to do |format|
      if @search_employee.valid?
        flash[:warning] = 'Nenhum Servidor Encontrado'
        format.html {redirect_to search_employee_path}
        format.js {}
      else
        format.html {render action: 'search_employee'}
      end
    end

  end

  def loan

    if !params[:format].blank?
      session[:employee_id] = params[:format].to_i
    end

    @employee = PublicEmployee.where(:id => session[:employee_id]).first

    @loan_grid = initialize_grid(Loan)
    @loan = Loan.new

  end

  private

  def choose_grid
    cpf = flash[:cpf_search]
    registration = flash[:registration_search]

    grid = nil

    if !cpf.blank? && registration.blank?
      @search_employee.cpf = cpf
      grid = initialize_grid(PublicEmployee,
                             :include => [:organ, :commissioned],
                             :custom_order => {
                                 'public_employees.organ_id' => 'organs.name',
                                 'public_employees.commissioned_id' => 'commissioned.name'
                             },
                             :conditions => {:cpf => cpf})
    end


    if !registration.blank? && cpf.blank?
      @search_employee.registration = registration
      grid = initialize_grid(PublicEmployee,
                             :include => [:organ, :commissioned],
                             :custom_order => {
                                 'public_employees.organ_id' => 'organs.name',
                                 'public_employees.commissioned_id' => 'commissioned.name'
                             },
                             :conditions => {:registration => registration})
    end

    if cpf.blank? && registration.blank?
      grid = initialize_grid(PublicEmployee,
                             :include => [:organ, :commissioned],
                             :custom_order => {
                                 'public_employees.organ_id' => 'organs.name',
                                 'public_employees.commissioned_id' => 'commissioned.name'
                             },
                             :conditions => {:cpf => '0000000000000'})
    end

    if !cpf.blank? && !registration.blank?
      grid = initialize_grid(PublicEmployee,
                             :include => [:organ, :commissioned],
                             :custom_order => {
                                 'public_employees.organ_id' => 'organs.name',
                                 'public_employees.commissioned_id' => 'commissioned.name'
                             },
                             :conditions => {:cpf => cpf, :registration => registration})
    end

    return grid
  end
end
