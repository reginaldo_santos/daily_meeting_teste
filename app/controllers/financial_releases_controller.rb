class FinancialReleasesController < ApplicationController
  load_and_authorize_resource
  before_action :set_financial_release, only: [:show, :edit, :update, :destroy]

  # GET /financial_releases
  # GET /financial_releases.json
  def index
    @financial_releases = FinancialRelease.all
  end

  # GET /financial_releases/1
  # GET /financial_releases/1.json
  def show
  end

  def file_release

    access = IoAccess.where(:category => IoAccess::IMPORT_CARD_RELEASE).order(created_at: :desc).first

    if access && access.finished == false
      flash[:warning] = 'O arquivo esta sendo processado, por favor aguarde, a tela estará disponivel após o processamento'
      redirect_to dashboard_path
      return
    elsif access && access.finished == true && access.successful == false
      flash[:warning] = 'Arquivo fora do formato ou possui dados invalidos, por favor verifique os erros abaixo.'
    end

    @reviews = ReviewFinancialRelease.all
    @reviews_grid = initialize_grid(ReviewFinancialRelease,
    :include => [:financial_release_file])
  end

  def upload

    if params['releases_file']

      release_file_existent = FinancialReleaseFile.where(:name => params['releases_file'].original_filename).first

      if release_file_existent
        flash[:warning] = 'Informe um novo nome para seu arquivo, pois o nome especificado já foi utilizado.'
        redirect_to :action => 'file_release'
      else
        
        if 'text/plain' == params['releases_file'].content_type

          access = IoAccess.create(:category => IoAccess::IMPORT_CARD_RELEASE)

          tempfile = params['releases_file'].tempfile
          original_name = params['releases_file'].original_filename
          encoding = tempfile.external_encoding.name
          file = File.join("public", original_name)
          FileUtils.cp tempfile.path, file

          access.delay.import_card_release(original_name, current_user.id, encoding)
          
          flash[:warning] = 'Arquivo em processamento, ao terminar o processo a tela estará disponivel novamente'
          redirect_to dashboard_path
        else
          flash[:error] = 'Arquivo com extensão invalida'
          redirect_to :action => 'file_release'
        end
      end

    else
      flash[:warning] = 'Selecione um arquivo para ser importado ao sistema.'
      redirect_to :action => 'file_release'
    end



  end

  # GET /financial_releases/new
  def new
    @financial_release = FinancialRelease.new
  end

  # GET /financial_releases/1/edit
  def edit
  end

  # POST /financial_releases
  # POST /financial_releases.json
  def create
    @financial_release = FinancialRelease.new(financial_release_params)

    respond_to do |format|
      if @financial_release.save
        format.html { redirect_to @financial_release, notice: 'Financial release was successfully created.' }
        format.json { render :show, status: :created, location: @financial_release }
      else
        format.html { render :new }
        format.json { render json: @financial_release.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /financial_releases/1
  # PATCH/PUT /financial_releases/1.json
  def update
    respond_to do |format|
      if @financial_release.update(financial_release_params)
        format.html { redirect_to @financial_release, notice: 'Financial release was successfully updated.' }
        format.json { render :show, status: :ok, location: @financial_release }
      else
        format.html { render :edit }
        format.json { render json: @financial_release.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /financial_releases/1
  # DELETE /financial_releases/1.json
  def destroy
    @financial_release.destroy
    respond_to do |format|
      format.html { redirect_to financial_releases_url, notice: 'Financial release was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_financial_release
      @financial_release = FinancialRelease.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def financial_release_params
      params.require(:financial_release).permit(:card_id, :value)
    end
end
