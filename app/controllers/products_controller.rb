class ProductsController < ApplicationController
  load_and_authorize_resource
  before_action :set_product, only: [:show, :edit, :update, :destroy]


  # GET /products
  # GET /products.json
  def index
    if !params[:format].nil?
      session[:consignee_id_products] = params[:format].to_i
    end

    consignee = Consignee.find(session[:consignee_id_products])
    @products = Product.where(:consignee_id => consignee.id)
    @products_grid = initialize_grid(Product,
    :conditions => { :consignee_id => session[:consignee_id_products] })
    @consignee = consignee.name

  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
    @product_types = ProductType.where('id not in (?)', Product.select(:product_type_id).where(:consignee_id => 4))
    @consignee = Consignee.where(:id => session[:consignee_id_products]).take.name
  end

  # GET /products/1/edit
  def edit
    @product_types = ProductType.where('id not in (?)', Product.select(:product_type_id).where(:consignee_id => 4))
    @consignee = Consignee.where(:id => session[:consignee_id_products]).take.name
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    @product.consignee_id = session[:consignee_id_products]
    @product.event_code.downcase!

    respond_to do |format|
      if @product.save
        flash[:success] = 'Produto criado com sucesso'
        format.html { redirect_to products_path(session[:consignee_id_products]) }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      @product.consignee_id = session[:consignee_id_products]
      @product.event_code.downcase!
      if @product.update(product_params)
        flash[:warning] = 'Produto atualizado com sucesso'
        format.html { redirect_to products_path(session[:consignee_id_products]) }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    products = Product.where(:consignee_id => session[:consignee_id_products])

    if products.length == 1
      flash[:warning] = 'O produto selecionado não pode ser removido, pois a consignatária deve ter pelo menos um produto.'
    else
      @product.destroy
      flash[:success] = 'Produto removido com sucesso.'
    end

    redirect_to products_url

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:event_code, :deadline, :product_type_id)
    end
end
