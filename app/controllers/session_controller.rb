class SessionController < ApplicationController

  authorize_resource :class => :session

  layout 'login_layout'


  def login
    build_sign_in
  end

  def login_attempt

    build_sign_in

    if true
      reset_session

      if @sign_in.user && @sign_in.user_blocked?
        flash[:error] = I18n.t('authentication.blocked_user')
        redirect_to :action => 'login'
      else
        if @sign_in.save
          session[:user_id] = @sign_in.user.id
          session[:last_interaction] = Time.now

          redirect_to dashboard_path
        else
          user = @sign_in.user
          if user
            user.update_attribute(:login_attempts, user.login_attempts + 1)
            user.update_attribute(:blocked, true) if user.login_attempts >= 3
          end
          render 'login'
        end
      end

    else
      flash[:error] = 'Captcha incorreto'
      redirect_to :action => 'login'
    end


  end

  def logout
    session[:user_id] = nil
    session[:last_interaction] = nil
    flash[:success] = 'Você saiu do sistema.'
    redirect_to :action => 'login'
  end

  private

    def build_sign_in
      @sign_in = SignIn.new(sign_in_params)
    end

    def sign_in_params
      sign_in_params = params[:sign_in]
      sign_in_params.permit(:username, :password) if sign_in_params
    end

end
