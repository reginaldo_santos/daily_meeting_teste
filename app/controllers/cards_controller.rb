class CardsController < ApplicationController
  authorize_resource :class => :card_controller

  before_action :set_card, only: [:show, :edit, :update, :destroy]

  # GET /cards
  # GET /cards.json
  def index
    @cards = Card.all
  end

  # GET /cards/1
  # GET /cards/1.json
  def show
  end

  def manage_cards
    @cards = Card.where(:consignee_id => current_user.get_consignee).order('status').order('created_at ASC')

    if current_user.get_consignee
      if current_user.isS?(User::CONSIGNEE_CARD_MASTER) || current_user.isS?(User::FORMALIZATION_CARD)
        @manage_card_grid = initialize_grid(Card,
                                            :include => [:public_employee],
                                            :conditions => { :consignee_id => current_user.get_consignee },
                                            order: 'cards.created_at',
                                            order_direction: 'desc')
      else
        @manage_card_grid = initialize_grid(Card,
                                            :include => [:public_employee],
                                            :conditions => { :consignee_id => current_user.get_consignee,
                                                             :operator_id => current_user.operator.id },
                                            order: 'cards.created_at',
                                            order_direction: 'desc')
      end

    else
      @manage_card_grid = initialize_grid(Card,
                                          :include => [:public_employee],
                                          order: 'cards.created_at',
                                          order_direction: 'desc')
    end
  end

  # GET /cards/new
  def new

    if !params[:format].blank?
      session[:employee_id] = params[:format].to_i
    end

    @employee = PublicEmployee.where(:id => session[:employee_id]).first

    @card = Card.new
  end

  # GET /cards/1/edit
  def edit
    card = Card.find(params[:id])

    @employee = card.public_employee
  end

  # POST /cards
  # POST /cards.json
  def create
    @card = Card.new(card_params)
    @card.public_employee_id = session[:employee_id]
    @card.operator = current_user.operator
    @card.consignee = current_user.get_consignee

    property = Property.where(:category => Property::CARD_LIMIT).take

    if @card.public_employee.cards.where(:status => [Card::AUTORIZADO_SERVIDOR, Card::AUTORIZADO_CONSIGNATARIA]).count > property.value.to_i
      flash[:error] = 'Operação não pôde ser realizada, pois o limite de cartões para o servidor especificado foi atingido'
      redirect_to search_employee_path
    else
      respond_to do |format|
        if @card.save
          flash[:success] = 'Reserva de margem realizada com sucesso.'
          format.html { redirect_to search_employee_path }
        else
          @employee = @card.public_employee
          format.html { render :new }
        end
      end
    end
  end

  # PATCH/PUT /cards/1
  # PATCH/PUT /cards/1.json
  def update
    @card.approval_password = params[:card][:approval_password]

    if @card.valid?
      if !@card.approval_password.blank?
        @card.update_attribute(:status, Card::AUTORIZADO_SERVIDOR)
        flash[:success] = 'Cartão atualizado com sucesso.'
        redirect_to manage_cards_path
      end
    else
      @employee = @card.public_employee
      render :edit
    end

  end

  # DELETE /cards/1
  # DELETE /cards/1.json
  def destroy
    @card.destroy
    respond_to do |format|
      format.html { redirect_to cards_url, notice: 'Card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def approve
    load_card(Card::AsApproveCard)
    build_approve_card(Card::AsApproveCard)
  end

  def approve_execute
    load_card(Card::AsApproveCard)
    build_approve_card(Card::AsApproveCard)

    ActiveRecord::Base.transaction do
      save_card or render :approve
    end
  end

  def refuse
    load_card(Card::AsRefuseCard)
    build_refuse_card(Card::AsRefuseCard)
  end

  def refuse_execute

    load_card(Card::AsRefuseCard)
    build_refuse_card(Card::AsRefuseCard)

    ActiveRecord::Base.transaction do
      save_card or render :refuse
    end

  end

  def cancel
    load_card(Card::AsCancelCard)
    build_cancel_card(Card::AsCancelCard)
  end

  def cancel_execute

    load_card(Card::AsCancelCard)
    build_cancel_card(Card::AsCancelCard)

    ActiveRecord::Base.transaction do
      save_card or render :cancel
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card
      @card = Card.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def card_params
      params.require(:card).permit(:proposal_number, :discount_value, :approval_password)
    end

    def make_manege_card_grid
      grid = initialize_grid(Card,
                             :include => [:consignee],
                             :custom_order => {
                                 'loans.consignee_id' => 'consignees.name'
                             },
                             :conditions => ["public_employee_id = ? and status in (?)", @employee.id, [Loan::APROVADO, Loan::EM_FOLHA]])
    end

    def validate_approval_password(password, employee)

    end

    def load_card(scope)
      if @card.class == Card
        @card = nil
      end
      @card ||= card_scope(scope).find(params[:id])
    end

    def build_approve_card(scope)
      @card ||= card_scope(scope).build
      @card.attributes = approve_card_params
    end

    def build_refuse_card(scope)
      @card ||= card_scope(scope).build
      @card.attributes = refuse_card_params
    end

    def build_cancel_card(scope)
      @card ||= card_scope(scope).build
      @card.attributes = cancel_card_params
    end

    def save_card
      if @card.save

        card = params[:card][:action]

        case card
          when 'approve_execute'
            flash[:success] = 'Cartão Aprovado com sucesso.'
          when 'refuse_execute'
            flash[:success] = 'Cartão Reprovado com sucesso'
          else
            flash[:success] = 'Operação efetuada com sucesso'
        end

        redirect_to manage_cards_path
      end
    end

    def card_scope(scope)
      scope.where(nil)
    end

    def approve_card_params
      approve_card_params = params[:card]
      approve_card_params ? approve_card_params.permit(:proposal_number, :observation, :operator_id) : {}
    end

    def refuse_card_params
      refuse_card_params = params[:card]
      refuse_card_params ? refuse_card_params.permit(:observation, :operator_id) : {}
    end

    def cancel_card_params
      cancel_card_params = params[:card]
      cancel_card_params ? cancel_card_params.permit(:observation, :operator_id, :password) : {}
    end

end
