class ConsigneesController < ApplicationController
  authorize_resource :class => :consignee

  def index
    load_consignees
    @consignees_grid = initialize_grid(Consignee)
  end

  def show
    load_consignee
  end

  def new
    build_consignee
  end

  def edit
    load_consignee
    build_consignee
  end

  def create
    build_consignee
    save_consignee or render :new
  end

  def update
    load_consignee
    build_consignee
    save_consignee or render :edit
  end

  def destroy
    load_consignee
    if @consignee.attendance_locals.empty? && @consignee.products.empty? && @consignee.operators.empty? &&
        @consignee.loans.empty? && @consignee.assistance_contracts.empty? && @consignee.cards.empty? &&
        @consignee.portability_requests.empty?
      @consignee.destroy
      flash[:success] = 'Consignatária removida com sucesso.'
    else
      flash[:warning] = 'Consignatária não pode ser removida, pois está sendo utilizada'
    end

    redirect_to consignees_path
  end

  private

    def load_consignees
      @consignees ||= consignee_scope.to_a
    end

    def load_consignee
      @consignee ||= consignee_scope.find(params[:id])
    end

    def build_consignee
      @consignee ||= consignee_scope.build
      @consignee.attributes = consignee_params
    end

    def save_consignee
      if @consignee.save
        flash[:success] = 'Consignatária cridada com sucesso.' if action_name == 'create'
        flash[:success] = 'Consignatária atualizada com sucesso.' if action_name == 'update'
        redirect_to consignees_path
      end
    end

    def consignee_scope
      Consignee.where(nil)
    end

    def consignee_params
      consignee_params = params[:consignee]
      consignee_params ? consignee_params.permit(:name, :cnpj, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :contact, :post, :phone_number, :second_contact, :second_post, :second_phone_number) : {}
    end
end
