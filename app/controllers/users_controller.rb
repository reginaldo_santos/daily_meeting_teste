#encoding: utf-8
#require 'will_paginate/array'

class UsersController < ApplicationController

  authorize_resource :class => :users

  layout 'login_layout', only: [:new_employee_user, :create_employee_user, :inform_login_and_password, :update_login_for_employee_user]
  before_action :set_user, only: [:show, :destroy]

  def index
   @users_grid = make_grid
  end

  def dashboard
  end

  def show
  end

  def new
    build_user
    @user.build_operator
    set_select_options
  end

  def edit
    load_user
    build_user
    set_select_options
  end

  def create
    build_user
    @user.operator.roles_for_save = @user.roles
    generated_password = rand.to_s[2..9]
    @user.password = generated_password
    @user.password_confirmation = generated_password
    @user.login_attempts = 0
    save_user
  end

  def update
    load_user
    build_user
    save_user
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    operator = @user.operator

    if operator.loans.empty? && operator.assistance_contracts.empty? &&
         operator.cards.empty? && operator.financial_releases.empty? &&
    operator.loan_observations.empty? && operator.variations.empty? &&
    operator.card_observations.empty?
      @user.operator.destroy
      flash[:sucess] = 'Usuário removido com sucesso'
    else
      flash[:warning] = 'Usurário não pode ser removido, pois esta associado com operações no sistema'
    end

    redirect_to users_url
  end

  def change_personal_data

    if current_user.operator_id
      @operator = current_user.operator
    else
      @public_employee = current_user.public_employee
    end

  end

  def update_personal_data

    if current_user.operator

      @operator = Operator.where(:id => params[:operator_id].to_i).first
      @operator.email = params['operator']['email']
      @operator.company_phone_number = params['operator']['company_phone_number']
      if @operator.save
        flash[:success] = 'Dados pessoais atualizado com sucesso.'
        redirect_to dashboard_path
      else
        render :change_personal_data
        return
      end
    else
      @public_employee = PublicEmployee.where(:id => params[:public_employee_id].to_i).first
      @public_employee.street = params['public_employee']['street']
      @public_employee.number = params['public_employee']['number']
      @public_employee.complement = params['public_employee']['complement']
      @public_employee.neighborhood = params['public_employee']['neighborhood']
      @public_employee.zip_code = params['public_employee']['zip_code']
      @public_employee.city = params['public_employee']['city']
      @public_employee.email = params['public_employee']['email']
      @public_employee.phone = params['public_employee']['phone']
      @public_employee.approval_password = params['public_employee']['approval_password']
      @public_employee.marital_status_id = params['public_employee']['marital_status_id'].to_i
      if @public_employee.save
        flash[:success] = 'Dados pessoais atualizado com sucesso.'
        redirect_to dashboard_path
      else
        render :change_personal_data
        return
      end
    end

  end

  def change_password
    load_user_password(User::AsChangePassword)
    build_user_password(User::AsChangePassword)
  end

  def update_password
    load_user_password(User::AsChangePassword)
    build_user_password(User::AsChangePassword)

    save_user_change_password or render :change_password
  end

  def block
    user = User.where(:id => params[:format].to_i).first
    user.update_attribute(:blocked, true)

    flash[:warning] = I18n.t('feedback_messages.block_user_success')
    redirect_to :action => 'index'
  end

  def unlock
    user = User.where(:id => params[:format].to_i).first
    user.update_attribute(:blocked, false)
    user.update_attribute(:login_attempts, 0)

    flash[:warning] = I18n.t('feedback_messages.unlock_user_success')
    redirect_to :action => 'index'
  end

  def new_employee_user
  end

  def create_employee_user

    errors = []

    if params['registration'].blank?
      errors << 'Mátricula deve ser preenchida'
    end

    if params['organ'].blank?
      errors << 'Órgão deve ser preenchido'
    end

    if params['approval_password'].blank?
      errors << 'Senha de aprovação deve ser preenchida'
    end

    if !errors.empty?
      flash.now[:error] = errors.join('<br/>').html_safe
      render :new_employee_user
      return
    else
      if verify_recaptcha
        organ = Organ.where(:code => params['organ']).first
        if organ
          employee = PublicEmployee.where(:registration => params['registration'], :organ_id => organ.id).first
          if employee
            if employee.user

              flash.now[:error] = 'A matricula informada já possui um usuário.'
              render :new_employee_user
              return

            else

              if employee.email

                if Encryption.encrypt(params['approval_password']) == employee.approval_password
                  user = User.new
                  user.login = User::UNDEFINED + employee.cpf
                  user.password = User::UNDEFINED
                  user.password_confirmation = User::UNDEFINED
                  user.blocked = true
                  user.skip_validation = true
                  user.login_attempts = 0
                  user.public_employee = employee
                  user.profiles << Profile.where(:name => 'Employee').first
                  user.save!

                  user.send_create_user_instructions

                  flash[:warning] = 'As instruções de como criar o seu usuário foi enviada para o seu email'
                  redirect_to root_path
                  return
                else
                  flash.now[:error] = 'Senha de aprovação incorreta'
                  render :new_employee_user
                  return
                end
              else
                flash.now[:error] = 'A matricula informada nao possue endereço de email associado, por favor se dirija ao setor responsavel.'
                render :new_employee_user
                return
              end

            end
          else
            flash.now[:error] = 'Mátricula inexistente'
            render :new_employee_user
            return
          end
        else
          flash[:error] = 'Órgão inválido'
          render :new_employee_user
          return
        end
      else
        flash.now[:error] = 'Captcha incorreto'
        render :new_employee_user
        return
      end
    end
  end

  def inform_login_and_password
    @user = User.find_by_create_user_token(params[:token])
    @user.login = nil
  end

  def update_login_for_employee_user

    @user = User.where('create_user_token = ?', params[:token]).first

    if @user.update(user_params)
       flash[:success] = 'Seu usuário foi criado com sucesso.'
       redirect_to root_path
    else
      render :inform_login_and_password
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:login, :login_attempts, :password, :password_confirmation, :current_password, :origin, :blocked,
                                   operator_attributes: [:name, :cpf, :company_phone_number, :consignee_id, :attendance_local_id, :email, :current_user_roles, :hidden_id, :roles_for_save],
                                   role_ids: [])
    end

    def user_params_as_operator
      user_params_as_operator = params[:user]
      user_params_as_operator ? user_params_as_operator.permit(:login, :login_attempts, :password, :password_confirmation, :current_password, :origin, :blocked, :operator_id,
                                   operator_attributes: [:id, :name, :cpf, :company_phone_number, :consignee_id, :attendance_local_id, :email, :current_user_roles, :hidden_id, :roles_for_save],
                                   role_ids: []) : {}
    end

    def build_user
      @user ||= user_scope.build
      operator_id = @user.operator.id if @user.operator
      @user.attributes = user_params_as_operator
      @user.operator_id = operator_id
    end

    def user_scope
      User::AsOperator.where(nil)
    end

    def load_user
      @user ||= user_scope.find(params[:id])
    end

    def set_select_options
      user = current_user
      @current_user_roles = user.roles
      @operator_id = user.operator.id
      if user.isS?(User::ADMIN)
        @attendance_locals = AttendanceLocal.all
      else
        @attendance_locals = AttendanceLocal.where(consignee_id: user.operator.consignee.id)
      end

      @roles_options = Role.all if user.isS?(User::ADMIN)
      @roles_options = Role.where(category: Role::LOAN_GROUP) if user.isS?(User::CONSIGNEE_LOAN_MASTER) || user.isS?(User::USERS_LOAN)
      @roles_options = Role.where(category: Role::ASSISTANCE_GROUP) if user.isS?(User::CONSIGNEE_ASSISTANCE_MASTER) || user.isS?(User::USERS_ASSISTANCE)
      @roles_options = Role.where(category: Role::CARD_GROUP) if user.isS?(User::CONSIGNEE_CARD_MASTER) || user.isS?(User::USERS_CARD)
      @roles_options = Role.where(category: Role::RH_GROUP) if user.isS?(User::RH_MASTER)
    end

    def save_user
      if @user.save
        flash[:success] = 'Usuário cridado com sucesso.' if action_name == 'create'
        flash[:success] = 'Usuário atualizado com sucesso.' if action_name == 'update'
        redirect_to users_path
      else
        set_select_options
        render :edit
      end
    end

    def save_user_change_password
      if @user.save
        flash[:sucess] = 'Senha Alterada com sucesso.'
        redirect_to dashboard_path
      end
    end

    def make_grid
      user = current_user

      if user.isS?(User::RH_MASTER)
        grid = initialize_grid(User.joins(:operator).where('operators.consignee_id is null and operators.attendance_local_id is null'))
      elsif user.isS?(User::CONSIGNEE_LOAN_MASTER) || user.isS?(User::USERS_LOAN) ||
            user.isS?(User::CONSIGNEE_CARD_MASTER) || user.isS?(User::USERS_CARD) ||
            user.isS?(User::CONSIGNEE_CARD_MASTER) || user.isS?(User::USERS_CARD)
        consignee_id = current_user.get_consignee.id
        attendance_locals = AttendanceLocal.where(:consignee_id => consignee_id)
        if attendance_locals.blank?
          grid = initialize_grid(User.joins(:operator).where('operators.consignee_id = ?', consignee_id))
        else
          grid = initialize_grid(User.joins(:operator).where('operators.consignee_id = ? or operators.attendance_local_id in (?)', consignee_id, attendance_locals.to_a))
        end
      else
        grid = initialize_grid(User)
      end

    end

  def user_scope_password(scope)
    scope.where(nil)
  end

  def load_user_password(scope)
    if @user.class == User
      @user = nil
    end
    @user ||= user_scope_password(scope).find(params[:id])
  end

  def build_user_password(scope)
    @user ||= user_scope_password(scope).build
    @user.attributes = change_password_params
  end

  def change_password_params
    change_password_params = params[:user]
    change_password_params ? change_password_params.permit(:new_pwd, :pwd_confirmation, :actual_password) : {}
  end
end
