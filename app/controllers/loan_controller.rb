class LoanController < ApplicationController

  load_and_authorize_resource

  def search_for_portability
    if !params[:cpf].blank? && !params[:registration].blank?
      employee = PublicEmployee.where(:cpf => params[:cpf], :registration => params[:registration]).take
      employee.nil? ? @loans = [] : @loans = employee.loans.where('status = ? and consignee_id != ? and public_employee_id = ?', Loan::EM_FOLHA, current_user.get_consignee.id, employee.id)
      if @loans.empty?
        flash.now[:warning] = 'Nenhum contrato encontrado para o servidor especificado'
      end
    else
      if params[:search] == 'true'
        error_messages = []
        error_messages << '* CPF deve ser preenchido' if params[:cpf].blank?
        error_messages << '* Matricula deve ser preenchida' if params[:registration].blank?
        flash.now[:warning] = error_messages.join('<br/>').html_safe
        @loans = []
      else
        @loans = []
      end
    end
  end

  def request_portability
    loan = Loan.find(params[:loan_id])
    buyer_id = current_user.get_consignee.id
    request_saved = PortabilityRequest.where(:loan_id => loan.id, :status => PortabilityRequest::AGUARDANDO_RESPOSTA,
    :buyer_id => buyer_id).take

    if request_saved
      flash[:warning] = 'Já existe uma solicitação pendente para o contrato especificado.'
      redirect_to :back
    else
      request = PortabilityRequest.new(:seller_id => loan.consignee_id, :buyer_id => buyer_id,
                                       :loan_id => loan.id, :status => PortabilityRequest::AGUARDANDO_RESPOSTA)
      if request.save
        flash[:success] = 'Solicitação realizada com sucesso'
        redirect_to :back
      end
    end

  end

  def edit
    @loan = Loan.find(params[:id])

    @employee = @loan.public_employee
    @operation = @loan.origin_operation
    @loan_grid = make_grid(@loan.origin_operation)
    @action = 'update'
  end

  def loans_portability
    employee = PublicEmployee.find(params[:id])
    test = 'test'
  end

  def show
  end

  def update
    @loan.approval_password = params[:loan][:approval_password]

    if @loan.valid?
      if !@loan.approval_password.blank?
        ActiveRecord::Base.transaction do
          @loan.update_attribute(:status, Loan::AUTORIZADO_SERVIDOR)
          @loan.children.each do |child|
            if @loan.contract_type == Loan::REFIN
              child.status = Loan::REFINANCIADO
            elsif @loan.contract_type == Loan::PORT
              child.status = Loan::AGUARDANDO_PORTABLIDADE
            end
            child.installments_paid = child.deadline
            child.balance_due = BigDecimal.new('0')
            child.save!
          end
        end
      end
      flash[:success] = 'Empréstimo atualizado com sucesso'
      redirect_to manage_loan_path
    else
      @employee = @loan.public_employee
      @operation = @loan.origin_operation
      @loan_grid = make_grid(@loan.origin_operation)
      @action = 'update'
      render :edit
    end
  end

  def manage_loan
    @loans = Loan.where(:consignee_id => current_user.get_consignee)

    if current_user.get_consignee
      if current_user.isS?(User::CONSIGNEE_LOAN_MASTER) || current_user.isS?(User::FORMALIZATION_LOAN)
        @manage_loans_grid = initialize_grid(Loan,
                                             :include => [:public_employee],
                                             :conditions => { :consignee_id => current_user.get_consignee },
                                             order: 'loans.created_at',
                                             order_direction: 'desc')
      else
        @manage_loans_grid = initialize_grid(Loan,
                                             :include => [:public_employee],
                                             :conditions => { :consignee_id => current_user.get_consignee,
                                                              :operator_id => current_user.operator.id },
                                             order: 'loans.created_at',
                                             order_direction: 'desc')
      end

    else
      @manage_loans_grid = initialize_grid(Loan,
                                           :include => [:public_employee],
                                           order: 'loans.created_at',
                                           order_direction: 'desc'
      )
    end
  end

  def refuse
    load_loan(Loan::AsRefuseLoan)
    build_loan(Loan::AsRefuseLoan)
  end

  def refuse_execute

    load_loan(Loan::AsRefuseLoan)
    build_loan(Loan::AsRefuseLoan)

    ActiveRecord::Base.transaction do
      save_loan or render :refuse
    end
  end

  def liquidate
    load_loan(Loan::AsLiquidateLoan)
    build_loan(Loan::AsLiquidateLoan)
  end

  def liquidate_execute
    load_loan(Loan::AsLiquidateLoan)
    build_loan(Loan::AsLiquidateLoan)

    ActiveRecord::Base.transaction do
      save_loan or render :liquidate
    end
  end

  def portability_accept

  end

  def suspended
    load_loan(Loan::AsSuspendLoan)
    negotiation = Negotiation.where(:child_id => @loan.id, :status => Negotiation::OPEN).first
    if negotiation
      flash[:warning] = 'Contrato não pode ser suspenso, pois está associado a um contrato que ainda espera aprovação do servidor.'
      redirect_to manage_loan_path
    else
      build_suspend_loan(Loan::AsSuspendLoan)
    end
  end

  def suspended_execute

    load_loan(Loan::AsSuspendLoan)
    build_suspend_loan(Loan::AsSuspendLoan)

    ActiveRecord::Base.transaction do
      save_loan or render :suspended
    end

  end

  def approve
    load_loan_as_approve_loan
    build_approve_loan
  end

  def approve_execute
    load_loan_as_approve_loan
    build_approve_loan

    ActiveRecord::Base.transaction do
      save_approve_loan
    end

  end

  def cancel
    load_loan_as_cancel_loan
    build_cancel_loan
  end

  def cancel_execute
    load_loan_as_cancel_loan
    build_cancel_loan

    ActiveRecord::Base.transaction do
      save_loan or render :cancel
    end
  end

  def new

    if !params[:user_id].blank?
      session[:employee_id] = params[:user_id].to_i
    end

    @employee = PublicEmployee.where(:id => session[:employee_id]).first
    @operation = params[:operation]

    if (@operation == 'REFIN' && params[:type] == 'RETAIN') || @operation == 'PORT'
      request = PortabilityRequest.find(params[:request_portability_id])
      @will_be_bought = request.loan
      @portability_request_id = request.id
      @operation_type = 'RETAIN'
    else
      @loan_grid = make_grid(params[:operation])
    end

    @loan = Loan.new
    @action = 'create'

  end

  def new_refin
    initialize_params(params[:id], params[:operation_type], params[:request_id])
    @loan = Loan.new
  end

  def new_portability
    initialize_portability_params(params[:id], params[:request_id])
    @loan = Loan.new
  end

  def create

    @loan = Loan.new(loan_params)

    ActiveRecord::Base.transaction do
        create_new(@loan)
    end

  end

  def create_refin

    @loan = Loan.new(loan_params)

    ActiveRecord::Base.transaction do
      # verifica se o refin está relacionado com portabilidade, ou seja, se o usuário clicou em reter, senão é um processo de refin comum
      if @loan.operation_type == 'RETAIN'
        request = PortabilityRequest.find(@loan.portability_request_id)
        loan_selected = request.loan
        @loan.purchased_loans_ids = []
        if @loan.portability_balance_due.blank?
          flash.now[:error] = 'Por favor preencha o saldo dever do contrato que está sendo refinanciado'
          initialize_params(@loan.public_employee.id, @loan.operation_type, @loan.portability_request_id)
          render :new_refin
          return
        else
          loan_selected.update_attributes(:balance_due_release => BigDecimal.new(@loan.portability_balance_due))
          @loan.purchased_loans_ids << loan_selected.id
        end
      else
        @loan.purchased_loans_ids = []
        if params['grid']
          params['grid']['selected'].each do |selected|
            balance_due_release = params["value_#{selected.to_i}"]
            #verifica se o saldo devedor foi informado
            if balance_due_release.blank?
              flash.now[:error] = 'Por favor, preencha o saldo devedor do(s) contrato(s)'
              initialize_params(@loan.public_employee.id, @loan.operation_type, @loan.portability_request_id)
              render :new_refin
              return
            else
              loan_selected = Loan.where(:id => selected.to_i).first
              loan_selected.update_attributes(:balance_due_release => BigDecimal.new(balance_due_release))
              @loan.purchased_loans_ids << loan_selected.id
            end
          end
        else
          flash.now[:error] = 'Por favor, selecione o(s) contrato(s) que deseja refinanciar e preencha seus saldos devedores'
          initialize_params(@loan.public_employee.id, @loan.operation_type, @loan.portability_request_id)
          render :new_refin
          return
        end
      end

      if @loan.save
        @loan.children.each do |child|
          child.status = Loan::REFINANCIADO
          child.save!
          requests = child.portability_requests.where('status in (?)', [PortabilityRequest::AGUARDANDO_RESPOSTA, PortabilityRequest::RESPONDIDO])
          requests.each do |request|
            request.update_attributes(:status => PortabilityRequest::CANCELADO)
          end
        end
        flash[:success] = I18n.t('feedback_messages.loan_success')
        if @loan.operation_type == 'RETAIN'

          redirect_to manage_portability_path
          return
        else
          redirect_to search_employee_path
          return
        end
      else
        @employee = @loan.public_employee
        @operation = @loan.origin_operation
        @loan_grid = make_grid(params[:operation])
        render :new_refin
      end
    end
  end

  def create_portability
    @loan = Loan.new(loan_params)

    ActiveRecord::Base.transaction do
      request = PortabilityRequest.find(@loan.portability_request_id)
      loan_selected = request.loan
      @loan.purchased_loans_ids = []
      @loan.purchased_loans_ids << loan_selected.id

      if @loan.save
        flash[:success] = I18n.t('feedback_messages.loan_success')
        redirect_to manage_portability_path
      else
        @employee = @loan.public_employee
        @operation = @loan.origin_operation
        initialize_portability_params(@loan.public_employee.id, @loan.portability_request_id)
        render :new_portability
      end
    end
  end

  private

    def create_new(loan)

      property = Property.where(category: Property::LOAN_LIMIT).take
      if loan.public_employee.loans.where(:status => [Loan::APROVADO, Loan::EM_FOLHA, Loan::AUTORIZADO_SERVIDOR]).count > property.value.to_i
        flash[:error] = 'Não foi possivel realizar a operação, pois o limite de emprestimos para o servidor especificado foi atingido.'
        redirect_to search_employee_path
      else
        ActiveRecord::Base.transaction do
          if loan.save
            flash[:success] = I18n.t('feedback_messages.loan_success')
            redirect_to search_employee_path
          else
            @employee = loan.public_employee
            @operation = loan.origin_operation
            @loan_grid = make_grid(loan.origin_operation)
            render :new
          end
        end
      end

    end

    def initialize_params(employee_id, operation_type, request_id)
      @employee = PublicEmployee.find(employee_id)
      @operation = 'REFIN'

      if operation_type == 'RETAIN'
        request = PortabilityRequest.find(request_id)
        @will_be_bought = request.loan
        @portability_request_id = request.id
        @operation_type = 'RETAIN'
      else
        @loan_grid = make_grid(@operation)
      end

      @action = 'create'
    end

    def initialize_portability_params(employee_id, request_id)
      @employee = PublicEmployee.find(employee_id)
      @operation = 'PORT'
      @operation_type = 'PORT'

      request = PortabilityRequest.find(request_id)
      @will_be_bought = request.loan
      @portability_request_id = request.id

      @action = 'create'
    end


    def loan_params
      params.require(:loan).permit(:contract_value, :liquid_value, :deadline, :installment_value,
                                   :approval_password, :cet, :interest_tax, :others, :public_employee_id, :consignee_id_portability,
                                   :origin_operation, :operator_id, :consignee_id, :portability_request_id, :operation_type, :portability_balance_due)
    end

    def make_grid(operation)
      conditions = ['public_employee_id = ? and status in (?)', @employee.id, [Loan::APROVADO, Loan::EM_FOLHA]]

      if operation == 'NEW'
        conditions = ['public_employee_id = ? and status in (?)', 0, [Loan::APROVADO, Loan::EM_FOLHA]]
      elsif operation == 'REFIN'
        conditions = ['public_employee_id = ? and status in (?) and consignee_id = ?', @employee.id, [Loan::APROVADO, Loan::EM_FOLHA], current_user.get_consignee.id]
      elsif operation == 'PORT'
        conditions = ['public_employee_id = ? and status in (?) and consignee_id not in (?)', @employee.id, [Loan::APROVADO, Loan::EM_FOLHA], current_user.get_consignee.id]
      end

      grid = initialize_grid(Loan,
                             :include => [:consignee],
                             :custom_order => {
                                 'loans.consignee_id' => 'consignees.name'
                             },
                             :conditions => conditions)

      return grid
    end

  def load_loan_as_approve_loan
    # TODO como nao consegui encontrar aonde estava sendo setado @loan como Loan tive que forcar @loan para nil, obtendo o comportamento esperado.
    if @loan.class == Loan
      @loan = nil
    end
    @loan ||= approve_loan_scope.find(params[:id])
  end

  def build_approve_loan
    @loan ||= approve_loan_scope.build
    @loan.attributes = approve_loan_params
  end

  def save_loan
    if @loan.save
      if params[:loan][:action] == 'refuse_execute'
        flash[:success] = 'Contrato Reprovado com sucesso'
      elsif params[:loan][:action] == 'suspended_execute'
        flash[:success] = 'Contrato Suspenso com sucesso'
      elsif params[:loan][:action] == 'cancel_execute'
        flash[:success] = 'Reserva cancelada com sucesso'
      elsif params[:loan][:action] == 'liquidate_execute'
        flash[:success] = 'Contrato liquidado com sucesso'
      else
        flash[:success] = "Contrato Aprovado com sucesso"
      end
      redirect_to manage_loan_path
    end
  end

  def save_approve_loan
    if @loan.save
      flash[:success] = 'Contrato Aprovado Com Sucesso.'
      redirect_to manage_loan_path
    else
      render :approve
    end
  end

  def approve_loan_scope
    Loan::AsApproveLoan.where(nil)
  end

  def approve_loan_params
    approve_loan_params = params[:loan]
    approve_loan_params ? approve_loan_params.permit(:contract_number, :cet, :interest_tax, :others, :observation, :operator_id) : {}
  end

  def load_loan_as_cancel_loan
    # TODO como nao consegui encontrar aonde estava sendo setado @loan como Loan tive que forcar @loan para nil, obtendo o comportamento esperado.
    if @loan.class == Loan
      @loan = nil
    end
    @loan ||= cancel_loan_scope.find(params[:id])
  end

  def build_cancel_loan
    @loan ||= cancel_loan_scope.build
    @loan.attributes = cancel_loan_params
  end

  def cancel_loan_scope
    Loan::AsCancelLoan.where(nil)
  end

  def cancel_loan_params
    cancel_loan_params = params[:loan]
    cancel_loan_params ? cancel_loan_params.permit(:observation, :approval_password, :operator_id) : {}
  end

  def load_loan(scope)
    # TODO como nao consegui encontrar aonde estava sendo setado @loan como Loan tive que forcar @loan para nil, obtendo o comportamento esperado.
    if @loan.class == Loan
      @loan = nil
    end
    @loan ||= loan_scope(scope).find(params[:id])
  end

  def build_loan(scope)
    @loan ||= loan_scope(scope).build
    @loan.attributes = refuse_loan_params
  end

  def loan_scope(scope)
    scope.where(nil)
  end

  def refuse_loan_params
    refuse_loan_params = params[:loan]
    refuse_loan_params ? refuse_loan_params.permit(:observation, :operator_id) : {}
  end

  def build_suspend_loan(scope)
    @loan ||= loan_scope(scope).build
    @loan.attributes = suspend_loan_params
  end

  def suspend_loan_params
    suspend_loan_params = params[:loan]
    suspend_loan_params ? suspend_loan_params.permit(:observation, :operator_id) : {}
  end



end
