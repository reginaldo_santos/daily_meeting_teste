class ReturnCodesController < ApplicationController
  load_and_authorize_resource

  def index
    load_return_codes
    @return_codes_grid = initialize_grid(ReturnCode)
  end

  def show
    load_return_code
  end

  def new
    build_return_code
  end

  def edit
    load_return_code
    build_return_code
  end

  def create
    build_return_code
    save_return_code or render :new
  end

  def update
    load_return_code
    build_return_code
    save_return_code or render :edit
  end

  def destroy
    if ReturnCode.count == 1
      flash[:warning] = 'Código de retorno não pode ser removido, pois o sistema deve ter pelo menos um codigo de retorno cadastro.'
    else
      @return_code.destroy
      flash[:success] = 'Código de retorno removido com sucesso.'
    end

    redirect_to return_codes_path
  end

  private

    def load_return_codes
      @return_codes ||= return_code_scope.to_a
    end

    def load_return_code
      @return_code ||= return_code_scope.find(params[:id])
    end

    def build_return_code
      @return_code ||= return_code_scope.build
      @return_code.attributes = return_code_params
    end

    def save_return_code
      if @return_code.save
        flash[:success] = 'Código de retorno cridado com sucesso.' if action_name == 'create'
        flash[:success] = 'Codigo de retorno atualizado com sucesso.' if action_name == 'update'
        redirect_to return_codes_path
      end
    end

    def return_code_scope
      ReturnCode.where(nil)
    end

    def return_code_params
      return_code_params = params[:return_code]
      return_code_params ? return_code_params.permit(:code, :description, :obs) : {}
    end
end
