class VariationsController < ApplicationController

  authorize_resource :class => :variation

  before_action :set_variation, only: [:show, :edit, :update, :destroy]

  # GET /variations
  # GET /variations.json
  def index

    if params[:assistance_id].blank?
      @assistance_id = session[:assistance_contract_id]
      @variations = Variation.where(:assistance_contract_id => @assistance_id)
      @variations_grid = initialize_grid(Variation,
      :conditions => { :assistance_contract_id => @assistance_id })
    else
      session[:assistance_contract_id] = params[:assistance_id].to_i
      @assistance_id = params[:assistance_id].to_i
      @variations = Variation.where(:assistance_contract_id => params[:assistance_id].to_i)
      @variations_grid = initialize_grid(Variation,
                                         :conditions => { :assistance_contract_id => @assistance_id })
    end

  end

  # GET /variations/1
  # GET /variations/1.json
  def show
  end

  # GET /variations/new
  def new

    @variation = Variation.new
    @variation.operator = current_user.operator

    @assistance = AssistanceContract.where(:id => session[:assistance_contract_id]).first

  end

  # GET /variations/1/edit
  def edit
    @variation = Variation.find(params[:id])
    @assistance = AssistanceContract.where(:id => session[:assistance_contract_id]).first
  end

  # POST /variations
  # POST /variations.json
  def create

    @variation = Variation.new(variation_params)
    @variation.operator = current_user.operator

    respond_to do |format|
      if @variation.save
        flash[:success] = 'Variação criada com sucesso.'
        format.html { redirect_to variations_path }
      else
        @assistance = @variation.assistance_contract
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /variations/1
  # PATCH/PUT /variations/1.json
  def update
    respond_to do |format|
      if @variation.update(variation_params)
        if !@variation.approval_password.blank?
          @variation.update_attribute(:status, Variation::AUTORIZADO_SERVIDOR)
        else
          @variation.update_attribute(:status, Variation::AGUARDANDO_SERVIDOR)
        end
        flash[:success] = 'Variação atualizada com sucesso'
        format.html { redirect_to variations_path }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /variations/1
  # DELETE /variations/1.json
  def destroy
    @variation.destroy
    respond_to do |format|
      format.html { redirect_to variations_url, notice: 'Variation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_variation
      @variation = Variation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def variation_params
      params.require(:variation).permit(:value, :category, :assistance_contract_id, :approval_password)
    end
end
