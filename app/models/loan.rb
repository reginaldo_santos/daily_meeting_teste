class Loan < ActiveRecord::Base

  has_paper_trail

  has_many :negotiations
  has_many :children, :through => :negotiations

  belongs_to :consignee
  belongs_to :public_employee
  belongs_to :operator
  has_many :loan_observations, dependent: :destroy
  has_many :insertion_lines
  has_many :portability_requests

  validates :contract_value, presence: true
  validates :liquid_value, presence: true
  validates :deadline, presence: true
  validates :installment_value, presence: true
  validates :origin_operation, presence: true
  validates :approval_password, presence: true, :on => :create
  validate :validate_installment_value, :on => :create
  validate :validate_approval_password

  before_create :set_status_loan
  before_create :calculate_balance_due
  before_create :set_contract_type
  after_create :create_negotiation
  after_create :set_children_status
  after_create :set_status_portability_request
  after_create :finish_portability

  attr_accessor :approval_password, :consignee_id_portability, :purchased_loans_ids, :portability_request_id, :operation_type, :portability_balance_due


  #STATUS
  AGUARDANDO_SERVIDOR =  'Aguardando Servidor'
  AUTORIZADO_SERVIDOR = 'Autorizado servidor'
  CANCELADO = 'Cancelado'
  APROVADO = 'Aprovado Consignataria'
  REPROVADO = 'Repovado Consignataria'
  EM_FOLHA = 'Em folha'
  PORTADO = 'PORTADO'
  SUSPENSO = 'Suspenso'
  REFINANCIADO = 'Refinanciado'
  PORT_RENEG = 'Portabilidade/Renegociado'
  PAGO_SERVIDOR = 'Pago pelo servidor'
  LIQUIDADO = 'Liquidado'
  AGUARDANDO_PORTABLIDADE = 'Aguardando Portabilidade'
  PORTABILIDADE_CONFIRMADA = 'Portabilidade Confirmada'
  SERVIDOR_FALECIDO = 'Servidor Falecido'

  #TIPO_CONTRATO
  NOVO = 'Novo'
  REFIN = 'Refin'
  PORT = 'Portabilidade'

  def validate_installment_value
    if !self.installment_value.blank?
      if self.installment_value > self.public_employee.loan_margin_available
        errors.add(:installment_value, 'maior do que margem disponivel')
      end
    end
  end

  def validate_approval_password

    if !approval_password.blank?
      if !self.public_employee.validate_approval_password(approval_password)
        errors.add(:approval_password, 'incorreta')
      end
    end

  end

  def set_status_loan
    if !self.approval_password.blank? && self.public_employee.validate_approval_password(self.approval_password)
      config = ProductConfiguration.joins(:product, :configuration).where('products.consignee_id = ? and configurations.category = ?', self.operator.get_consignee.id, Configuration::LOAN).first
      if config && config.value == true
        self.status = Loan::APROVADO
      else
        self.status = Loan::AUTORIZADO_SERVIDOR
      end
    else
      self.status = Loan::AGUARDANDO_SERVIDOR
    end
  end

  def calculate_balance_due
    self.balance_due = self.deadline * self.installment_value
  end

  def set_children_status
    if !self.children.empty? && self.public_employee.validate_approval_password(self.approval_password)
      self.children.each do |child|
        if self.contract_type == Loan::REFIN
          child.update_attribute(:status, Loan::REFINANCIADO)
        else
          child.update_attribute(:status, Loan::PORTADO)
        end
      end
    end
  end

  def set_contract_type
    case self.origin_operation
      when 'NEW'
        self.contract_type = Loan::NOVO
      when 'REFIN'
        self.contract_type = Loan::REFIN
      when 'PORT'
        self.contract_type = Loan::PORT
    end
  end

  def create_negotiation
    if !self.purchased_loans_ids.blank?
      self.purchased_loans_ids.each do |id|
        negotiation = Negotiation.new
        negotiation.loan = self
        negotiation.child_id = id
        self.contract_type == Loan::PORT ? negotiation.status = Negotiation::OPEN : negotiation.status = Negotiation::CLOSED
        negotiation.negotiation_type = self.contract_type
        negotiation.save!
      end
    end
  end

  def set_status_portability_request
    if self.origin_operation == 'REFIN' && self.operation_type == 'RETAIN'
      request = PortabilityRequest.find(self.portability_request_id)
      request.update_attribute(:status, PortabilityRequest::RETIDO)
    end
    if self.origin_operation == 'PORT'
      request = PortabilityRequest.find(self.portability_request_id)
      request.update_attribute(:status, PortabilityRequest::AVERBADO)
    end
  end

  def finish_portability
    if self.contract_type == Loan::PORT
      operator = Operator.find(self.operator_id)
      loan_id = self.children.first.id
      operator_request = PortabilityRequest.where(:buyer_id => operator.get_consignee.id, :status => PortabilityRequest::AVERBADO, :loan_id => loan_id).first
      operator_request.update_attribute(:status, PortabilityRequest::FINALIZADO)
      other_requests = PortabilityRequest.where('buyer_id != ? and status not in (?) and loan_id = ?', operator.get_consignee.id, [PortabilityRequest::RETIDO, PortabilityRequest::FINALIZADO, PortabilityRequest::CANCELADO], loan_id)
      other_requests.each do |request|
        request.update_attribute(:status, PortabilityRequest::CANCELADO)
      end
    end
    if self.operation_type == 'RETAIN'
      operator = Operator.find(self.operator_id)
      loan_id = self.children.first.id
      other_requests = PortabilityRequest.where('buyer_id != ? and status not in (?) and loan_id = ?', operator.get_consignee.id, [PortabilityRequest::RETIDO, PortabilityRequest::FINALIZADO, PortabilityRequest::CANCELADO], loan_id)
      other_requests.each do |request|
        request.update_attribute(:status, PortabilityRequest::CANCELADO)
      end
    end
  end
end
