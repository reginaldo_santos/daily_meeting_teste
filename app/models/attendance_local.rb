class AttendanceLocal < ActiveRecord::Base

  has_paper_trail

  belongs_to :consignee
  has_many :operators

  validates :name, :presence => true
  validates :consignee_id, :presence => true, if: :user_is_admin?

  before_save :set_consignee_if_blank

  attr_accessor :is_admin, :user_id

  private

  def set_consignee_if_blank
    if self.consignee_id.blank?
      user = User.find(self.user_id)
      self.consignee_id = user.get_consignee.id
    end
  end

  def user_is_admin?
    user = User.find(self.user_id)
    user.isS?(User::ADMIN)
  end


end
