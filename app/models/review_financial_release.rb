class ReviewFinancialRelease < ActiveRecord::Base

  has_paper_trail

  belongs_to :financial_release_file

end
