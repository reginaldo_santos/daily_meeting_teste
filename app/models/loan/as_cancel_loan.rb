class Loan::AsCancelLoan < ActiveType::Record[Loan]

  attribute :observation, :text
  attribute :operator_id, :integer
  #attribute :password, :string

  before_save :set_cancel_status
  after_save :create_observation
  after_save :finish_portability
  after_save :rollback_purchased_contracts

  validates :observation, :presence => true
  #validates :password, :presence => true
  validate :validate_password_key

  def create_observation
    observation = LoanObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.loan = self
    observation.save!
  end

  def set_cancel_status
    self.status = Loan::CANCELADO
  end

  def validate_password_key
    if !self.approval_password.blank?
      if self.public_employee.approval_password != Encryption.encrypt(self.approval_password)
        errors.add(:approval_password, 'Senha Incorreta')
      end
    else
      errors.add(:approval_password, 'Não pode ficar em branco')
    end
  end

  def finish_portability
    if self.contract_type == Loan::PORT
      operator = Operator.find(self.operator_id)
      operator_request = PortabilityRequest.where(:buyer_id => operator.get_consignee.id, :status => PortabilityRequest::FINALIZADO, :loan_id => self.children.take.id).take
      operator_request.update_attribute(:status, PortabilityRequest::CANCELADO)
    else
      child = self.children.first if !self.children.blank?
      request = child.portability_requests.where(:status => PortabilityRequest::RETIDO).first if child
      if request
        request.update_attribute(:status, PortabilityRequest::CANCELADO)
      end
    end
  end

  def rollback_purchased_contracts
    if self.contract_type != Loan::NOVO
      self.children.each do |child|
        child = child.previous_version
        child.save!
      end
    end
  end

end