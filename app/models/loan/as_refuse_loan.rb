class Loan::AsRefuseLoan < ActiveType::Record[Loan]

  attribute :observation, :string
  attribute :operator_id, :integer

  validates :observation, :presence => true

  before_save :set_status
  after_save :create_observation
  after_save :rollback_purchased_contracts
  after_save :finish_portability

  def set_status
    self.status = Loan::REPROVADO
  end

  def create_observation
    observation = LoanObservation.new
    observation.text = self.observation
    observation.operator_id = operator_id
    observation.loan = self
    observation.save!
  end

  def rollback_purchased_contracts
    if self.contract_type != Loan::NOVO
      self.children.each do |child|
        child = child.previous_version
        child.save!
      end
    end
  end

  def finish_portability
    if self.contract_type == Loan::PORT
      operator = Operator.find(self.operator_id)
      operator_request = PortabilityRequest.where(:buyer_id => operator.get_consignee.id, :status => PortabilityRequest::FINALIZADO, :loan_id => self.children.first.id).first
      operator_request.update_attribute(:status, PortabilityRequest::SEM_CONTRATO)
    else
      child = self.children.first if !self.children.blank?
      request = child.portability_requests.where(:status => PortabilityRequest::RETIDO).first if child
      if request
        request.update_attribute(:status, PortabilityRequest::SEM_CONTRATO)
      end
    end
  end

end