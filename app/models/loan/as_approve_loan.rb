class Loan::AsApproveLoan < ActiveType::Record[Loan]

  attribute :observation, :text
  attribute :operator_id, :integer

  before_save :set_approved_status
  after_save :create_observation if :validate_observation

  validates :contract_number, :presence => true
  validates :interest_tax, :presence => true
  validates :cet, :presence => true
  validates :others, :presence => true

  def create_observation
    if !self.observation.blank?
      observation = LoanObservation.new
      observation.text = self.observation
      observation.operator_id = self.operator_id
      observation.loan = self
      observation.save!
    end
  end

  def set_approved_status
    self.status = Loan::APROVADO
  end

  def validate_observation
    self.observation.blank? ? false : true
  end

end