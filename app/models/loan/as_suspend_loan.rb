class Loan::AsSuspendLoan < ActiveType::Record[Loan]

  attribute :observation, :text
  attribute :operator_id, :integer

  validates :observation, :presence => true

  before_save :set_status
  after_save :create_observation

  def set_status
    self.status = Loan::SUSPENSO
  end

  def create_observation
    observation = LoanObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.loan = self
    observation.save!
  end
end