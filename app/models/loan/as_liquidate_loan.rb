class Loan::AsLiquidateLoan < ActiveType::Record[Loan]

  attribute :observation, :string
  attribute :operator_id, :integer

  validates :observation, :presence => true

  before_save :set_status
  before_save :liquidate_installments
  after_save :create_observation

  def set_status
    self.status = Loan::LIQUIDADO
  end

  def liquidate_installments
    self.installments_paid = self.deadline
  end

  def create_observation
    observation = LoanObservation.new
    observation.text = self.observation
    observation.operator_id = operator_id
    observation.loan = self
    observation.save!
  end

end