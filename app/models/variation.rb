class Variation < ActiveRecord::Base

  has_paper_trail

  belongs_to :assistance_contract
  belongs_to :operator

  has_many :insertion_lines

  attr_accessor :approval_password

  validates :value, presence: true
  validates :category, presence: true
  validates :approval_password, presence: true, :on => :create
  validate :validate_discount_value
  validate :validate_approval_password

  after_create :set_status_variation

  #STATUS
  AGUARDANDO_SERVIDOR =  'Aguardando Servidor'
  AUTORIZADO_SERVIDOR = 'Autorizado servidor'
  CANCELADO = 'Cancelado'
  APROVADO = 'Aprovado'
  EM_FOLHA = 'Em folha'
  SUSPENSO = 'Suspenso'
  ENCERRADO = 'Encerrado'
  SERVIDOR_FALECIDO = 'Servidor Falecido'

  def validate_discount_value

    if self.value
      if self.value > self.assistance_contract.public_employee.assistance_margin_available
        errors.add(:value, 'maior do que margem disponivel')
      end
    end

  end

  def validate_approval_password

    if !self.approval_password.blank?
      if !assistance_contract.public_employee.validate_approval_password(approval_password)
        errors.add(:approval_password, 'incorreta')
      end
    end

  end

  def set_status_variation
    if self.approval_password.blank?
      self.update(:status => Variation::AGUARDANDO_SERVIDOR)
    else
      self.update(:status => Variation::AUTORIZADO_SERVIDOR)
    end
  end

end
