require 'fileutils'

class IoAccess < ActiveRecord::Base

	has_paper_trail

	IMPORT_MARGIN = 'IMPORT_MARGIN'
	IMPORT_RETURN = 'IMPORT_RETURN'
	IMPORT_CARD_RELEASE = 'IMPORT_CARD_RELEASE'
	EXPORT_INSERTION = 'EXPORT_INSERTION'

	def import_margin(encoding, original_name)
		employees_list= []
		line_index = 1

		file_path = Rails.public_path.join(original_name)

		organs = Organ.select(:id, :code)
		situations = Situation.select(:id, :file_reference)
		marital_status = MaritalStatus.ids
    	literacy = Literacy.ids
    	suspension_age = SuspensionAge.select(:id, :file_reference)
    	commissioned = Commissioned.ids

    	lines = File.readlines(file_path)

		begin
			ActiveRecord::Base.transaction do
				lines.each_slice(100) do |batch|
    				batch.each do |line|
    					line.force_encoding(encoding).encode!
              if !line.blank?
                employee = fill_public_employee(line, organs, situations, marital_status, literacy, suspension_age, commissioned, @file_name, line_index)
                exist_employee = PublicEmployee.where(:organ_id => employee.organ_id, :registration => employee.registration).first
                if exist_employee
                  update_public_employee(exist_employee, employee)
                else
                  employee.save!
                end
              end

    					line_index += 1
					end
				end
				update_not_found_employees
			end
			IoAccess.where(:category => IoAccess::IMPORT_MARGIN).order(created_at: :desc).first.update_attributes(:finished => true, :successful => true)
      		FileUtils.rm file_path
    rescue => ex
      puts ex.message
			IoAccess.where(:category => IoAccess::IMPORT_MARGIN).order(created_at: :desc).first.update_attributes(:finished => true)
      		text = ex.message.gsub('A validação falhou: ', '')
      		MarginCriticism.create(text: "Linha #{line_index}: #{text}", file_name: original_name)
      		FileUtils.rm file_path
		end
	end

	def export_insertion(month, year)
		begin
			insertion_consolidated_property = Property.where(:name => Property::INSERTION_FILE_CONSOLIDATED).first

    		if insertion_consolidated_property.value.to_i == 1
      			generate_consolidated_insertion_file
    		else
      			generate_detailed_insertion_file(month, year)
    		end
    		IoAccess.where(:category => IoAccess::EXPORT_INSERTION).order(created_at: :desc).first.update_attributes(:finished => true, :successful => true)
		rescue => ex
      Error.create(:message => ex.message)
			IoAccess.where(:category => IoAccess::EXPORT_INSERTION).order(created_at: :desc).first.update_attributes(:finished => true)
		end
	end

	def import_return(encoding, file_name)
		critics_messages = []
		begin
			line_index = 1

			file_path = Rails.public_path.join(file_name)
			lines = File.readlines(file_path)

			ActiveRecord::Base.transaction do
				lines.each_slice(100) do |batch|
					batch.each do |line|
						line.force_encoding(encoding).encode!
						averbation_code = line[34..35]
            insertion_code = line[110..114]

            			if return_line_valid?(averbation_code, insertion_code, critics_messages, line_index)
            				case averbation_code
               					when '00'
                  					averbar_contrato(insertion_code, averbation_code)
                				when '07', '51', '53', '54', '56', '57'
                  					insertion_line = InsertionLine.find_by(:line_code => insertion_code)
                  					insertion_line.averbation.code = averbation_code
                  					insertion_line.line_code = nil
                  					insertion_line.save!
                				when '52'
                  					dead_employee(insertion_code, averbation_code)
              				end
        				end
        				line_index += 1
					end
				end
			end
			critics_messages.each do |message|
        		critic = ReturnFileCritic.new(:file_name => file_name, :observation => message)
        		critic.save!
      		end

      		FileUtils.rm file_path

      		if critics_messages.empty?
      			IoAccess.where(:category => IoAccess::IMPORT_RETURN).order(created_at: :desc).first.update_attributes(:finished => true, :successful => true)
  			else
  				IoAccess.where(:category => IoAccess::IMPORT_RETURN).order(created_at: :desc).first.update_attributes(:finished => true)
			end
		rescue => ex
			IoAccess.where(:category => IoAccess::IMPORT_RETURN).order(created_at: :desc).first.update_attributes(:finished => true)
      		FileUtils.rm file_path
		end
	end

	def import_card_release(file_name, user_id, encoding)

		messages = []

		begin
			line_index = 1

			file_path = Rails.public_path.join(file_name)
			user = User.find(user_id)
			ActiveRecord::Base.transaction do
				
				financial_file = FinancialReleaseFile.new
				financial_file.name = file_name
				financial_file.save!

        lines = File.readlines(file_path)

        lines.each_slice(100) do |batch|
          batch.each do |line|
            
            line.force_encoding(encoding).encode!
            line = line.squish

            row_will_fail(line, user, financial_file.id, messages, line_index)

            

              line_data = line.split(';')

              employee = PublicEmployee.where(:cpf => line_data[1], :registration => line_data[2]).first

              card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee).first
              
              release = FinancialRelease.new
              release.card = card
              release.value = BigDecimal.new(line_data[3].gsub(',', '.'))
              release.status = FinancialRelease::AUTORIZADO_CONSIGNATARIA
              release.operator = user.operator
              release.financial_release_file = financial_file

              release.save!
          

            line_index += 1
          end
        end
      end

      FileUtils.rm file_path

      
      IoAccess.where(:category => IoAccess::IMPORT_CARD_RELEASE).order(created_at: :desc).first.update_attributes(:finished => true, :successful => true)
      
		rescue => ex

      ReviewFinancialRelease.create(:error => ex.message)
			IoAccess.where(:category => IoAccess::IMPORT_CARD_RELEASE).order(created_at: :desc).first.update_attributes(:finished => true)
			FileUtils.rm file_path
		end
	end

	private

	#========== MARGIN ==========

	def fill_public_employee(line, organs, situations, marital_status, literacy, suspension_age, commissioned, file_name, line_index)

	    employee = PublicEmployee::AsUploadMargin.new

	    employee.line_index = line_index
	    employee.file_name = file_name

	    employee.name = line[0, 30]
	    employee.rg = line[30, 13]
	    employee.dispatcher_organ = line[43, 4]
	    employee.dispatcher_uf = line[47, 02]
	    employee.cpf = line[49, 11]
	    employee.organ = organs.where(:code => line[60, 3].to_i.to_s).first
      employee.registration = line[63, 8]
      employee.role = line[71, 15]
      employee.admission_date = Date.parse(line[90, 4]+'-'+line[88, 2]+'-'+line[86, 2]) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[86..93]
      employee.situation = situations.where(:file_reference => line[94, 2]).first
      employee.end_contract_date = Date.new(line[100, 4].to_i, line[98, 2].to_i, line[96, 2].to_i) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[96..103]
      employee.street = line[104, 32]
      employee.number = line[136, 5]
      employee.complement = line[141, 20]
      employee.neighborhood = line[161, 20]
      employee.city = line[181, 15]
      employee.zip_code = line[196, 8]
      employee.phone = line[204, 8].rjust(10, '0') if !line[204, 8].blank?
      employee.birth_date = Date.new(line[216, 4].to_i, line[214, 2].to_i, line[212, 2].to_i) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[212..219]
      employee.marital_status_id = line[220, 1] if marital_status.include?(line[220, 1].to_i)
      employee.voter_registration = line[221, 12]
      employee.zone = line[233, 3]
      employee.section = line[236, 4]
      employee.gender = line[240, 1]
      employee.literacy_id = line[241, 1] if literacy.include?(line[241, 1].to_i)
      employee.margin_value = BigDecimal.new(line[242, 11])/ BigDecimal.new(100) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[242, 11]
      employee.suspension_age = suspension_age.where(:file_reference => line[253, 1]).first
      employee.commissioned_id = line[254, 1].to_i if commissioned.include?(line[254, 1].to_i)

	    return employee
  	end

  	def update_not_found_employees

	    # se o arquivo for importado mais de uma vez no mesmo dia, nao zerara a margem dos servidores nao encontrados,
	    # entretanto, se for importado arquivos em dias diferentes os servidores nao encontrados terao sua margem zerada.

	    not_found_employees = PublicEmployee.where.not(:updated_at => Date.today.beginning_of_day .. Date.today.end_of_day)

	      not_found_employees.each do |employee|
	        attributes = {margin_value: BigDecimal.new('0')}
	        employee.update(attributes)
	      end
  	end	

  	def update_public_employee(employee, modification)
      attributes = {name: modification.name, rg: modification.rg, dispatcher_organ: modification.dispatcher_organ,
                    dispatcher_uf: modification.dispatcher_uf, cpf: modification.cpf, organ_id: modification.organ_id,
                    registration: modification.registration, role: modification.role, admission_date: modification.admission_date,
                    situation_id: modification.situation_id, end_contract_date: modification.end_contract_date, street: modification.street,
                    number: modification.number, complement: modification.complement, neighborhood: modification.neighborhood,
                    city: modification.city, zip_code: modification.zip_code, phone: modification.phone,
                    birth_date: modification.birth_date, marital_status_id: modification.marital_status_id, voter_registration: modification.voter_registration,
                    zone: modification.zone, section: modification.section, gender: modification.gender,
                    literacy_id: modification.literacy_id, margin_value: modification.margin_value, suspension_age_id: modification.suspension_age_id,
                    commissioned_id: modification.commissioned_id}

      employee.assign_attributes(attributes)
      employee.save!
      employee.touch
  	end

  	#========== INSERTION ==========

  	def generate_detailed_insertion_file(month, year)

      ActiveRecord::Base.transaction do
        cut_day = Property.where(:name => Property::CUT_DAY).first.value.to_i

        month = month.to_i
        year = year.to_i

        mon_str = month.to_s.rjust(2, '0')

        cut_date = DateTime.new(year, month, cut_day).end_of_day

        if Rails.env.staging?
          file_name = "Sape_staging_#{month}_#{year}_detalhado.txt"
          path = "/opt/mf/tmp"
        elsif Rails.env.development?
          file_name = "Sape_development_#{month}_#{year}_detalhado.txt"
          path = "/home/reginaldo/Área de Trabalho/file/#{file_name}"
        elsif Raisl.env.test?
          file_name = "Sape_Mearim_#{month}_#{year}_detalhado.txt"
          path = "/opt/mf/tmp"
        end

        insertion = Insertion.new
        insertion.month = month
        insertion.year = year
        insertion.file_name = file_name
        insertion.file_path = path
        insertion.save!


        employees = PublicEmployee.includes(:loans, :assistance_contracts, :cards, :organ).all
        priority_properties = Property.where(:category => Property::INSERTION_PRIORITY).order('value ASC')
        partial_discount_value = Property.where(:category => Property::PARTIAL_DISCOUNT).first.value.to_i
        partial_discount_activated = 1

        File.open(path, 'w+') do |f|

          employees.each do |employee|
            margin_in_file = 0
            employee_margin = employee.margin_value
            organ = employee.organ.code.rjust(3, '0')
            registration = employee.registration.rjust(8, '0')
            blank_field = '%-2.2s' % nil
            uso_emp = '%-29.29s' % nil
            final_fill = '0'.rjust(8, '.')
            if employee.cpf
              cpf = employee.cpf
            else
              cpf = '%-11.11s' % nil
            end
            priority_properties.each do |priority_property|

              ActiveRecord::Base.transaction do
                name = priority_property.name

                case name
                  when Property::PRIORITY_ASSISTANCE
                    product_type = ProductType.where(:name => ProductType::ASSISTANCE).take
                    get_assistance_contract(employee, cut_date).each do |assistance|

                      have_margin = validate_margin_employee(employee_margin, margin_in_file, assistance.discount_value)
                      do_partial_discount = (!have_margin && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                      if have_margin || do_partial_discount

                        product = Product.joins(:product_type, :consignee).where('product_types.id = ? and consignees.id = ?', product_type.id, assistance.consignee.id).take
                        event_code = product.event_code.to_s.rjust(6, '0')
                        discount_value = (assistance.discount_value * 100).to_i.to_s.rjust(11, '0') if have_margin
                        discount_value = ((assistance.discount_value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount

                        if assistance.contract_number
                          contract_number = assistance.contract_number.rjust(18, '0')
                        else
                          contract_number = '%-18.18s' % nil
                        end

                        if assistance.paid_installments
                          installment_to_pay = (assistance.paid_installments + 1).to_s.rjust(3, '0')
                        else
                          installment_to_pay = 1.to_s.rjust(3, '0')
                        end
                        deadline = 999
                        segment = '02' #segmento assistencial
                        id_consignee = assistance.consignee.id

                        assistance_insertion_code = StringGenerator.generate_unique_code_to_insertion
                        assistance_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                      :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                      :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                      :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                      :final_fill => final_fill, :insertion_type => InsertionLine::ASSISTANCE,
                                                                      :assistance_contract_id => assistance.id, :line_code => assistance_insertion_code,
                                                                      :partial_discount => do_partial_discount, :insertion_id => insertion.id, :consignee_id => id_consignee)
                        assistance_insertion_line.save!

                        assistance_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{assistance_insertion_code}"
                        f.puts(assistance_line)
                        margin_in_file += assistance.discount_value
                        assistance.variations.each do |variation|

                          have_margin_variation = validate_margin_employee(employee_margin, margin_in_file, variation.value)
                          do_partial_discount_variation = (!have_margin_variation && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                          if have_margin_variation || do_partial_discount_variation

                            #TODO falta buscar codigo do evento
                            event_code = product.event_code.to_s.rjust(6, '0')
                            discount_value = (variation.value * 100).to_i.to_s.rjust(11, '0') if have_margin_variation
                            discount_value = ((variation.value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount_variation

                            contract_number = '%-6.6s' % nil

                            if variation.paid_installments
                              installment_to_pay = (variation.paid_installments + 1).to_s.rjust(3, '0')
                            else
                              installment_to_pay = 1.to_s.rjust(3, '0')
                            end
                            deadline = 999
                            segment = '02' #segmento assistencial( ja que nao existe segmento de variação)
                            consignee_id = assistance.consignee.id
                            variation_insertion_code = StringGenerator.generate_unique_code_to_insertion
                            variation_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                          :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                          :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                          :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                          :final_fill => final_fill, :insertion_type => InsertionLine::VARIATION,
                                                                          :variation_id => variation.id, :line_code => variation_insertion_code,
                                                                          :partial_discount => do_partial_discount_variation, :insertion_id => insertion.id, :consignee_id => consignee_id)
                            variation_insertion_line.save!
                            variation_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{variation_insertion_code}"

                            f.puts(variation_line)
                            margin_in_file += variation.value
                          end
                        end
                      end
                    end
                  when Property::PRIORITY_LOAN
                    product_type = ProductType.where(:name => ProductType::LOAN).take
                    get_loan(employee, cut_date).each do |loan|
                      have_margin = validate_margin_employee(employee_margin, margin_in_file, loan.installment_value)
                      do_partial_discount = (!have_margin && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                      if have_margin || do_partial_discount

                        product = Product.joins(:consignee, :product_type).where('consignees.id = ? and product_types.id = ?', loan.consignee.id, product_type.id).take

                        event_code = product.event_code.to_s.rjust(6, '0')

                        discount_value = (loan.installment_value * 100).to_i.to_s.rjust(11, '0') if have_margin
                        discount_value = ((loan.installment_value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount

                        if loan.contract_number
                          contract_number = loan.contract_number.rjust(18, '0')
                        else
                          contract_number = '%-18.18s' % nil
                        end

                        if loan.installments_paid
                          installment_to_pay = (loan.installments_paid + 1).to_s.rjust(3, '0')
                        else
                          installment_to_pay = 1.to_s.rjust(3, '0')
                        end
                        deadline = loan.deadline.to_s.rjust(3, '0')
                        segment = '01' #segmento financeiro
                        consignee_id = loan.consignee.id
                        loan_insertion_code = StringGenerator.generate_unique_code_to_insertion
                        loan_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                      :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                      :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                      :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                      :final_fill => final_fill, :insertion_type => InsertionLine::LOAN,
                                                                      :loan_id => loan.id, :line_code => loan_insertion_code,
                                                                      :partial_discount => do_partial_discount, :insertion_id => insertion.id, :consignee_id => consignee_id)
                        loan_insertion_line.save!
                        loan_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{loan_insertion_code}"

                        f.puts(loan_line)
                        margin_in_file += loan.installment_value
                      end
                    end
                  when Property::PRIORITY_CARD
                    product_type = ProductType.where(:name => ProductType::CARD).take
                    get_card(employee, cut_date).each do |card|
                      get_financial_releases(card, cut_date).each do |release|

                        have_margin = validate_margin_employee(employee_margin, margin_in_file, release.value)
                        do_partial_discount = (!have_margin && (employee_margin - margin_in_file > BigDecimal.new('0.0'))) && partial_discount_value == partial_discount_activated

                        if have_margin || do_partial_discount

                          product = Product.joins(:product_types, :consignees).where('consignees.id = ? and product_types.id = ?', card.consignee.id, product_type.id)
                          event_code = product.event_code.to_s.rjust(3, '0')

                          discount_value = (release.value * 100).to_i.to_s.rjust(11, '0') if have_margin
                          discount_value = ((release.value - (employee_margin - margin_in_file)) * 100).to_i.to_s.rjust(11, '0') if do_partial_discount

                          contract_number = '%-6.6s' % nil

                          installment_to_pay = 0.to_s.rjust(3, '0')

                          deadline = 0.to_s.rjust(3, '0')
                          segment = '03' #segmento financeiro
                          consignee_id = card.consignee.id
                          release_insertion_code = StringGenerator.generate_unique_code_to_insertion
                          release_insertion_line = InsertionLine.new(:year => year, :month => mon_str, :organ_code => organ, :registration => registration,
                                                                        :event_code => event_code, :discount_value => discount_value, :blank_field => blank_field,
                                                                        :cpf => cpf, :contract_number => contract_number, :uso_emp => uso_emp,
                                                                        :installment_to_pay => installment_to_pay, :deadline => deadline, :segment => segment,
                                                                        :final_fill => final_fill, :insertion_type => InsertionLine::FINANCIAL_RELEASE,
                                                                        :financial_release_id => release.id, :line_code => release_insertion_code,
                                                                        :partial_discount => do_partial_discount, :insertion_id => insertion.id, :consignee_id => consignee_id)
                          release_insertion_line.save!
                          release_line = "#{year}#{mon_str}#{organ}#{registration}#{event_code}#{discount_value}#{blank_field}#{cpf}#{contract_number}#{uso_emp}#{installment_to_pay}#{deadline}#{segment}#{final_fill}#{release_insertion_code}"

                          f.puts(release_line)
                          margin_in_file += release.value
                        end

                      end
                      #end get_financial_releases
                    end
                    #end get_cards
                end
                #end case card
              end

            end
          end

        end
      end
      # end transaction
    end

	def validate_margin_employee(margin, margin_in_file, discount_value)
      if (margin - margin_in_file) >= discount_value
        return true
      else
        return false
      end
    end

    def get_assistance_contract(employee, cut_date)
      assistance_list = employee.assistance_contracts.includes(:consignee, :variations).where('created_at <= ? and status in (?)', cut_date, [AssistanceContract::APROVADO, AssistanceContract::EM_FOLHA]).order('created_at ASC')
      return assistance_list
    end

    def get_loan(employee, cut_date)
      loan_list = employee.loans.where('created_at <= ? and status in (?)', cut_date, [Loan::APROVADO, Loan::EM_FOLHA]).order('created_at ASC')
      return loan_list
    end

    def get_card(employee, cut_date)
      card_list = employee.cards.where('created_at <= ? and status in (?)', cut_date, [Card::AUTORIZADO_CONSIGNATARIA]).order('created_at ASC')
      return card_list
    end

    def get_financial_releases(card, cut_date)
      releases = card.financial_releases.where('created_at <= ? and status in (?)', cut_date, [FinancialRelease::AUTORIZADO_CONSIGNATARIA]).order('created_at ASC')
      return releases
    end

    #========== RETURN ==========

    def dead_employee(insertion_code, averbation_code)
    insertion_line = InsertionLine.find_by(:line_code => insertion_code)
    case insertion_line.insertion_type
      when InsertionLine::LOAN
        insertion_line.loan.update_attribute(:status, Loan::SERVIDOR_FALECIDO)
      when InsertionLine::ASSISTANCE
        insertion_line.assistance_contract.update_attribute(:status, AssistanceContract::SERVIDOR_FALECIDO)
      when InsertionLine::VARIATION
        insertion_line.variation.update_attribute(:status, Variation::SERVIDOR_FALECIDO)
      when InsertionLine::FINANCIAL_RELEASE
        insertion.financial_release.update_attribute(:status, FinancialRelease::SERVIDOR_FALECIDO)
    end
    insertion_line.line_code = nil
    insertion_line.averbation_code = averbation_code
    insertion_line.save!
  end

  def averbar_contrato(insertion_code, averbation_code)
    insertion_line = InsertionLine.includes(:loan, :assistance_contract, :variation, :financial_release).where(:line_code => insertion_code).first
    case insertion_line.insertion_type
      when InsertionLine::LOAN
        loan = insertion_line.loan
        loan.installments_paid += 1
        if loan.installments_paid == loan.deadline
          loan.status = Loan::PAGO_SERVIDOR
        end
        if loan.installments_paid == 1
        	loan.status = Loan::EM_FOLHA
    	end
        loan.save!
      when InsertionLine::ASSISTANCE
        assistance = insertion_line.assistance_contract
        assistance.paid_installments += 1
        assistance.save!
      when InsertionLine::VARIATION
        variation = insertion_line.variation
        variation.paid_installments += 1
        variation.save!
      when InsertionLine::FINANCIAL_RELEASE
        release = insertion.financial_release
        release.status = FinancialRelease::PAGO
        release.save!
    end
    insertion_line.line_code = nil
    insertion_line.averbation_code = averbation_code
    insertion_line.save!
  end

  def return_line_valid?(averbation_code, insertion_code, critics_messages, line_counter)

    if averbation_code.blank?
      critics_messages << "Linha #{line_counter}: Código de averbação não informado"
    else
      return_code = ReturnCode.find_by(:code => averbation_code)
      if !return_code
        critics_messages << "Linha #{line_counter}: Código de averbação inexistente"
      end
    end

    if insertion_code.blank?
      critics_messages << "Linha #{line_counter}: Código de inserção não informado"
    else
      code = InsertionLine.find_by(:line_code => insertion_code)
      if !code
        critics_messages << "Linha #{line_counter}: Código de inserção inexistente"
      end
    end

    critics_messages.empty? ? true : false
  end

  #========== CARD RELEASE ==========
  def row_will_fail(row, user, financial_release_file_id, messages, line_index)

      row_data = row.split(';')

      #nome, cpf, matricula e valor
      cpf_exist = !row_data[1].blank?
      registration_exist = !row_data[2].blank?
      value_exist = !row_data[3].blank?
      employee = nil
      value_valid = false
      release_duplicate = false
      release_off_limit = false
      employee_valid = false
      card_valid = false
      fail = false

      if cpf_exist && registration_exist
        employee = PublicEmployee.where(:cpf => row_data[1].gsub('.', '').gsub('-', ''), :registration => row_data[2]).first
      end

      if employee
        employee_valid = employee_valid?(employee, user.get_consignee.id)
        if !employee_valid
          raise "#{line_index}: O servidor não possui cartão válido para a sua consignataria"
        end
      else
        raise "#{line_index}: Servidor não encontrado."
      end

      if value_exist
        value_valid = discount_value_valid?(row_data[3])
        if !value_valid
          raise "#{line_index}: Valor inválido"
        end
      else
        raise "#{line_index}: Por favor informe o valor a ser descontado."
      end

      if employee && employee_valid
        card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee, :status => Card::AUTORIZADO_CONSIGNATARIA).first
        if card
        	card_valid = true
    	  else
    		  raise "#{line_index}: Cartão nao encontrado."
    	  end
        release_duplicate = release_duplicate?(card.id)
        if release_duplicate
          raise "#{line_index}: Desconto duplicado"
        end
      end

      if (value_exist && value_valid) && (employee && employee_valid)

        card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee, :status => Card::AUTORIZADO_CONSIGNATARIA).first
        release_off_limit = release_off_limit?(BigDecimal.new(row_data[3]), card.discount_value)

        if release_off_limit
          raise "#{line_index}: Desconto maior que o limite."
        end
      end
    end

    def create_review_hash(row, error, financial_release_file_id, line_index)

      row_data = row.split(';')

    	review_hash = Hash.new
    	review_hash[:error] = error
    	review_hash[:value] = row_data[3]
    	review_hash[:financial_release_file_id] = financial_release_file_id
    	review_hash[:line] = line_index

      return review_hash
    end

    def discount_value_valid?(discount_value)

      regex = /^\d+\,\d\d$/ #um ou mais digitos seguidos de uma virgula seguido de dois digitos

      if regex.match(discount_value).nil?
        return false
      else
        return true
      end

    end

    def employee_valid?(employee, consignee_id)
      card = Card.where(:public_employee_id => employee.id, :consignee_id => consignee_id, :status => Card::AUTORIZADO_CONSIGNATARIA).first

      if card
        return true
      else
        return false
      end
    end

    def release_duplicate?(card_id)

      release = FinancialRelease.where('extract(month from created_at) = ? and status = ? and card_id = ?', Date.today.month, Card::AUTORIZADO_CONSIGNATARIA, card_id).first

      if release
        return true
      else
        return false
      end

    end

    def release_off_limit?(discount_value, card_value)

      if discount_value > card_value
        return true
      else
        return false
      end

    end
end
