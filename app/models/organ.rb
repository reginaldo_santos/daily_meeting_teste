class Organ < ActiveRecord::Base

  has_paper_trail

  validates :code, :presence => true, :uniqueness => true
  validates :name, :presence => true, :uniqueness => true
  validates :cnpj, :uniqueness => true, allow_blank: true

  has_many :public_employees

end
