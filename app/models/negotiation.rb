class Negotiation < ActiveRecord::Base

  belongs_to :loan
  belongs_to :child, :class_name => 'Loan'


  validates :loan_id, presence: true
  validates :child_id, presence: true
  validates :status, presence: true

  OPEN = 'OPEN'
  CLOSED = 'CLOSED'

end
