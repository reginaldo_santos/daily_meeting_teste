class Situation < ActiveRecord::Base

  has_paper_trail

  has_many :public_employees

  validates :name, presence: true
  validates :file_reference, presence: true
end
