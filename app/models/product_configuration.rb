class ProductConfiguration < ActiveRecord::Base

  has_paper_trail

  belongs_to :product
  belongs_to :configuration
end
