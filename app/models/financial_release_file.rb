class FinancialReleaseFile < ActiveRecord::Base

  has_paper_trail

  has_many :financial_releases
  has_many :review_financial_releases

end
