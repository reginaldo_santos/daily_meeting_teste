class AssistanceObservation < ActiveRecord::Base

  has_paper_trail

  belongs_to :operator
  belongs_to :assistance_contract
end
