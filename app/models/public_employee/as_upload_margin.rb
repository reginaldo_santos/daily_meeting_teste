class PublicEmployee::AsUploadMargin < ActiveType::Record[PublicEmployee]

  NUMBER_REGEX = /\A[-+]?\d+\z/

  attribute :line_index, :integer
  attribute :file_name, :string

  validate :name_valid?
  validate :cpf_valid?
  validate :organ_valid?
  validate :registration_valid?

  validate :admission_date_valid?
  validate :end_contract_date_valid?

  validate :situation_valid?
  validate :commissioned_valid?
  validate :margin_value_valid?

  private

    def cpf_valid?
      if self.cpf.blank?
        message = "CPF deve ser informado."
        errors.add(:cpf, message)
      else
        if !NUMBER_REGEX === self.cpf
          message = "CPF inválido."
          errors.add(:cpf, message)
        end
      end
    end

    def organ_valid?
      if self.organ_id.blank?
        message = "Órgão válido deve ser informado."
        errors.add(:organ_id, message)
      end
    end

    def situation_valid?
      if self.situation_id.blank?
        message = "Situação válida deve ser informada."
        errors.add(:situation_id, message)
      end
    end

    def commissioned_valid?
      if self.commissioned_id.blank?
        message = "Tipo de contratação válida deve ser informada."
        errors.add(:commissioned_id, message)
      end
    end

    def margin_value_valid?
      if self.margin_value.blank?
        message = "Margem informada deve ser válida."
        errors.add(:margin_value, message)
      end
    end

    def admission_date_valid?
      if self.admission_date.blank?
        message = "Data de admissão válida deve ser informada."
        errors.add(:admission_date, message)
      end
    end

    def end_contract_date_valid?
      if self.end_contract_date.blank?
        message = "Data de fim do contrato válida deve ser informada."
        errors.add(:end_contract_date, message)
      end
    end

    def name_valid?
      if self.name.blank?
        message = "Nome do servidor deve ser informado."
        errors.add(:name, message)
      end
    end

    def registration_valid?
      if self.registration.blank?
        message = "Nome do servidor deve ser informado."
        errors.add(:registration, message)
      end
    end

end