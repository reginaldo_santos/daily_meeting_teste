class PublicEmployee::AsTransferEmployee < ActiveType::Record[PublicEmployee]

  validates :registration, :presence => true
  validates :organ_id, :presence => true
  validate :check_registration_on_organ

  private

  def check_registration_on_organ
    employee = PublicEmployee.where(:organ_id => self.organ_id, :registration => self.registration).take
    if employee
      errors.add(:registration, 'Já está em uso')
    end
  end
end