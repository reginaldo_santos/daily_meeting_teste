class PublicEmployee::AsUpdateContacts < ActiveType::Record[PublicEmployee]

  validates :phone, presence: true

end