class ProductType < ActiveRecord::Base

  has_paper_trail

  has_many :products

  #TYPES
  LOAN = 'Empréstimo'
  ASSISTANCE = 'Assistencial'
  CARD = 'Cartão Consignado'

end
