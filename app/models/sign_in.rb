class SignIn < ActiveType::Object

  attribute :username, :string
  attribute :password, :string

  validate :validate_user_exists
  validate :validate_password_correct

  after_save :reset_login_attempt_counter

  def user
    User.find_by_login(username) if username.present?
  end

  def user_blocked?
    user_local = user
    user_local.blocked if user_local
  end

  private

    def validate_user_exists
      if user.blank?
        errors.add(:username, 'Usuário não encontrado.')
      end
    end

    def validate_password_correct
      if user && !user.authenticate(password)
        errors.add(:password, 'Senha inválida.')
      end
    end

    def reset_login_attempt_counter
      user.update_attribute(:login_attempts, 0)
    end

end