class PortabilityRequest < ActiveRecord::Base

  has_paper_trail

  belongs_to :loan
  belongs_to :buyer, :class_name => 'Consignee'
  belongs_to :seller, :class_name => 'Consignee'

  validates :buyer_id, presence: true
  validates :seller_id, presence: true
  validates :loan_id, presence: true
  validates :status, presence: true

  OPEN = 'Aberto'
  CLOSED = 'Fechado'

  AGUARDANDO_RESPOSTA = 'Aguardando Resposta'
  CANCELADO = 'Cancelado'
  RESPONDIDO = 'Respondido'
  RETIDO = 'Retido'
  AVERBADO = 'Averbado'
  FINALIZADO = 'Finalizado'
  SEM_CONTRATO = 'Finalizado sem contrato.'

end
