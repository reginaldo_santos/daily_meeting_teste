class Property < ActiveRecord::Base

  has_paper_trail

  IN_GROUP = 'PORCENTAGEM_GRUPO'
  INDIVIDUAL = 'PORCENTAGEM_INDIVIDUAL'
  LOAN = 'EMPRESTIMO'
  ASSISTANCE = 'ASSISTENCIAL'
  CARD = 'CARTAO'
  COMERCIAL = 'COMERCIAL'
  PRODUCT_GROUP = 'GRUPO_PRODUTO'
  PREFECTURE_NAME = 'NOME_CONVENIO'
  MANDATORY_PASSWORD_CHANGE = 'Mudança de senha obrigatória.'
  CUT_DAY = 'Dia de corte'
  INSERTION_FILE_CONSOLIDATED = 'Arquivo de inserção consolidado'
  INSERTION_FILE_DETAILED =  'Arquivo de inserção detalhado'

  PRIORITY_LOAN = 'Prioridade do empréstimo'
  PRIORITY_CARD = 'Prioridade do cartão'
  PRIORITY_ASSISTANCE = 'Prioridade do contrato assistencial'

  #CATEGORIES
  INSERTION_PRIORITY = 'INSERTION_PRIORITY'
  PARTIAL_DISCOUNT = 'PARTIAL_DISCOUNT'
  LOAN_LIMIT = 'LOAN_LIMIT'
  ASSISTANCE_LIMIT = 'ASSISTANCE_LIMIT'
  CARD_LIMIT = 'CARD_LIMIT'

  validates :value, :presence => true
  validates :observation, presence: true


end
