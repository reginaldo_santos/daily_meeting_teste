class FinancialRelease < ActiveRecord::Base

  has_paper_trail

  belongs_to :card
  belongs_to :operator
  belongs_to :financial_release_file

  has_many :insertion_lines

  validates :card_id, presence: true
  validates :value, presence: true

  CANCELADO = 'Cancelado'
  PAGO = 'Pago'
  AUTORIZADO_CONSIGNATARIA = 'Autorizado consignataria'
  SERVIDOR_FALECIDO = 'Servidor Falecido'


  MANUAL_TYPE = 'Importação manual'

  IMPORTADO_ERROS = 0
  IMPORTADO = 1

  def self.import(file, user)

    errors = false
    feedback_code = nil

    begin
      ActiveRecord::Base.transaction do

        financial_file = FinancialReleaseFile.new
        financial_file.name = file.original_filename
        financial_file.save!

        quote_chars = %w(" | ~ ^ & *)
        CSV.foreach(file.path,{:col_sep => ';', quote_char: quote_chars.shift}) do |row|

          failed = row_will_fail(row, user, financial_file.id)
          if !failed

            employee = PublicEmployee.where(:cpf => row[1], :registration => row[2]).first

            card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee).first

            release = self.new
            release.card = card
            release.value = BigDecimal.new(row[3].gsub(',', '.'))
            release.status = AUTORIZADO_CONSIGNATARIA
            release.operator = user.operator
            release.financial_release_file = financial_file

            release.save!

          else
            errors = true
          end

        end
      end
    rescue Exception => ex
      raise ex
    end

    if errors
      feedback_code = IMPORTADO_ERROS
    else
      feedback_code = IMPORTADO
    end

    return feedback_code
  end


    def self.employee_valid?(employee, consignee_id)
      card = Card.where(:public_employee_id => employee.id, :consignee_id => consignee_id, :status => Card::AUTORIZADO_CONSIGNATARIA).first

      if card
        return true
      else
        return false
      end
    end

    def self.proposal_number_valid?(proposal_number, consignee_id)

      card = Card.where(:proposal_number => proposal_number, :consignee_id => consignee_id, :status => Card::AUTORIZADO_CONSIGNATARIA).first

      if card
        return true
      else
        return false
      end

    end

    def self.discount_value_valid?(discount_value)

      regex = /^\d+\,\d\d$/ #um ou mais digitos seguidos de uma virgula seguido de dois digitos

      if regex.match(discount_value).nil?
        return false
      else
        return true
      end

    end

    def self.release_duplicate?(card_id)

      release = FinancialRelease.where('extract(month from created_at) = ? and status = ? and card_id = ?', Date.today.month, AUTORIZADO_CONSIGNATARIA, card_id).first

      if release
        return true
      else
        return false
      end

    end

    def self.release_off_limit?(discount_value, card_value)

      if discount_value > card_value
        return true
      else
        return false
      end

    end

    def self.create_review(row, error, financial_release_file_id)
      review  = ReviewFinancialRelease.new
      review.error = error
      review.value = row[3]
      review.financial_release_file_id = financial_release_file_id

      return review
    end

    def self.row_will_fail(row, user, financial_release_file_id)

      #nome, cpf, matriucla e valor

      cpf_exist = !row[1].nil?
      registration_exist = !row[2].nil?
      value_exist = !row[3].nil?
      employee = nil
      value_valid = false
      release_duplicate = false
      release_off_limit = false
      employee_valid = false
      fail = false
      reviews = []

      if cpf_exist && registration_exist
        employee = PublicEmployee.where(:cpf => row[1], :registration => row[2]).first
      end

      if employee
        employee_valid = employee_valid?(employee, user.get_consignee.id)
        if !employee_valid
          fail = true
        end
      else
        fail = true
      end

      if value_exist
        value_valid = discount_value_valid?(row[3])
        if !value_valid
          fail = true
        end
      else
        fail = true
      end

      if employee && employee_valid
        card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee, :status => Card::AUTORIZADO_CONSIGNATARIA).first
        release_duplicate = release_duplicate?(card.id)
        if release_duplicate
          fail = true
        end
      end

      if (value_exist && value_valid) && (employee && employee_valid)

        card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee, :status => Card::AUTORIZADO_CONSIGNATARIA).first
        release_off_limit = release_off_limit?(BigDecimal.new(row[3]), card.discount_value)

        if release_off_limit
          fail = true
        end
      end

      if release_duplicate
        reviews << create_review(row, 'Lançamento duplicado.', financial_release_file_id)
      else
        if cpf_exist && registration_exist
          if employee.blank?
            reviews << create_review(row, 'Servidor Não Encontrado', financial_release_file_id)
          end
        else
          reviews << create_review(row, 'CPF ou mátricula em branco', financial_release_file_id)
        end
        if value_exist
          if !value_valid
            reviews << create_review(row, 'Valor inválido.', financial_release_file_id)
          end
        else
          reviews << create_review(row, 'Valor em branco.', financial_release_file_id)
        end
        if release_off_limit
          reviews << create_review(row, 'Valor informado é maior que o valor disponivel do cartão.', financial_release_file_id)
        end
      end

      if employee
        card = Card.where(:public_employee_id => employee.id, :consignee_id => user.get_consignee).first

        reviews.each do |review|
          review.name = employee.name
          review.cpf = employee.cpf
          review.registration = employee.registration
          review.organ = employee.organ.name
        end
      end

      reviews.each do |review|
        review.save!
      end

      return fail

    end

end
