class PublicEmployee < ActiveRecord::Base

  has_paper_trail

  belongs_to :organ
  belongs_to :literacy
  belongs_to :situation
  belongs_to :commissioned
  belongs_to :suspension_age
  belongs_to :marital_status
  has_many :loans
  has_many :assistance_contracts
  has_many :cards

  has_many :margin_histories, dependent: :destroy
  has_one :user

  validates :name, presence: true
  validates :cpf, length: { is: 11}, :allow_blank => true
  validates :zip_code, length: { is: 8}, :allow_blank => true
  validates :phone, length: { in: 10..11 }, :allow_blank => true
  validates :organ_id, presence: true
  validates :registration, presence: true
  #validates :admission_date, presence: true
  validates :situation_id, presence: true
  validates :margin_value, presence: true
  validates :commissioned_id, presence: true
  #validates :suspension_age_id, presence: true
  #validates :end_contract_date, presence: true
  validates :email, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}, allow_blank: true
  validates :registration, :uniqueness => { :scope => :organ_id }

  def loan_margin_available
    if Property.where(:value => Property::INDIVIDUAL).first
      margins = calculate_margin_available_individual

    else
      margins = calculate_margin_available_group
    end

    return margins['loan'].truncate(2)
  end

  def assistance_margin_available
    if Property.where(:value => Property::INDIVIDUAL).first
      margins = calculate_margin_available_individual
    else
      margins = calculate_margin_available_group
    end

    return margins['assistance'].truncate(2)
  end

  def card_margin_available

    if Property.where(:value => Property::INDIVIDUAL).first
      margins = calculate_margin_available_individual
    else
      margins = calculate_margin_available_group
    end
    return margins['card'].truncate(2)
  end


  def validate_approval_password(str)

    if str.blank?
      return false
    else
      if Encryption.encrypt(str) != self.approval_password
        return false
      else
        return true
      end
    end

  end

  def encrypt_password
    if !self.approval_password.blank?
      self.approval_password = Encryption.encrypt(self.approval_password)
    end
  end

  def format_cpf
    return "#{self.cpf[0..2]}.#{self.cpf[3..5]}.#{self.cpf[6..8]}-#{self.cpf[9..10]}"
  end

  private

    def verify_registration_uniqueness_by_organ_create
      employee = PublicEmployee.where(:registration => self.registration, :organ_id => self.organ_id).first

      errors.add(:registration, I18n.t('upload.employee_with_registration_organ_exist') + self.registration) if !employee.nil?

    end

    def verify_registration_uniqueness_by_organ_update

      employee = PublicEmployee.where(:registration => self.registration, :organ_id => self.organ_id).first

      if employee
        if self.cpf != employee.cpf
          errors.add(:registration, 'Já está em uso para o órgão especificado')
        end
      end

    end

    def calculate_margin_available_individual

      brute_margin = self.margin_value

      margins = Hash.new

      perc_loan = BigDecimal.new(Property.where(:name => Property::LOAN).first.value)
      perc_assistance = BigDecimal.new(Property.where(:name => Property::ASSISTANCE).first.value)
      perc_card = BigDecimal.new(Property.where(:name => Property::CARD).first.value)

      discounts_loan = loan_discounts
      discounts_assistance = assistance_discounts
      discounts_card = card_discounts
      discounts_total = discounts_loan + discounts_card + discounts_assistance

      margin_tmp_loan = (brute_margin * perc_loan) - discounts_loan
      margin_tmp_assistance = (brute_margin * perc_assistance) - discounts_assistance
      margin_tmp_card = (brute_margin * perc_card) - discounts_card

      if (margin_tmp_loan < 0 && margin_tmp_card < 0 && margin_tmp_assistance < 0) || (margin_tmp_loan > 0 && margin_tmp_card > 0 && margin_tmp_assistance > 0)
        margins['loan'] = margin_tmp_loan
        margins['card'] = margin_tmp_card
        margins['assistance'] = margin_tmp_assistance
      elsif margin_tmp_loan > 0 || margin_tmp_card > 0 || margin_tmp_assistance > 0
        total_positive = 0

        loan_positive = false
        card_positive = false
        assistance_positive = false

        if margin_tmp_loan > 0
          total_positive += 1
          loan_positive = true
        end

        if margin_tmp_card > 0
          total_positive += 1
          card_positive = true
        end

        if margin_tmp_assistance > 0
          total_positive += 1
          assistance_positive = true
        end

        margin_available = (brute_margin - discounts_total)/total_positive

        if loan_positive
          margins['loan'] = margin_available
        else
          margins['loan'] = margin_tmp_loan
        end

        if card_positive
          margins['card'] = margin_available
        else
          margins['card'] = margin_tmp_card
        end

        if assistance_positive
          margins['assistance'] = margin_available
        else
          margins['assistance'] = margin_tmp_assistance
        end


      elsif discounts_total > brute_margin
        if margin_tmp_loan > 0
          margins['loan'] = BigDecimal.new(0)
        else
          margins['loan'] = margin_tmp_loan
        end
        if margin_tmp_card > 0
          margins['card'] = BigDecimal.new(0)
        else
          margins['card'] = margin_tmp_card
        end
        if margin_tmp_assistance > 0
          margins['assistance'] = BigDecimal.new(0)
        else
          margins['assistance'] = margin_tmp_assistance
        end
      end

      return margins

    end

    def calculate_margin_available_group
      brute_margin = self.margin_value

      margins = Hash.new

      perc_group = BigDecimal.new(Property.where(:name => Property::PRODUCT_GROUP).first.value)
      perc_card = BigDecimal.new(Property.where(:name => Property::CARD).first.value)

      discounts_group = loan_discounts + assistance_discounts
      discounts_card = card_discounts
      discounts_total = discounts_group + discounts_card

      margin_tmp_group = (brute_margin * perc_group) - discounts_group
      margin_tmp_card = (brute_margin * perc_card) - discounts_card

      if margin_tmp_group == 0
        margins['loan'] = BigDecimal.new(0)
        margins['assistance'] = BigDecimal.new(0)
      end

      if margin_tmp_card == 0
        margins['card'] = BigDecimal.new(0)
      end

      if (margin_tmp_group < 0 && margin_tmp_card) || (margin_tmp_group > 0 && margin_tmp_card > 0)
        margins['loan'] = margin_tmp_group
        margins['card'] = margin_tmp_card
        margins['assistance'] = margin_tmp_group
      elsif margin_tmp_group > 0 || margin_tmp_card > 0

        total_positive = 0

        group_positive = false
        card_positive = false

        if margin_tmp_group > 0
          total_positive += 1
          group_positive = true
        end

        if margin_tmp_card > 0
          total_positive += 1
          card_positive = true
        end

        margin_available = (brute_margin - discounts_total)/total_positive

        if group_positive
          margins['loan'] = margin_available
          margins['assistance'] = margin_available
        else
          margins['loan'] = margin_tmp_group
          margins['assistance'] = margin_tmp_group
        end

        if card_positive
          margins['card'] = margin_available
        else
          margins['card'] = margin_tmp_card
        end

      elsif discounts_total > brute_margin
        if margin_tmp_group > 0
          margins['loan'] = BigDecimal.new(0)
          margins['assistance'] = BigDecimal.new(0)
        else
          margins['loan'] = margin_tmp_group
          margins['assistance'] = margin_tmp_group
        end
        if margin_tmp_card > 0
          margins['card'] = BigDecimal.new(0)
        else
          margins['card'] = margin_tmp_card
        end
      else
        margins['loan'] = BigDecimal.new(0)
        margins['card'] = BigDecimal.new(0)
        margins['assistance'] = BigDecimal.new(0)
      end

      return margins


    end

    def loan_discounts
      discounts_loan = BigDecimal.new(0)
      loans = Loan.where(:public_employee_id => self.id, :status => [Loan::AUTORIZADO_SERVIDOR, Loan::APROVADO, Loan::SUSPENSO, Loan::EM_FOLHA] )
      loans.each do |loan|
        discounts_loan += loan.installment_value
      end

      return discounts_loan
    end

    def assistance_discounts
      discounts_assistance = BigDecimal.new(0)
      assistances = AssistanceContract.where(:public_employee_id => self.id, :status => [AssistanceContract::AUTORIZADO_SERVIDOR, AssistanceContract::APROVADO, AssistanceContract::SUSPENSO, AssistanceContract::EM_FOLHA])

      assistances.each do |assistance|
        discounts_assistance += assistance.discount_value
        variations = Variation.where(:assistance_contract_id => assistance.id, :status => [Variation::AUTORIZADO_SERVIDOR, Variation::APROVADO, Variation::EM_FOLHA, Variation::SUSPENSO])
        variations.each do |variation|
          discounts_assistance += variation.value
        end
      end

      return discounts_assistance
    end

    def card_discounts
      discounts_card = BigDecimal.new(0)
      cards = Card.where(:public_employee_id => self.id, :status => [Card::AUTORIZADO_SERVIDOR, Card::AUTORIZADO_CONSIGNATARIA])

      cards.each do |card|
        discounts_card += card.discount_value
      end

      return discounts_card
    end

end
