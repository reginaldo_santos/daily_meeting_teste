class Operator < ActiveRecord::Base

  has_paper_trail

  has_one :user, :dependent => :destroy
  has_many :loans
  has_many :loan_observations
  has_many :assistance_contracts
  has_many :variations
  has_many :cards
  has_many :card_observations
  has_many :financial_releases

  belongs_to :consignee
  belongs_to :attendance_local

  accepts_nested_attributes_for :user


  validates :name, :presence => true, :on => :create
  validates :cpf, :presence => true
  validate :cpf_uniqueness
  validates :cpf, :length => { :minimum => 11, :maximum => 11 }
  validates :company_phone_number, :presence => true
  validate :validate_consignee_or_attendance_local
  #validates :email, :presence => true

  attr_accessor :hidden_id, :current_user_roles, :roles_for_save

  before_save :set_consignee_or_attendance_local

  def get_consignee
    consignee = nil
    consignee = self.consignee if self.consignee
    consignee = self.attendance_local.consignee if self.attendance_local
    return consignee
  end

  private

  def validate_consignee_or_attendance_local
    if !self.current_user_roles.blank?
      if self.current_user_roles.include?(User::ADMIN)
        roles = self.roles_for_save
        if !roles.include?(User::ADMIN) && !roles.include?(User::RH_MASTER) && !roles.include?(User::RH_ONE) && !roles.include?(User::RH_ORGAN) && !roles.include?(User::GESTOR_CONVENIO)
          if self.consignee_id.blank? && self.attendance_local_id.blank?
            errors.add(:consignee_id, 'Consignataria ou Local de atendimento deve ser preenchido')
            errors.add(:attendance_local_id, 'Consignataria ou Local de atendimento deve ser preenchido')
          end

          if !self.consignee_id.blank? && !self.attendance_local_id.blank?
            errors.add(:consignee_id, 'Apenas consignataria ou local de atendimento deve ser preenchido, nunca os dois')
            errors.add(:attendance_local_id, 'Apenas consignataria ou local de atendimento deve ser preenchido, nunca os dois')
          end
        end
      end
    end

  end

  def cpf_uniqueness
    if !Operator.where.not(:id => self.id).where(:cpf => self.cpf).blank?
      errors.add(:cpf, 'O CPF informado já está sendo utilizado.')
    end
  end

  def set_consignee_or_attendance_local
    if !self.current_user_roles.blank?
      if self.consignee_id.blank? && !self.hidden_id.blank?
        current_operator = Operator.find(self.hidden_id)
        if !current_operator.consignee_id.blank? || !current_operator.attendance_local_id.blank?
          current_operator.consignee_id.blank? ? self.consignee_id = current_operator.attendance_local.consignee_id : self.consignee_id = current_operator.consignee_id
        end
      end
    end
  end

end
