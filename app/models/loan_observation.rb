class LoanObservation < ActiveRecord::Base

  has_paper_trail

  belongs_to :operator
  belongs_to :loan

  validates :text, presence: true
  validates :loan_id, presence: true
  validates :operator_id, presence: true

end
