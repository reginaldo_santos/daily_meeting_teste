class MarginHistory < ActiveRecord::Base

  has_paper_trail

  belongs_to :public_employee

  validates :margin_value, presence: true
  validates :public_employee_id, presence: true

  private

end
