class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    guest_user = false

    if user.roles_mask == 0 || user.roles_mask.nil?
      guest_user = true
    end

    if user.isS?(User::ADMIN)
      can :manage, :holiday
      can :manage, :search_form
      can :manage, :file
      can :manage, Property
      can :manage, :users
      can :manage, :insertion
      can :manage, :card_controller
      can :manage, PublicEmployee
      can :manage, FinancialRelease
      can :manage, :attendance_local
      can :manage, :consignee
      can :manage, Loan
      can :manage, :assistance
      can :manage, :variation
      can :manage, Organ
      can :manage, ReturnCode
      can :manage, :holidays
      can :manage, Product
      can :manage, :operators
      can :manage, :report
      can :manage, :search_contract
      can :manage, PortabilityRequest
      can :manage, :session
      can :manage, InsertionLine
    end

    if user.isS?(User::USER)
      can :manage, :session
      can :dashboard, :users
    end

    if user.isS?(User::RH_MASTER)
      # suspender contrato
      can :manage_loan, Loan
      can :suspended, Loan
      can :suspended_execute, Loan
      #pode fazer qualquer coisa com consignatária e produto
      can :manage, :consignee
      can :manage, Product
      #pode fazer qualquer coisa com orgao
      can :manage, Organ
      # pode fazer qualquer coisa com servidor
      can :manage, PublicEmployee
      # arquivo de margem e arquivo de retorno
      can :manage, :file
      # arquivo de insercao
      can :manage, :insertion
      # pode fazer qualquer coisa com usuario
      can :manage, :users
      # pode fazer qualquer coisa com codigo de retorno
      can :manage, ReturnCode
      # pode fazer qualquer coisa com relatorios
      can :manage, :report
      #pode fazer qualquer coisa com propriedades
      can :manage, Property
      can :manage, :session
    end

    if user.isS?(User::RH_ONE)
      # suspender contrato
      can :manage_loan, Loan
      can :suspended, Loan
      can :suspended_execute, Loan
      # gerar senha de reserva de margem
      can :index, PublicEmployee
      can :generate_approval_password, PublicEmployee
      # arquivo de margem e arquivo de retorno
      can :manage, :file
      # arquivo de insercao
      can :manage, :insertion
      #pode fazer qualquer coisa com consignatária e produto
      can :manage, :consignee
      can :manage, Product
      #pode fazer qualquer coisa com orgao
      can :manage, Organ
      # pode fazer qualquer coisa com servidor
      can :manage, PublicEmployee
      # pode fazer qualquer coisa com relatorios
      can :manage, :report
      can :manage, :session
      can :dashboard, :users
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::RH_ORGAN)
      # suspender contrato
      can :manage_loan, Loan
      can :suspended, Loan
      can :suspended_execute, Loan
      # gerar senha de reserva de margem
      can :index, PublicEmployee
      can :generate_approval_password, PublicEmployee
      # relatorio
      can :manage, :report
      can :manage, :session
      can :dashboard, :users
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::GESTOR_CONVENIO)
      #relatorio
      can :manage, :report
      can :manage, :session
      can :dashboard, :users
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::CONSIGNEE_LOAN_MASTER)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      # reservar margem
      can :new, Loan
      can :create, Loan
      can :new_refin, Loan
      can :create_refin, Loan
      #aprovar contrato
      can :manage_loan, Loan
      can :approve, Loan
      can :approve_execute, Loan
      #reprovar contrato
      can :refuse, Loan
      can :refuse_execute,Loan
      #cancelar contrato
      can :cancel, Loan
      can :cancel_execute, Loan
      #liquidar contrato
      can :liquidate, Loan
      can :liquidate_execute, Loan
      #adicionar senha de aprovação do servidor
      can :edit, Loan
      can :update, Loan
      #usuários consignataria
      can :manage, :users
      #local de atendimento
      can :manage, :attendance_local
      #relatorios
      can :manage_reports, :report
      can :manage, :session
      can :manage, InsertionLine
      can :manage, ProductConfiguration

      #finalizar portabilidade
      can :manage_requests, PortabilityRequest
      can :new_portability, Loan
      can :create_portability, Loan

      #responder portabilidade
      can :manage_requests, PortabilityRequest
      can :answer_request, PortabilityRequest
      can :answer_request_execute, PortabilityRequest

      # reter portabilidade
      can :manage_requests, PortabilityRequest
      can :new_refin, Loan
      can :create_refin, Loan

      # solicitar portabilidade
      can :search_for_portability, Loan
      can :request_portability, Loan
    end

    if user.isS?(User::SELLER_LOAN_ONE)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage_loan, Loan
      can :show, Loan
    end

    if user.isS?(User::SELLER_LOAN_TWO)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      # reservar margem
      can :new, Loan
      can :create, Loan
      can :new_refin, Loan
      can :create_refin, Loan
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      #gerenciar emprestimo
      can :manage_loan, Loan
      #exibir emprestimo
      can :show, Loan
    end

    if user.isS?(User::FORMALIZATION_LOAN)
      #aprovar contrato
      can :manage_loan, Loan
      can :approve, Loan
      can :approve_execute, Loan
      #reprovar contrato
      can :refuse, Loan
      can :refuse_execute,Loan
      #liquidar contrato
      can :liquidate, Loan
      can :liquidate_execute, Loan
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :show, Loan
    end

    if user.isS?(User::USERS_LOAN)
      #usuários consignataria
      can :manage, :users
      #local de atendimento
      can :manage, :attendance_local
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::REPORTS_LOAN)
      #relatorios
      can :manage, :report
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::CONSIGNEE_ASSISTANCE_MASTER)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      #reservar margem
      can :new, :assistance
      can :create, :assistance
      #aprovar contrato
      can :manage_assistance_contract, :assistance
      can :approve, :assistance
      can :approve_execute, :assistance
      #reprovar contrato
      can :refuse, :assistance
      can :refuse_execute, :assistance
      #cancelar contrato assistencial
      can :cancel, :assistance
      can :cancel_execute, :assistance
      # usuarios consignataria
      can :manage, :users
      #local de atendimento
      can :manage, :attendance_local
      #relatorios
      can :manage, :report
      # incluir e excluir variacao
      can :manage, :variation
      can :manage, :session
      can :manage, InsertionLine
      can :manage, ProductConfiguration
    end

    if user.isS?(User::SELLER_ASSISTANCE_ONE)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::SELLER_ASSISTANCE_TWO)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      #reservar margem
      can :new, :assistance
      can :create, :assistance
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::USERS_ASSISTANCE)
      # usuarios consignataria
      can :manage, :users
      #local de atendimento
      can :manage, :attendance_local
      can :manage, :session
    end

    if user.isS?(User::REPORTS_ASSISTANCE)
      #relatorios
      can :manage, :report
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::FORMALIZATION_ASSISTANCE)
      #aprovar contrato
      can :manage_assistance_contract, :assistance
      can :approve, :assistance
      can :approve_execute, :assistance
      #reprovar contrato
      can :refuse, :assistance
      can :refuse_execute, :assistance
      # incluir e excluir variacao
      can :manage, :variation
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::CONSIGNEE_CARD_MASTER)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      #reservar margem
      can :new, :card_controller
      can :create, :card_controller
      #aprovar cartao
      can :manage_cards, :card_controller
      can :approve, :card_controller
      can :approve_execute, :card_controller
      can :show, :card_controller
      #reprovar cartao
      can :refuse, :card_controller
      can :refuse_execute, :card_controller
      #cancelar cartao
      can :cancel, :card_controller
      can :cancel_execute, :card_controller
      #usuarios consignataria
      can :manage, :users
      #local de atendimento
      can :manage, :attendance_local
      #relatorios
      can :manage, :report
      #lancamento fatura
      can :manage, FinancialRelease
      can :manage, :session
      can :manage, InsertionLine
      can :manage, ProductConfiguration
    end

    if user.isS?(User::SELLER_CARD_ONE)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::SELLER_CARD_TWO)
      #consultar margem
      can :search_employee, :search_form
      can :search, :search_form
      can :search_for_operation, PublicEmployee
      can :manage_cards, :card_controller
      #reservar margem
      can :new, :card_controller
      can :create, :card_controller
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :show, :card_controller
    end

    if user.isS?(User::USERS_CARD)
      #usuarios consignataria
      can :manage, :users
      #local de atendimento
      can :manage, :attendance_local
      can :manage, :session
    end

    if user.isS?(User::REPORTS_CARD)
      #relatorios
      can :manage, :report
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::FILE_CARD)
      #lancamento fatura
      can :manage, FinancialRelease
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
    end

    if user.isS?(User::FORMALIZATION_CARD)
      #aprovar cartao
      can :manage_cards, :card_controller
      can :approve, :card_controller
      can :approve_execute, :card_controller
      #reprovar cartao
      can :refuse, :card_controller
      can :refuse_execute, :card_controller
      #cancelar cartao
      can :card_cancel, :card_controller
      can :cancel_execute, :card_controller
      can :manage, :session
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :show, :card_controller
    end

    if user.isS?(User::PORTABILITY_ANSWER)
      can :manage_requests, PortabilityRequest
      can :answer_request, PortabilityRequest
      can :answer_request_execute, PortabilityRequest
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if user.isS?(User::PORTABILITY_RETAIN)
      can :manage_requests, PortabilityRequest
      can :new_refin, Loan
      can :create_refin, Loan
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if user.isS?(User::PORTABILITY_REQUEST)
      can :search_for_portability, Loan
      can :request_portability, Loan
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if user.isS?(User::PORTABILITY_FINISH)
      can :manage_requests, PortabilityRequest
      can :new_portability, Loan
      can :create_portability, Loan
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if user.isS?(User::CARD_FILES)
      can :manage, InsertionLine
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if user.isS?(User::LOAN_FILES)
      can :manage, InsertionLine
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if user.isS?(User::ASSISTANCE_FILES)
      can :manage, InsertionLine
      can :dashboard, :users
      # alterar dados pessoais
      can :change_personal_data, :users
      can :update_personal_data, :users
      # alterar senha
      can :change_password, :users
      can :update_password, :users
      can :manage, :session
    end

    if guest_user
      can :manage, :session
      can :new_employee_user, User
      can :create_employee_user, User
      can :inform_login_and_password, User
      can :update_login_for_employee_user, User
      can :manage, :password_reset
      can :dashboard, :users
      can :change_password, :users
      can :update_password, :users
    end

  end
end
