class Literacy < ActiveRecord::Base

  has_paper_trail

  has_many :public_employees

  validates :name, presence: true

end
