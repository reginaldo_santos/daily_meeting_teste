class AssistanceContract < ActiveRecord::Base

  has_paper_trail

  belongs_to :public_employee
  belongs_to :operator
  belongs_to :consignee

  has_many :variations
  has_many :insertion_lines
  has_many :assistance_observations

  validates :discount_value, presence: true
  validates :approval_password, presence: true, :on => :create
  validate :validate_approval_password
  validate :validate_discount_value, :on => :create

  before_create :set_status_assistance

  attr_accessor :approval_password

  #STATUS
  AGUARDANDO_SERVIDOR =  'Aguardando Servidor'
  AUTORIZADO_SERVIDOR = 'Autorizado servidor'
  CANCELADO = 'Cancelado'
  APROVADO = 'Aprovado consignataria'
  EM_FOLHA = 'Em folha'
  SUSPENSO = 'Suspenso'
  PAGO_SERVIDOR = 'Pago pelo servidor'
  LIQUIDADO = 'Liquidado'
  SERVIDOR_FALECIDO = 'Servidor Falecido'

  def validate_discount_value
    if self.discount_value > self.public_employee.assistance_margin_available
      errors.add(:discount_value, 'maior do que margem disponivel')
    end
  end

  def validate_approval_password
    if !approval_password.blank?
      if !self.public_employee.validate_approval_password(approval_password)
        errors.add(:approval_password, 'incorreta')
      end
    end
  end

  def set_status_assistance
    if !self.approval_password.blank? && self.public_employee.validate_approval_password(self.approval_password)
      config = ProductConfiguration.joins(:product, :configuration).where('products.consignee_id = ? and configurations.category = ?', self.operator.get_consignee.id, Configuration::ASSISTANCE).first
      if config && config.value == true
        self.status = AssistanceContract::APROVADO
      else
        self.status = AssistanceContract::AUTORIZADO_SERVIDOR
      end
    else
      self.status = AssistanceContract::AGUARDANDO_SERVIDOR
    end
  end

end
