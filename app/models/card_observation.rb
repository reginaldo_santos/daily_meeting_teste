class CardObservation < ActiveRecord::Base

  has_paper_trail

  belongs_to :operator
  belongs_to :card

end
