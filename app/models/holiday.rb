class Holiday < ActiveRecord::Base

  has_paper_trail

  enum holiday_type: {
    municipal: 1,
    estadual: 2,
    nacional: 3
  }

  validates :observation, :presence => true
  validates :holiday_type, :presence => true
  validates :holiday_date, :presence => true

end
