class ReturnCode < ActiveRecord::Base

	has_paper_trail

	validates :code, :presence => true, :uniqueness => true
	validates :description, :presence => true

end
