class InsertionLine < ActiveRecord::Base

  has_paper_trail

  belongs_to :insertion
  belongs_to :loan
  belongs_to :financial_release
  belongs_to :variation
  belongs_to :assistance_contract
  belongs_to :consignee


  #TYPE
  VARIATION = 'VARIATION'
  LOAN = 'LOAN'
  FINANCIAL_RELEASE = 'FINANCIAL_RELEASE'
  ASSISTANCE = 'ASSISTANCE'

  def get_financial_operation

    case self.insertion_type
      when VARIATION
        self.variation
      when LOAN
        self.loan
      when FINANCIAL_RELEASE
        self.financial_release
      when ASSISTANCE
        self.assistance_contract
    end

  end

  def get_contract_number
    case self.insertion_type
      when VARIATION
        ''
      when LOAN
        self.loan.contract_number
      when FINANCIAL_RELEASE
        ''
      when ASSISTANCE
        self.assistance_contract.contract_number
    end
  end

  def get_operation_value
    case self.insertion_type
      when VARIATION
        self.variation.value
      when LOAN
        self.loan.installment_value
      when FINANCIAL_RELEASE
        self.financial_release.value
      when ASSISTANCE
        self.assistance_contract.discount_value
    end
  end

end
