class Configuration < ActiveRecord::Base

  has_many :product_configurations

  validates :name,  presence: true
  validates :observation, presence: true

  #categories
  LOAN = 'LOAN'
  ASSISTANCE = 'ASSISTANCE'
  CARD = 'CARD'
end
