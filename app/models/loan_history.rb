class LoanHistory < ActiveRecord::Base

  has_paper_trail

  belongs_to :loan

end
