class AssistanceContract::AsApprove < ActiveType::Record[AssistanceContract]

  attribute :observation, :text
  attribute :operator_id, :integer

  before_save :set_status
  after_save :create_observation if :validate_observation

  validates :contract_number, :presence => true

  def set_status
    self.status = AssistanceContract::APROVADO
  end

  def create_observation
    observation = AssistanceObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.assistance_contract = self
    observation.save!
  end

  def validate_observation
    self.observation.blank? ? false : true
  end
end