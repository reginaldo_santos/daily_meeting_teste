class AssistanceContract::AsCancel < ActiveType::Record[AssistanceContract]

  attribute :observation, :text
  attribute :password, :string
  attribute :operator_id, :integer

  before_save :set_cancel_status
  after_save :create_observation

  validates :observation, presence: true
  validates :password, presence:true
  validate :validate_password_key

  def set_cancel_status
    self.status = AssistanceContract::CANCELADO
  end

  def validate_password_key
    if !self.password.blank?
      if self.public_employee.approval_password != Encryption.encrypt(self.password)
        errors.add(:password, 'Senha Incorreta')
      end
    else
      errors.add(:password, 'Não pode ficar em branco')
    end
  end

  def create_observation
    observation = AssistanceObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.assistance_contract = self
    observation.save!
  end
end