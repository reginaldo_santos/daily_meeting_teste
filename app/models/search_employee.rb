class SearchEmployee
  include ActiveModel::Validations
  include ActiveModel::Conversion
  include ActiveModel::Naming

  attr_accessor :cpf, :registration
  validate :field_presence

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  private

    def field_presence

      if !self.cpf.present? && !self.registration.present?
        errors.add(:cpf, 'Pelo menos um campo deve ser preenchido')
        errors.add(:registration, 'Pelo menos um campo deve ser preenchido')
      end

    end


end