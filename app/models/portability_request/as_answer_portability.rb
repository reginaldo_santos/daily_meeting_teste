class PortabilityRequest::AsAnswerPortability < ActiveType::Record[PortabilityRequest]

  attribute :balance_due_release, :decimal

  validates :observation_seller, presence: true
  validates :portability_code, presence: true
  validates :balance_due_release, presence: true
  validate :check_balance_due_value

  before_save :set_status
  after_save :update_balance_due_release


  private

    def set_status
      self.status = PortabilityRequest::RESPONDIDO
    end

    def update_balance_due_release
      self.loan.update_attribute(:balance_due_release, self.balance_due_release)
    end

    def check_balance_due_value
      if self.loan.balance_due_release && self.balance_due_release > self.loan.balance_due
        errors.add(:balance_due_release, 'Não pode ser maior do que o valor atual')
      end
    end

end