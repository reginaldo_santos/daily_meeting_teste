class Consignee < ActiveRecord::Base

  has_paper_trail

  has_many :attendance_locals
  has_many :products
  has_many :operators
  has_many :loans
  has_many :assistance_contracts
  has_many :cards
  has_many :purchase_requests, :class_name => 'PortabilityRequest', :foreign_key => 'buyer_id'
  has_many :sale_requests, :class_name => 'PortabilityRequest', :foreign_key => 'seller_id'
  has_many :insertion_lines

  validates :name, :presence => true
  validates :cnpj, :presence => true
  validates :cnpj, :uniqueness => true, on: :create
  validates :street, :presence => true
  validates :number, :presence => true
  validates :neighborhood, :presence => true
  validates :zip_code, :presence => true
  validates :city, :presence => true
  validates :state, :presence => true
  validates :country, :presence => true
  validates :contact, :presence => true
  validates :post, :presence => true
  validates :phone_number, :presence => true

end
