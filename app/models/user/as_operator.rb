class User::AsOperator < ActiveType::Record[User]

	validate :presence_of_role
	validates_associated :public_employee

	def presence_of_role
		if self.role_ids.blank?
			errors.add(:role_ids, 'Selecione ao menos um perfil para o usuário')
		end
	end
end