class User::AsChangePassword < ActiveType::Record[User]

  attribute :actual_password, :string
  attribute :new_pwd, :string
  attribute :pwd_confirmation, :string

  validates :actual_password, presence: true
  validates :new_pwd, presence: true
  validates :pwd_confirmation, presence: true
  validate :check_current_password
  validate :check_equality_current_password_and_password
  validate :check_equality_password_and_confirmation

  before_save :set_last_password_change
  after_validation :set_pwd_and_confirmation

  private

  def check_current_password
    valid_password = true if self.authenticate(self.actual_password)
    if !valid_password
      errors.add(:actual_password, 'Senha Incorreta')
    end
  end

  def check_equality_password_and_confirmation
    if self.new_pwd != self.pwd_confirmation
      errors.add(:pwd_confirmation, 'Confirmação diferente da nova senha.')
    end
  end

  def check_equality_current_password_and_password
    if self.actual_password == self.new_pwd
      errors.add(:password, 'Igual a senha atual.')
    end
  end

  def set_last_password_change
    self.last_password_change = Time.now
  end

  def set_pwd_and_confirmation
    self.password = self.new_pwd
    self.password_confirmation = self.pwd_confirmation
  end

end