class Card < ActiveRecord::Base

  has_paper_trail

  belongs_to :operator
  belongs_to :public_employee
  belongs_to :consignee
  has_many :card_observations, dependent: :destroy
  has_many :financial_releases, dependent: :destroy

  attr_accessor :approval_password

  validates :discount_value, presence: true
  validates :public_employee_id, presence: true
  validates :consignee_id, presence: true
  validates :operator_id, presence: true
  validates :approval_password, presence: true, :on => :create
  validate :validate_discount_value, on: :create
  validate :validate_approval_password
  validate :check_uniqueness_proposal_number,:allow_blank => true

  before_create :set_status_card

  #STATUS
  AGUARDANDO_SERVIDOR =  'Aguardando Servidor'
  AUTORIZADO_SERVIDOR = 'Autorizado servidor'
  CANCELADO = 'Cancelado'
  AUTORIZADO_CONSIGNATARIA = 'Autorizado consignatária'
  REPROVADO_CONSIGNATARIA = 'Reprovado consignatária'


  def validate_discount_value
    if !self.discount_value.blank?
      if self.discount_value > self.public_employee.card_margin_available
        errors.add(:discount_value, 'maior do que margem disponivel')
      end
    end
  end

  def validate_approval_password
    if !approval_password.blank?
      if !self.public_employee.validate_approval_password(approval_password)
        errors.add(:approval_password, 'incorreta')
      end
    end
  end

  def check_uniqueness_proposal_number
    if !self.proposal_number.blank?
      card = Card.where(:proposal_number=> self.proposal_number, :consignee_id => self.consignee_id).first
      if card
        errors.add(:proposal_number, 'já está em uso')
      end
    end
  end

  def set_status_card
    if !self.approval_password.blank? && self.public_employee.validate_approval_password(self.approval_password)
      config = ProductConfiguration.joins(:product, :configuration).where('products.consignee_id = ? and configurations.category = ?', self.operator.get_consignee.id, Configuration::CARD).first
      if config && config.value == true
        self.status = Loan::APROVADO
      else
        self.status =  Loan::AUTORIZADO_SERVIDOR
      end
    else
      self.status =  Loan::AGUARDANDO_SERVIDOR
    end
  end

end
