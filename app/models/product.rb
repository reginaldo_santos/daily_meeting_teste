class Product < ActiveRecord::Base

  has_paper_trail

  belongs_to :consignee
  belongs_to :product_type
  has_many :product_configurations

  validates :deadline, presence: true
  validates :product_type_id, presence: true
  validates :event_code, presence: true, :uniqueness => true
  validates :consignee_id, presence: true

  after_create :define_configuration


  private

  def define_configuration
    configuration = ProductConfiguration.new
    configuration.product = self

    case self.product_type.name
      when ProductType::LOAN
        configuration.configuration = Configuration.where(:category => 'LOAN').first
      when ProductType::ASSISTANCE
        configuration.configuration = Configuration.where(:category => 'ASSISTANCE').first
      when ProductType::CARD
        configuration.configuration = Configuration.where(:category => 'CARD').first
      else
        configuration.configuration = Configuration.where(:category => 'LOAN').first
    end

    configuration.value = false
    configuration.save!
  end
end