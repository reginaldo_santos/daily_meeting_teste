class User < ActiveRecord::Base
  has_secure_password
  has_paper_trail

  attr_accessor :current_password, :origin, :skip_validation

  belongs_to :operator
  belongs_to :public_employee

  has_many :authorizations
  has_many :roles, :through => :authorizations

  accepts_nested_attributes_for :operator

  validates :password, :length => {:minimum => 5, :maximum => 10}, :allow_blank => true, :on => :update
  validates :login, :presence => true, :length => {:minimum => 3, :maximum => 25}
  validates :login, :uniqueness => true
  after_create :send_password_to_user_operator

  UNDEFINED = 'UNDEFINED'


  ADMIN = 'Admin'
  USER = 'User'
  OPERATOR = 'Operator'
  RH_MASTER = 'RH Master'
  RH_ONE = 'RH 1'
  RH_ORGAN = 'RH Órgão'
  GESTOR_CONVENIO = 'Gestor Convênio'
  CONSIGNEE_LOAN_MASTER = 'Master - Emprestimo'
  SELLER_LOAN_ONE = 'Atendimento1 - Emprestimo'
  SELLER_LOAN_TWO = 'Atendimento2 - Emprestimo'
  USERS_LOAN = 'Usuários - Emprestimo'
  REPORTS_LOAN = 'Relatórios - Emprestimo'
  FORMALIZATION_LOAN = 'Formalização - Emprestimo'
  PORTABILITY_ANSWER = 'Portabilidade - Responder'
  PORTABILITY_RETAIN = 'Portabilidade - Reter'
  PORTABILITY_REQUEST = 'Portabilidade - Solicitar'
  PORTABILITY_FINISH = 'Portabilidade - Finalizar'
  CONSIGNEE_ASSISTANCE_MASTER = 'Master - Assistencial'
  SELLER_ASSISTANCE_ONE = 'Atendimento1 - Assistencial'
  SELLER_ASSISTANCE_TWO = 'Atendimento2 - Assistencial'
  USERS_ASSISTANCE = 'Usuários - Assistencial'
  REPORTS_ASSISTANCE = 'Relatórios - Assistencial'
  FORMALIZATION_ASSISTANCE = 'Formalização - Assistencial'
  CONSIGNEE_CARD_MASTER = 'Master - Cartão'
  SELLER_CARD_ONE = 'Atendimento1 - Cartão'
  SELLER_CARD_TWO = 'Atendimento2 - Cartão'
  USERS_CARD = 'Usuários - Cartão'
  REPORTS_CARD = 'Relatórios - Cartão'
  FORMALIZATION_CARD = 'Formalização - Cartão'
  FILE_CARD = 'Cartão - Importar Descontos'
  CARD_FILES = 'Cartão - Arquivos'
  LOAN_FILES = 'Empréstimo - Arquivos'
  ASSISTANCE_FILES = 'Assistencial - Arquivos'


  ROLES_RH = [RH_MASTER, RH_ONE, RH_ORGAN, GESTOR_CONVENIO]
  ROLES_LOAN = [CONSIGNEE_LOAN_MASTER, SELLER_LOAN_ONE,
           SELLER_LOAN_TWO, USERS_LOAN, REPORTS_LOAN, FORMALIZATION_LOAN, PORTABILITY_ANSWER, PORTABILITY_RETAIN, PORTABILITY_REQUEST, PORTABILITY_FINISH, LOAN_FILES]
  ROLES_ASSISTANCE = [CONSIGNEE_ASSISTANCE_MASTER, SELLER_ASSISTANCE_ONE, SELLER_ASSISTANCE_TWO,
           USERS_ASSISTANCE, REPORTS_ASSISTANCE, FORMALIZATION_ASSISTANCE, ASSISTANCE_FILES]
  ROLES_CARD = [CONSIGNEE_CARD_MASTER, SELLER_CARD_ONE, SELLER_CARD_TWO,
           USERS_CARD, REPORTS_CARD, FORMALIZATION_CARD, FILE_CARD, CARD_FILES]
  ROLES_OTHER = [ADMIN, USER]

  ROLES = ROLES_OTHER + ROLES_RH + ROLES_LOAN + ROLES_ASSISTANCE + ROLES_CARD

  RH_GROUP = 'RH'
  ADM_GROUP = 'ADM'
  LOAN_GROUP = 'EMP'
  ASSISTANCE_GROUP = 'ASSIS'
  CARD_GROUP = 'CAR'

  def send_password_reset
    self.password_reset_token = generate_reset_token
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def send_create_user_instructions
    self.create_user_token = generate_user_create_token
    self.create_user_token_sent_at = Time.zone.now
    save!
    UserMailer.define_login_and_password(self).deliver
  end

  def send_password_to_user_operator
    UserMailer.send_password_to_user_operator(self).deliver if self.operator
  end

  def generate_reset_token
    token = SecureRandom.urlsafe_base64

    while !token_valid?(token) do
      token = SecureRandom.urlsafe_base64
    end

    return token
  end

  def generate_user_create_token
    token = SecureRandom.urlsafe_base64

    while !create_user_token_valid?(token) do
      token = SecureRandom.urlsafe_base64
    end

    return token
  end

  def get_consignee
    if self.operator.attendance_local
      return self.operator.attendance_local.consignee
    else
      return self.operator.consignee
    end
  end

  def validate_public_employee_and_operator
    if !self.public_employee_id.blank? && !self.operator_id.blank?
      errors.add(:consignee_id, 'Apenas servidor ou operador deve ser preenchido, nunca os dois')
      errors.add(:attendance_local_id, 'Apenas servidor ou operador deve ser preenchido, nunca os dois')
    end
  end

  def get_email
    if self.public_employee
      return self.public_employee.email
    else
      return self.operator.email
    end
  end

  def isS?(role)
    !roles.where(name: role).empty?
  end

  private

    def token_valid?(token)
      user = User.where(:password_reset_token => token).first

      if user
        return false
      else
        return true
      end
    end

    def create_user_token_valid?(token)
      user = User.where(:create_user_token => token).first

      if user
        return false
      else
        return true
      end
    end

end
