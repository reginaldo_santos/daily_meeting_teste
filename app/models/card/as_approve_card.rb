class Card::AsApproveCard < ActiveType::Record[Card]

  attribute :observation, :text
  attribute :operator_id, :integer

  before_save :set_approved_status
  after_save :create_observation if :validate_observation


  def create_observation
    observation = CardObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.card = self
    observation.save!
  end

  def set_approved_status
    self.status = Card::AUTORIZADO_CONSIGNATARIA
  end

  def validate_observation
    self.observation.blank? ? false : true
  end

end