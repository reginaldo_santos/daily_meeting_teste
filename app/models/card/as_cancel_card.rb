class Card::AsCancelCard < ActiveType::Record[Card]

  attribute :observation, :text
  attribute :operator_id, :integer
  attribute :password, :string

  validates :observation, presence: true
  validates :password, presence: true
  validate :validate_approval_password

  before_save :set_status
  after_save :create_observation

  def set_status
    self.status = Card::CANCELADO
  end

  def create_observation
    observation = CardObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.card_id = self
    observation.save!
  end

  def validate_approval_password
    if !self.password.blank?
      if !self.public_employee.validate_approval_password(self.password)
        errors.add(:password, 'Senha Incorreta')
      end
    else
      errors.add(:password, 'Não pode ficar em branco')
    end
  end
end