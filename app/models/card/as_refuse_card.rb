class Card::AsRefuseCard < ActiveType::Record[Card]

  attribute :observation, :text
  attribute :operator_id, :integer

  before_save :set_status
  after_save :create_observation

  def set_status
    self.status = Card::REPROVADO_CONSIGNATARIA
  end

  def create_observation
    observation = CardObservation.new
    observation.text = self.observation
    observation.operator_id = self.operator_id
    observation.card_id = self
    observation.save!
  end

end