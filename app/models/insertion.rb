class Insertion < ActiveRecord::Base

  has_paper_trail

  has_many :insertion_lines

  attr_accessor :cut_day

  validates :month, :presence => true
  validates :year, :presence => true
  validates :file_name, :presence => true
  validate :validate_month_per_year


  def validate_month_per_year
    insertion = Insertion.where(:month => self.month, :year => self.year).first

    if insertion
      errors.add(:month, 'Já existe um arquivo de insercao para o mes especificado')
    end
  end

end
