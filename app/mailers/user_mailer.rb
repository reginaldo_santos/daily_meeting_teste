class UserMailer < ActionMailer::Base
  default from: 'no-reply@margemfacil.com.br'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    prefecture_name = Property.where(:name => Property::PREFECTURE_NAME).first.value
    mail :to => user.get_email, :subject => "#{prefecture_name} - Sistema Margem Fácil: Alterar senha."
  end

  def define_login_and_password(user)
    @user = user
    prefecture_name = Property.where(:name => Property::PREFECTURE_NAME).first.value
    mail :to => user.public_employee.email, :subject => "#{prefecture_name} - Sistema Margem Fácil: definir login"
  end

  def send_password_to_user_operator(user)
    @user = user
    @password = user.password
    prefecture_name = Property.where(:name => Property::PREFECTURE_NAME).first.value
    mail :to => user.operator.email, :subject => "#{prefecture_name} - Sistema Margem Fácil: Cadastro Efetuado"
  end
end
