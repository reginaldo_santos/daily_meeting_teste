class MarginService

  def initialize(lines, encoding, file_name)
    @lines = lines
    @encoding = encoding
    @file_name = file_name
  end

  def upload

    employees_list = []

    organs = Organ.all
    situations = Situation.all
    marital_status = MaritalStatus.ids
    literacy = Literacy.ids
    suspension_age = SuspensionAge.all
    commissioned = Commissioned.ids

    line_index = 1

    @lines.each do |line|
      line.force_encoding(@encoding).encode!

      employees_list << fill_public_employee(line, organs, situations, marital_status, literacy, suspension_age, commissioned, @file_name, line_index)
      line_index += 1
    end

    ActiveRecord::Base.transaction do
      employees_list.each_slice(100) do |employee_batch|
        employee_batch.each do |employee|
          exist_employee = PublicEmployee.where(:organ_id => employee.organ_id, :registration => employee.registration).first

          if exist_employee
            update_public_employee(exist_employee, employee)
          else
            employee.save!
          end
        end
      end
      update_not_found_employees
    end

  end

  private

  def fill_public_employee(line, organs, situations, marital_status, literacy, suspension_age, commissioned, file_name, line_index)

    employee = PublicEmployee::AsUploadMargin.new

    employee.line_index = line_index
    employee.file_name = file_name

    employee.name = line[0, 30]
    employee.rg = line[30, 13]
    employee.dispatcher_organ = line[43, 4]
    employee.dispatcher_uf = line[47, 02]
    employee.cpf = line[49, 11]
    employee.organ = organs.where(:code => line[60, 5].to_i.to_s).first
    employee.registration = line[65, 8]
    employee.role = line[73, 15]
    employee.admission_date = Date.parse(line[92, 4]+'-'+line[90, 2]+'-'+line[88, 2]) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[88..95]
    employee.situation = situations.where(:file_reference => line[96, 2]).first
    employee.end_contract_date = Date.new(line[102, 4].to_i, line[100, 2].to_i, line[98, 2].to_i) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[98..105]
    employee.street = line[106, 32]
    employee.number = line[138, 5]
    employee.complement = line[143, 20]
    employee.neighborhood = line[163, 20]
    employee.city = line[183, 15]
    employee.zip_code = line[198, 8]
    employee.phone = line[206, 8].rjust(10, '0')
    employee.birth_date = Date.new(line[218, 4].to_i, line[216, 2].to_i, line[214, 2].to_i) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[214..221]
    employee.marital_status_id = line[222, 1] if marital_status.include?(line[222, 1].to_i)
    employee.voter_registration = line[223, 12]
    employee.zone = line[235, 3]
    employee.section = line[238, 4]
    employee.gender = line[242, 1]
    employee.literacy_id = line[243, 1] if literacy.include?(line[243, 1].to_i)
    employee.margin_value = BigDecimal.new(line[244, 11])/ BigDecimal.new(100) if PublicEmployee::AsUploadMargin::NUMBER_REGEX === line[244, 11]
    employee.suspension_age = suspension_age.where(:file_reference => line[255, 1]).first
    employee.commissioned_id = line[256, 1].to_i if commissioned.include?(line[256, 1].to_i)

    employee.valid?

    return employee
  end

  def update_not_found_employees

    # se o arquivo for importado mais de uma vez no mesmo dia, nao zerara a margem dos servidores nao encontrados,
    # entretanto, se for importado arquivos em dias diferentes os servidores nao encontrados terao sua margem zerada.

    not_found_employees = PublicEmployee.where.not(:updated_at => Date.today.beginning_of_day .. Date.today.end_of_day)

      not_found_employees.each do |employee|
        attributes = {margin_value: BigDecimal.new('0')}
        employee.update(attributes)
      end
  end

  def update_public_employee(employee, modification)
      attributes = {name: modification.name, rg: modification.rg, dispatcher_organ: modification.dispatcher_organ,
                    dispatcher_uf: modification.dispatcher_uf, cpf: modification.cpf, organ_id: modification.organ_id,
                    registration: modification.registration, role: modification.role, admission_date: modification.admission_date,
                    situation_id: modification.situation_id, end_contract_date: modification.end_contract_date, street: modification.street,
                    number: modification.number, complement: modification.complement, neighborhood: modification.neighborhood,
                    city: modification.city, zip_code: modification.zip_code, phone: modification.phone,
                    birth_date: modification.birth_date, marital_status_id: modification.marital_status_id, voter_registration: modification.voter_registration,
                    zone: modification.zone, section: modification.section, gender: modification.gender,
                    literacy_id: modification.literacy_id, margin_value: modification.margin_value, suspension_age_id: modification.suspension_age_id,
                    commissioned_id: modification.commissioned_id}

      employee.assign_attributes(attributes)
      employee.update(attributes)
  end

end