$(document).ready(function(){

    $('#manage_loan_table').dataTable({
        sPaginationType: "full_numbers",
        bJQueryUI: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese-Brasil.json"
        }
    });

});