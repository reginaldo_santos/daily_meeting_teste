//= require jquery_ujs
//= require ../../../vendor/assets/javascripts/porto/jquery-ui-1.10.4.custom.js
//= require wice_grid.js
//= require ../../../vendor/assets/javascripts/porto/jquery.browser.mobile.js
//= require ../../../vendor/assets/javascripts/porto/bootstrap.js
//= require ../../../vendor/assets/javascripts/porto/nanoscroller.js
//= require ../../../vendor/assets/javascripts/porto/bootstrap-datepicker.js
//= require ../../../vendor/assets/javascripts/porto/magnific-popup.js
//= require ../../../vendor/assets/javascripts/porto/jquery.placeholder.js
//= require ../../../vendor/assets/javascripts/porto/theme.js
//= require ../../../vendor/assets/javascripts/porto/theme.custom.js
//= require ../../../vendor/assets/javascripts/porto/theme.init.js
//= require ../../../vendor/assets/javascripts/porto/examples.dashboard.js
//= require ../../../vendor/assets/javascripts/porto/jquery.dataTables.js
//= require ../../../vendor/assets/javascripts/porto/dataTables.tableTools.js
//= require ../../../vendor/assets/javascripts/porto/datatables.js
//= require ../../../vendor/assets/javascripts/porto/jquery.autosize.js
//= require ../../../vendor/assets/javascripts/porto/bootstrap-fileupload.min.js
//= require ../../../vendor/assets/javascripts/porto/select2.js
//= require ../../../vendor/assets/javascripts/porto/examples.datatables.default.js
//= require_tree .

//Remove phone masks at each form submission
//Set phone-links for mobile devices:

$(document).ready(function() {
    $.mask.definitions['9'] = '';

    $(".date_field").mask("99/99/9999");
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    });

    $('.cep').mask('99999-999');

    $('.cnpj-field').mask('99.999.999/9999-99')

    $('.phone-field').mask('(99) 9999 9999')

    $('.voter-field').mask('9999 9999 9999 99');

    $('.cpf').mask('000.000.000-00');
    $('.money').mask('000.000.000.000.000,00', {reverse: true});
    $('.interest').mask('##0,00', {reverse: true});

    $('form').submit(function(){
        remove_cnpj_mask('.cnpj-field');
        remove_cep_mask(('.cep'));
        remove_phone_mask('.phone-field');
        remove_cpf_mask('.cpf')
        remove_voter_mask('.voter-field');
        format_money_to_decimal('.money');
        format_interest_to_decimal('.interest');
    });

    function format_money_to_decimal(id){
        $(id).each(function(){
            val = $(this).val();
            val = val.replace('.', '');
            val = val.replace(',', '.');
            $(this).val(val);
        });
    }

    function format_interest_to_decimal(id){
        $(id).each(function(){
            val = $(this).val();
            val = val.replace(',', '.');
            //val = val.replace('%', '');
            $(this).val(val)
        });
    }

    function remove_cpf_mask(id) {
        $(id).each(function() {
            val = $(this).val();
            val = val.replace(".", "");
            val = val.replace(".", "");
            val = val.replace("-", "");
            $(this).val(val);
        });
    }

    function remove_voter_mask(id){
        $(id).each(function() {
            val = $(this).val();
            val = val.replace(" ", "");
            val = val.replace(" ", "");
            val = val.replace(" ", "");
            $(this).val(val);
        });
    }

    function remove_phone_mask(id) {
        $(id).each(function() {
            val = $(this).val();
            val = val.replace("(", "");
            val = val.replace(")", "");
            val = val.replace(" ", "");
            val = val.replace(" ", "");
            $(this).val(val);
        });
    }

    function remove_cnpj_mask(id) {
        $(id).each(function() {
            val = $(this).val();
            val = val.replace(".", "");
            val = val.replace(".", "");
            val = val.replace("/", "");
            val = val.replace("-", "");
            val = val.replace(" ", "");
            $(this).val(val);
        });
    }

    function remove_cep_mask(id) {
        $(id).each(function() {
            val = $(this).val();
            val = val.replace("-", "");
            $(this).val(val);
        });
    }

    //==BEGIN Set error on control-groups at forms
    setErrorOnControlGroups();
    showOperationsIconsOnlyOnSelectedRow();
    //==END   Set error on control-groups at forms

    //==BEGIN Tooltips for dashboard
    $('#configs').tooltip();
    $('#friend-editing-icon').tooltip();
    $('#friend-notifications-icon').tooltip();
    $('.sms_message_direction').tooltip();
    //==END   Tooltips for dashboard


    //Remove phone masks at each form submission
    $("form").submit(function() { unset_phone_mask(".phone"); });
    if (isMobile()) {
        phonesToLink = $('a[data-phone-link]');
        phonesToLink.each(function() {
            $(this).attr('href', "tel:" + $(this).attr("data-phone-link"));
        });
    }


    set_current_menu();


    set_phone_mask(".phone");
    $('#inputM-tel').addClass('selectedMethod');
    $(".time_field").mask("dd:dd");

    try {
        if ($('#login').val().match(/^([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]?|)$/))
            set_phone_mask("#login");
        else
            $("#login_type_1").click();
    } catch (e) {
        // no problem here...
    }


    $('form').each(function(){
        $(this).submit(function(){
            elements = $('.phone');
            // Needs to put a zero before unimed card number
            elements.each(function(){ $(this).val( $(this).mask() ); });
        });
    });
});

function setErrorOnControlGroups() {
    $(".field_with_errors").each(function () {
        if ($(this).parent().hasClass("controls")) {
            if ($(this).parent().parent().hasClass("control-group"))
                $(this).parent().parent().addClass("error");
        } else if ($(this).parent().hasClass("control-group")) {
            $(this).parent().addClass("error");
        }
    });
}

function showOperationsIconsOnlyOnSelectedRow() {
    $("table tr").mouseenter(function() {
        $(this).children().filter("#image-operations").css("opacity", "1");
        $(this).children().filter("#image-operations").css("filter", "alpha(opacity=100)");
    }).mouseleave(function() {
        $(this).children().filter("#image-operations").css("opacity", "0.1");
        $(this).children().filter("#image-operations").css("filter", "alpha(opacity=10)");
    });
}


function showHelpItem(input, display) {
    helps = $(".form_help_content");
    helps = Array.prototype.slice.call(helps);
    valueToSearch = 'help_' + input.id;
    for ( i = 0; i < helps.length; i++) {
        if (helps[i].id == valueToSearch) {
            if (display) {
                helps[i].style.display = 'inline';
                helps[i].style.right = input.offsetWidth.toString() + "px";
            } else {
                helps[i].style.display = 'none';
            }
        }
    }
}

function checkPassSelected() {
    passValue = $("#check_password");
    $("#password_box").attr('style', passValue.is(':checked') ? 'display:block' : 'display:none');
}

function removePasswordField() {
    passValue = $("#check_password");
    if (!passValue.is(':checked')) {
        $("#password_box").remove();
    }
}


function selectLoginInputMethod(obj) {
    $("#login").val("");
    spans = $('.spanInputMethod');
    for (i = 0; i < spans.length; i++) {
        if (obj.id == spans[i].id) $(obj).addClass('selectedMethod');//.css("color", "white");
        else $(spans[i]).removeClass('selectedMethod'); //.css("color", "#4A98AC");
    }
}

function selectShippingWayMethod(obj) {
    $("#shipping").val(obj.id);
    spans = $('.spanInputMethod');
    for (i = 0; i < spans.length; i++) {
        if (obj.id == spans[i].id) $(obj).addClass('selectedShippingMethod');//.css("color", "white");
        else $(spans[i]).removeClass('selectedShippingMethod'); //.css("color", "#4A98AC");
    }
}

var os = '';
var screenSize = '';
var navigatorName = '';
var navigatorVersion = '';

if (navigator.userAgent.indexOf('Linux') != -1) {
    os = "Linux";
} else if (navigator.userAgent.indexOf('Mac') != -1) {
    os = "Macintosh";
} else if (navigator.userAgent.toLowerCase().indexOf('unix') != -1) {
    os = "Unix";
} else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT 2.0') != -1)) {
    os = "Windows 95";
} else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT 3.0') != -1)) {
    os = "Windows 98";
} else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT 4.0') != -1)) {
    os = "Windows 2000";
} else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT 5.0') != -1) || (navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT 5.1') != -1)) {
    os = "Windows XP";
} else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT 6.0') != -1)) {
    os = "Windows Vista";
} else {
    os = "Not found";
}

if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    navigatorName = 'Firefox';
    navigatorVersion = new Number(RegExp.$1);
} else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
    navigatorName = 'Internet Explorer';
    navigatorVersion = new Number(RegExp.$1);
    if (navigatorVersion < 7) {
        if (os == "Windows XP") {
            //outdated
        }
    }
} else if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    navigatorName = 'Opera';
    navigatorVersion = new Number(RegExp.$1);
} else if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    navigatorName = 'Chrome';
    navigatorVersion = new Number(RegExp.$1);
} else if (/Safari[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    navigatorName = 'Safari';
    navigatorVersion = new Number(RegExp.$1);
} else {
    navigatorName = 'Not found';
    navigatorVersion = 'Not found';
}
screenSize = screen.width + "x" + screen.height;

function isMobile() {
    var index = navigator.appVersion.indexOf("Mobile");
    return (index > -1);
}

function getOSName() {
    return os;
}

function getBrowserVersion() {
    return navigatorVersion;
}

function getBrowserName() {
    return navigatorName;
}

function getScreenSize() {
    return screenSize;
}

var store = (window.name ? eval('(' + window.name + ')') : {
    focus : true,
    number : 0
});

var window_focus_in_chat = store['focus'];
var new_number = 0;

window.onblur = function() {
    window_focus_in_chat = false;
    store['focus'] = window_focus_in_chat;
    window.name = JSON.stringify(store);
}

window.onclick = window.onfocus = function() {
    window_focus_in_chat = true;
    store['focus'] = window_focus_in_chat;
    store['number'] = new_number;
    window.name = JSON.stringify(store);
}

function default_title(title) {
    window.parent.document.title = title;
}

function refresh_title(back_title, new_title, number, i) {
    new_number = number;
    if (window_focus_in_chat) {
        change_title(back_title, number);
    } else if (new_number > store['number']) {
        if (i % 2 == 0) {
            i = 0;
            change_title(new_title, number);
        } else
            change_title(back_title, number);
        i++;
    }

    setTimeout(function() {
        refresh_title(back_title, new_title, number, i);
    }, 800);
}

function redirect_page(url){
    window.location = url;
}

function set_phone_mask(id) {
    $(id).mask("(dd) dddd dddd?d");
    $(id).addClass('phone');
}


//This functions returns HTML code for chat rows. The same HTML on app/views/chat_rooms/_chat_messages.html.erb partial.
function updatedChatRoomMessages(data, patientLabel, systemLabel) {
    html = "";
    for (i=0; i < data.length; i++) {
        //Moment is a js plugin for formatting data on JS. Documentation: http://momentjs.com/docs/
        time = moment(data[i].time);
        html += "<tr class='row-data'>";
        check_incoming = data[i].incoming ? patientLabel : systemLabel;
        check_class_for_message_direction = data[i].incoming ? 'sms_message_incoming_img' : ((data[i].delayed_job_id == null) ? 'sms_message_outgoing_img' : 'sms_message_scheduled_img');
        html += "<td>" + check_incoming;
        html += "<div class='date_time_chat'>";
        //Format documentation: http://momentjs.com/docs/#/parsing/string-format/
        html += time.format("DD/MM/YYYY - HH:mm:ss");
        html += "</div></td>";
        html += "<td>" + data[i].text + "</td>";
        html += "<td><div class='" + check_class_for_message_direction + "'></div></td>";
        html += "</tr>";
    }
    return html;
}

function set_current_menu() {
    $url = $(location).attr('pathname');
    $('#menu li a').each(function() {
        if ($(this).data('value') && $url.match($(this).data('value')) != null) {
            $(this).addClass("selected-menu");
            return;
        }
    });
}