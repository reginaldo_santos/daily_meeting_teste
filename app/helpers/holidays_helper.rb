module HolidaysHelper

  def render_type_select(name, value, on_change_function = "", required = false)
    select_tag "#{name}", options_from_collection_for_select([Holiday::MUNICIPAL, Holiday::STATE,
                Holiday::NATIONAL], "to_s", "to_s", value), :prompt => I18n.t('holiday.choose_option'),
               :onchange => on_change_function, :required => required
  end

end
