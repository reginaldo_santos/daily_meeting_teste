#encoding: utf-8
module ApplicationHelper

  def messages(custom_css_class="")
    m = "<script>alertify.set({delay: 10000});"
    [:info, :success, :warning, :error].each do |item|
      m << "alertify.log('#{flash[item]}', '#{item}');" if flash[item]
    end
    m << "</script>"
    raw "<p>#{m}</p>" unless m.empty?
  end


  def format_currency(value)
    number_to_currency(value, unit: 'R$', separator: ',', delimiter: '.')
  end

  private

  def show_errors_for_attribute_if_any(object, attribute, linkable = false, link_url = "")
    content = content_tag :span, :class => "help-inline", :style => "color: #b94a48 !important;" do
      content_tag(:p, object.errors[attribute].first)
    end if !object.nil? && !object.errors[attribute].empty?
    content
  end

end
