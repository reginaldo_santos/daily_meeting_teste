class ApprovalPasswordPdf < Pdf

  PDF_OPTIONS = {
      :page_size => 'A4',
      :margin => [40, 75]
  }

  def initialize(employee, phone, email, approval_password)


    super('Reserva de Margem', PDF_OPTIONS )

    move_down 40
    text "Prezado(a) #{employee.name},"
    move_down 10
    text "Você está recebendo a senha de reserva de margem, com ela você realiza todas as transações de empréstimos, assistenciais e cartões relacionadas ao sistema Margem Fácil."
    move_down 10
    text 'Confira seus dados:', :style => :bold
    move_down 3
    text "Nome: #{employee.name}", :style => :bold
    text "Matricula: #{employee.registration}" , :style => :bold
    text "Órgão: #{employee.organ.name}", :style => :bold
    move_down 15
    text "Sua senha para reserva de margem no sistema de gestão de margem consignável é: #{approval_password}", :style => :bold
    move_down 40
    text 'Para dúvidas e maiores informações, entre em contato através da Central de Atendimento.'
    move_down 15
    text 'Central de Atendimento', :size => 10, :style => :bold
    text "#{phone}", :size => 10
    text "#{email}", :size => 10
  end

end