class Pdf < Prawn::Document

  def initialize(title, default_prawn_options={})
    super(default_prawn_options)
    header(title)
  end

  def header(title)
    image "#{Rails.root}/public/images/prefeitura_logo.png", :position => :center
    text title, size: 18, style: :bold_italic, align: :center
  end

end