json.array!(@return_codes) do |return_code|
  json.extract! return_code, :id, :code, :description, :obs
  json.url return_code_url(return_code, format: :json)
end
