json.array!(@operators) do |operator|
  json.extract! operator, :id, :name, :cpf, :company_phone_number, :consignee_id, :attendance_local_id, :email
  json.url operator_url(operator, format: :json)
end
