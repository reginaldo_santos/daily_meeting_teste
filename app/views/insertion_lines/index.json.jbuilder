json.array!(@insertion_lines) do |insertion_line|
  json.extract! insertion_line, :id
  json.url insertion_line_url(insertion_line, format: :json)
end
