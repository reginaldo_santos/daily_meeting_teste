json.array!(@holidays) do |holiday|
  json.extract! holiday, :id, :type, :observation, :holiday_date
  json.url holiday_url(holiday, format: :json)
end
