json.array!(@product_configurations) do |product_configuration|
  json.extract! product_configuration, :id, :product_id, :configuration_id, :value
  json.url product_configuration_url(product_configuration, format: :json)
end
