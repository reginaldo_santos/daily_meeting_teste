class Encryption
  require 'digest'

  KEY = 'M$RG#MF$C!L'

  def self.encrypt(str)
    encrypted = AESCrypt.encrypt(str, KEY)

    return encrypted
  end

  def self.decrypt(str)
    decrypted = AESCrypt.decrypt(str, KEY)

    return decrypted
  end

end