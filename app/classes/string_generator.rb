class StringGenerator

  def self.generate(size)
    str = SecureRandom.hex(size)
    str = str[0..size-1]
    return str
  end

  def self.generate_unique_code_to_insertion

    str = generate(5)

    insertion = InsertionLine.where(:line_code => str).first

    if insertion
      str = generate_unique_code_to_insertion
      return str
    else
      return str
    end

  end

end