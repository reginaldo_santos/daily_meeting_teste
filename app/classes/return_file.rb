class ReturnFile

  def self.upload(lines, encoding)

    if lines.size == 1
      messages = []

      line = lines[0]

      line.force_encoding(encoding).encode!

      averbation_code = line[34..35]
      insertion_code = line[110..114]

      if return_line_valid?(averbation_code, insertion_code, messages)

        case averbation_code
          when '00'
            averbar_contrato(insertion_code, averbation_code)
          when '07', '51', '53', '54', '56', '57'
            insertion_line = InsertionLine.find_by(:line_code => insertion_code)
            insertion_line.averbation.code = averbation_code
            insertion_line.line_code = nil
            insertion_line.save!
          when '52'
            dead_employee(insertion_code, averbation_code)
        end
      end


      return messages
    else
      messages = []
      last_index = lines.size - 1
      middle_index = last_index/2
      messages + upload(lines[0 .. middle_index], encoding)
      messages + upload(lines[ (middle_index + 1) .. last_index], encoding)
      return messages
    end

  end

  def self.return_line_valid?(averbation_code, insertion_code, critics_messages)

    if averbation_code.blank?
      critics_messages << "Código de averbação não informado"
    else
      return_code = ReturnCode.find_by(:code => averbation_code)
      if !return_code
        critics_messages << "Código de averbação #{averbation_code} inexistente"
      end
    end

    if insertion_code.blank?
      critics_messages << "Código de inserção não informado"
    else
      code = InsertionLine.find_by(:line_code => insertion_code)
      if !code
        critics_messages << "Código de inserção #{insertion_code} inexistente"
      end
    end

    critics_messages.empty? ? true : false
  end

  def self.dead_employee(insertion_code, averbation_code)
    insertion_line = InsertionLine.find_by(:line_code => insertion_code)
    case insertion_line.insertion_type
      when InsertionLine::LOAN
        insertion_line.loan.update_attribute(:status, Loan::SERVIDOR_FALECIDO)
      when InsertionLine::ASSISTANCE
        insertion_line.assistance_contract.update_attribute(:status, AssistanceContract::SERVIDOR_FALECIDO)
      when InsertionLine::VARIATION
        insertion_line.variation.update_attribute(:status, Variation::SERVIDOR_FALECIDO)
      when InsertionLine::FINANCIAL_RELEASE
        insertion.financial_release.update_attribute(:status, FinancialRelease::SERVIDOR_FALECIDO)
    end
    insertion_line.line_code = nil
    insertion_line.averbation_code = averbation_code
    insertion_line.save!
  end

  def self.averbar_contrato(insertion_code, averbation_code)
    insertion_line = InsertionLine.includes(:loan, :assistance_contract, :variation, :financial_release).where(:line_code => insertion_code).first
    case insertion_line.insertion_type
      when InsertionLine::LOAN
        loan = insertion_line.loan
        loan.installments_paid += 1
        if loan.installments_paid == loan.deadline
          loan.status = Loan::PAGO_SERVIDOR
        end
        loan.save!
      when InsertionLine::ASSISTANCE
        assistance = insertion_line.assistance_contract
        assistance.paid_installments += 1
        assistance.save!
      when InsertionLine::VARIATION
        variation = insertion_line.variation
        variation.paid_installments += 1
        variation.save!
      when InsertionLine::FINANCIAL_RELEASE
        release = insertion.financial_release
        release.status = FinancialRelease::PAGO
        release.save!
    end
    #insertion_line.line_code = nil
    #insertion_line.averbation_code = averbation_code
    #insertion_line.save!
  end

end