require 'authenticated_user'

Rails.application.routes.draw do

  resources :product_configurations

  resources :properties, except: [:new, :create, :destroy]

  resources :financial_releases

  resources :cards, except: [:index, :destroy]

  resources :variations

  get 'loan/search_employee'

  resources :products

  get 'file/margin_file'

  resources :public_employees

  resources :attendance_locals

  resources :consignees

  resources :return_codes

  resources :organs

  resources :operators

  resources :holidays

  resources :loan

  resources :assistance

  resources :variations

  resources :negotiations
  resources :insertion_lines



  resources :users

  match '/default_password' => 'public_employees#default_password', as: 'default_password', via: [:get]

  match '/set_default_password' => 'public_employees#set_default_password', as: 'set_default_password', via: [:post]

  # Autenticacao
  post 'authenticate', to: 'session#login_attempt', as: 'authenticate'
  # Relatorios
  match '/relatorio_analitico' => 'reports#analytical', as: 'analytical_report', via: [:get]
  match '/download_analitico_servidor' => 'reports#analytical_employee', via: [:post]
  match '/download_analitico_periodo' => 'reports#analytical_period', via: [:post]
  match '/relatorio_sintetico' => 'reports#synthetic', as: 'synthetic_report', via: [:get]
  # Servidores
  match '/public_employee/:id/atualizar_contatos' => 'public_employees#update_contacts', as: 'update_contacts', via: [:patch, :post]
  match '/public_employee/:id/editar_contatos' => 'public_employees#edit_contacts', as: 'edit_contacts', via: [:get]
  match '/gerar_senha_aprovacao_servidor' => 'public_employees#generate_approval_password', as: 'generate_approval_password', via: [:post]
  match '/criar_usuario_servidor' => 'users#new_employee_user', as: 'new_employee_user', via: [:get]
  match '/create_usuario_servidor' => 'users#create_employee_user', as: 'create_employee_user', via: [:post]
  # Comum
  match '/login' => 'session#login', as: 'login', via: [:get]
  match '/sair' => 'session#logout', as: 'logout', via:[:delete]

  #match '/atualizar_senha' => 'users#change_password', as: 'change_password', via: [:get]
  #match '/trocar_senha' => 'users#update_password', as: 'update_password', via: [:post]

  match '/user/:id/alterar_senha' => 'users#change_password', as: 'change_password', via: [:get]
  match '/user/:id/trocar_senha' => 'users#update_password', as: 'update_password', via: [:patch]

  match '/inicio' => 'users#dashboard', as: 'dashboard', via: [:get]
  match '/informe_email' => 'password_resets#inform_email', as: 'reset_password_inform_email', via: [:get]
  match '/resetar_senha/:id' => 'password_resets#reset', as: 'reset', via: [:patch]
  match '/enviar_instrucoes_resetar_senha' => 'password_resets#send_reset_password_instruction', as: 'send_reset_password_instruction', via: [:post]
  match '/informe_nova_senha' => 'password_resets#inform_new_password', as: 'inform_new_password', via: [:get]
  # Usuario
  match '/bloquear_usuario' => 'users#block', as: 'block_user', via: [:get]
  match '/desbloquear_usuario' => 'users#unlock', as: 'unlock_user', via: [:get]
  match '/cadastro_servidor' => 'public_employees#manual', as: 'public_employee_manual', via: [:get]
  match '/atualizar_dados_pessoais' => 'users#change_personal_data', as: 'change_personal_data', via: [:get]
  match '/update_personal_data' => 'users#update_personal_data', as: 'update_personal_data', via: [:patch]
  match '/informe_login_password' => 'users#inform_login_and_password', as: 'inform_login_and_password', via: [:get]
  match '/update_login_for_employee_user/:token' => 'users#update_login_for_employee_user', as: 'update_login_for_employee_user', via: [:patch]
  # Arquivos
  match '/arquivo_margem' => 'file#margin_file', as: 'margin_file', via: [:get]
  match '/arquivo_retorno' => 'file#return_file', as: 'return_file', via: [:get]
  match '/upload_margem' => 'file#upload_margin_file', as: 'upload_margin_file', via: [:post]
  match '/upload_retorno' => 'file#upload_return_file', as: 'upload_return_file', via: [:post]
  match '/insercao' => 'insertion#insertion', as: 'insertion', via: [:get]
  match '/insercao_download' => 'insertion#download', as: 'insertion_download', via: [:get]
  match '/generate_insertion_file' => 'insertion#generate_file', as: 'generate_insertion_file', via: [:post]
  match '/retorno_consignataria' => 'insertion_lines#download_file', as: 'download_file', via: [:get]
  # Consulta
  match '/consultar_servidor' => 'search_form#search_employee', as: 'search_employee', via: [:get]
  match '/consultar' => 'search_form#search', as: 'search', via: [:post]
  # Contrato
  match '/gerenciar_contrato' => 'loan#manage_loan', as: 'manage_loan', via: [:get]
  match '/loan/:id/liquidar_contrato' => 'loan#liquidate', as: 'liquidate_loan', via: [:get]
  match '/loan/:id/executar_liquidacao_contrato' => 'loan#liquidate_execute', as: 'liquidate_execute_loan', via: [:patch]
  match '/loan/:id/suspender_contrato' => 'loan#suspended', as: 'suspend_loan', via: [:get]
  match '/loan/:id/executar_suspensao_contrato' => 'loan#suspended_execute', as: 'suspend_execute_loan', via: [:patch]
  match '/loan/:id/recusar_contrato' => 'loan#refuse', as: 'refuse_loan', via:[:get]
  match '/loan/:id/recusar_contrato_executar' => 'loan#refuse_execute', as: 'refuse_loan_execute', via: [:patch]
  match '/loan/:id/aprovar_contrato' => 'loan#approve', as: 'loan_approve', via: [:get]
  match '/loan/:id/executar_aprovacao_contrato' => 'loan#approve_execute', as: 'approve_execute_loan', via: [:patch]
  match '/loan/:id/cancelar_contrato' => 'loan#cancel', as: 'loan_cancel', via: [:get]
  match '/loan/id/emprestimos_portabilidade' => 'loan#loans_portability', as: 'loans_portability', via: [:get]
  match '/loan/:id/executar_cancelamento' => 'loan#cancel_execute', as: 'cancel_execute_loan', via: [:patch]
  match '/busca_contratos_portabilidade' => 'loan#search_for_portability', as: 'search_for_portability', via: [:get]
  match '/busca_servidor' => 'public_employees#search_for_operation', as: 'search_for_operation', via: [:get]
  match '/solicitar_portabilidade' => 'loan#request_portability', as: 'request_portability', via: [:post]
  match '/gerenciar_portabilidade' => 'portability_requests#manage_requests', as: 'manage_portability', via: [:get]
  match '/portability_request/:id/responder_requisicao' => 'portability_requests#answer_request', as: 'answer_request', via: [:get]
  match '/portability_request/:id/responder_requisicao_executar' => 'portability_requests#answer_request_execute', as: 'answer_request_execute', via: [:patch]
  match '/loan/:id/:request_id/:operation_type/novo_refinanciamento' => 'loan#new_refin', as: 'new_refin', via: [:get]
  match '/loan/:id/:request_id/:operation_type/criar_refinanciamento' => 'loan#create_refin', as: 'create_refin', via: [:post]
  match '/loan/:id/:request_id/nova_portabilidade' => 'loan#new_portability', as: 'new_portability', via: [:get]
  match '/loan/:id/:request_id/efetuar_porabilidade' => 'loan#create_portability', as: 'create_portability', via: [:post]
  # Contrato Assistencial
  match '/contratos_assistenciais' => 'assistance#manage_assistance_contract', as: 'manage_assistance_contract', via: [:get]
  match '/assistance/:id/aprovar_contrato_assistencial' => 'assistance#approve', as: 'assistance_approve', via: [:get]
  match '/assistance/:id/executar_aprovacao_assistencial' => 'assistance#approve_execute', as: 'approve_execute_assistance', via: [:patch]
  match '/assistance/:id/recusar_assistencial' => 'assistance#refuse', as: 'assistance_refuse', via: [:get]
  match '/assistance/:id/executar_reprovacao_assistencial' => 'assistance#refuse_execute', as: 'refuse_execute_assistance', via: [:patch]
  match '/assistance/:id/suspender_assistencial' => 'assistance#suspended', as: 'suspended_assistance', via: [:get]
  match '/assistance/:id/suspender_assistencial_executar' => 'assistance#suspended_execute', as:'suspended_assistance_execute', via: [:patch]
  match '/assistance/:id/cancelar_contracto' => 'assistance#cancel', as: 'cancel_assistance', via: [:get]
  match '/assistance/:id/executar_cancelamento' => 'assistance#cancel_execute', as: 'cancel_assistance_execute', via: [:patch]
  # Cartão
  match '/cartoes' => 'cards#manage_cards', as: 'manage_cards', via: [:get]
  match '/card/:id/aprovar_cartao' => 'cards#approve', as: 'card_approve', via: [:get]
  match '/card/:id/executar_aprovacao' => 'cards#approve_execute', as: 'approve_execute', via: [:patch]
  match '/card/:id/recusar_cartao' => 'cards#refuse', as: 'card_refuse', via: [:get]
  match '/card/:id/executar_reprovacao' => 'cards#refuse_execute', as: 'refuse_execute', via: [:patch]
  match '/card/:id/cancelar_cartao' => 'cards#cancel', as: 'card_cancel', via: [:get]
  match '/card/:id/executar_cancelamento' => 'cards#cancel_execute', as: 'cancel_execute', via: [:patch]
  match '/arquivo_lancamentos' => 'financial_releases#file_release', as: 'file_release', via: [:get]
  match '/upload_lancamentos'=> 'financial_releases#upload', as: 'upload_releases', via: [:post]

  match '/public_employee/:id/tranferir_servidor' => 'public_employees#transfer_employee', as: 'transfer_employee', via: [:get]
  match '/public_employee/:id/executar_transferencia' => 'public_employees#execute_transference', as: 'execute_transference', via: [:patch, :post]

  match '/product_configuration/:id/ativar' => 'product_configurations#active', as: 'active_configuration', via: [:post]
  match '/product_configuration/:id/desativar' => 'product_configurations#disable', as: 'disable_configuration', via: [:post]


  root :to => 'users#dashboard', constraints: AuthenticatedUser

  root :to => 'session#login', as: 'guest_root'
end
